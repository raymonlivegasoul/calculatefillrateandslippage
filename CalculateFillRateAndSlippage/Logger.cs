﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateFillRateAndSlippage
{
    public static class Logger
    {

        static Stopwatch sw = new Stopwatch();
        static TimeSpan lastElapsed = new TimeSpan();
        static StringBuilder logfile = new StringBuilder();
        static string logPath;

        static Logger()
        {
            sw.Start();
            lastElapsed = sw.Elapsed;
        }

        public static void Init(string startDate, string endDate)
        {
            logPath = Path.Combine(Const.WorkingFolder, string.Format(@"Slippage_Log_{0}_{1}.txt", startDate, endDate));
            HelperFunction.EnsureFileExists(logPath);
            File.WriteAllText(logPath, string.Empty);
        }

        public static void PrintLogWithTimeStamp(string log)
        {
            var currentTime = sw.Elapsed;
            var sinceLastCheck = currentTime - lastElapsed;
            lastElapsed = currentTime;
            var content = string.Format("Elapsed:{0}, SinceLast:{1}, {2}", (int)currentTime.TotalSeconds, (int)sinceLastCheck.TotalSeconds, log);
            Log("Debug", content);
        }

        public static void Log(string logmessage, params object[] args)
        {
            Log("Info", logmessage, args);
        }

        public static void Log(string type, string logMessage, params object[] args)
        {
            if (args.Length > 0) logMessage = string.Format(logMessage, args);
            Console.WriteLine(logMessage);

            var msg = type + ": (" + DateTime.Now + ") " + logMessage;
            //logfile.AppendLine(msg);
            File.AppendAllText(logPath, msg + Environment.NewLine);
        }

        public static void Flush(string startDate, string endDate)
        {
            //HelperFunction.EnsureFileExists(logPath);
            //File.WriteAllText(logPath, logfile.ToString());
        }
    }
}
