﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace CalculateFillRateAndSlippage
{
    class Program
    {

        static void Main(string[] args)
        {
            var vm = new MainViewModel();
            if (args.Length == 0) throw new ArgumentException("Missing input arguments");

            DateTime date = DateTime.MinValue;
            switch (args[1])
            {
                case "TODAY":
                    date = DateTime.Today.Date;
                    break;
                case "YESTERDAY":
                    date = DateTime.Today.Date;
                    do
                    {
                        date = date.AddDays(-1);
                    }
                    while (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday); // Skip weekend

                    break;
                default:
                    var intDate = int.Parse(args[1]);
                    if (intDate < 0)
                    {
                        date = DateTime.Today.Date.AddDays(intDate);
                        // Skip weekend
                        while (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
                        {
                            // Skip two days
                            date = date.AddDays(-2);
                        }
                    }
                    else
                    {
                        date = DateTime.ParseExact(args[1], "yyyyMMdd", null);
                    }
                    break;
            }
                
            var dateRange = args[2].Split(',').Select(value => int.Parse(value)).ToList();
            vm.Init(args[0], date, dateRange);
            vm.Start();
        }
    }
}

