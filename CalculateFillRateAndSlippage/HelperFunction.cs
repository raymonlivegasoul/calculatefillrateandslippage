﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utility.Database;

namespace CalculateFillRateAndSlippage
{
    public static class HelperFunction
    {
        public static void EnsureFileExists(string path)
        {
            if (!File.Exists(path))
            {
                var stream = File.Create(path);
                stream.Close();
            }
        }

        public static void EnsureFolderExists(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static List<DateTime> GetDateList(DateTime start, DateTime end)
        {
            var current = start;
            var list = new List<DateTime>();
            while(current <= end)
            {
                if (current.DayOfWeek != DayOfWeek.Saturday && current.DayOfWeek != DayOfWeek.Sunday)
                {
                    list.Add(current);
                }
                current = current.AddDays(1);
            }
            return list;
        }

        public static DateTime ConvertToFiveMinuteBarTime(DateTime date)
        {
            var newDate = new DateTime(date.Year, date.Month, date.Day);
            var hour = date.Hour;
            int min = (date.Minute / 5 + 1) * 5;
            if (min >= 60)
            {
                hour++;
                min = 0;
            }
            newDate = newDate.AddHours(hour);
            newDate = newDate.AddMinutes(min);
            return newDate;
        }

        public static DataTable TryExecuteQuery(string sql)
        {
            var count = 0;
            DataTable tb = new DataTable();
            while (count < 10)
            {
                tb = ExecuteQuery(sql);
                if (tb != null) break;
                count++;
                System.Threading.Thread.Sleep(5000);
            }
            return tb;
        }

        public static DataTable ExecuteQuery(string sql)
        {
            DataTable tb = new DataTable();

            using (var connection = new SqlConnection(DBManager.Default.ConnectionString))
            {
                try
                {
                    connection.Open();

                    var command = new SqlCommand(sql, connection);
                    command.CommandTimeout = 3000;
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = command;
                    adapter.Fill(tb);
                    return tb;
                }
                catch (Exception ex)
                {
                    //return null;
                    throw ex;
                }
            }            
        }
    }
}
