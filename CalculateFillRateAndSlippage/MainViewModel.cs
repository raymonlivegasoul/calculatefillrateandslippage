﻿using CalculateFillRateAndSlippage.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Utility.Database;
using System.Threading.Tasks;
using System.Data;
using System.Text;
using System.Configuration;

namespace CalculateFillRateAndSlippage
{
    public class MainViewModel
    {
        #region Properties
        string StartDate;
        string EndDate;
        public List<string> Products = new List<string>();
        public List<DateTime[]> DateRangeList = new List<DateTime[]>();
        public bool IsAllProduct { get; set; }
        #endregion

        public void Start()
        {
            //LoadConfig();
            var dataModel = new DataViewModel();
            dataModel.Products = Products;
            dataModel.StartDate = StartDate;
            dataModel.EndDate = EndDate;

            Logger.Init(dataModel.StartDate, dataModel.EndDate);
            Logger.Log("Program Initiated");

            var noError = dataModel.LoadData();
            if (!noError) Environment.Exit(0);

            var lines = GetLines(dataModel);
            var test = lines[0].Data.Where(value => value.Algo == "VolumeCrossCommodity" && value.NewSlippageInTicks != 0).ToList();
            foreach (var dateRange in DateRangeList)
            {
                var reportModel = new ReportViewModel();
                var start = dateRange[0];
                var end = dateRange[1];
                var startDate = start.ToString("yyyyMMdd");
                var endDate = end.ToString("yyyyMMdd");
                Logger.PrintLogWithTimeStamp(string.Format("Generate report StartDate:{0}, EndDate:{1}.", startDate, endDate));
                var reportLines = lines.Where(value => value.Date >= start && value.Date <= end).ToList();
                reportModel.Init(startDate, endDate, dataModel, reportLines);
                reportModel.BuildReport();
            }

            Logger.Flush(dataModel.StartDate, dataModel.EndDate);
            Logger.Log("Program Completed");
        }

        public void Init(string products, DateTime date, List<int> dateRange)
        {   
            Products = new List<string>();
            products = products.ToUpper();
            if (!products.Contains("ALL"))
            {
                Products.AddRange(products.Split(','));
                IsAllProduct = false;
            }
            else IsAllProduct = true;

            DateRangeList = new List<DateTime[]>();
            foreach (var range in dateRange)
            {
                DateTime startDate = DateTime.MinValue;
                if (range > 10000)
                {
                    startDate = DateTime.ParseExact(range.ToString(), "yyyyMMdd", null);
                }
                else
                {
                    if (range == 1) startDate = date;
                    else startDate = date.AddDays(-range);
                }
                DateRangeList.Add(new DateTime[] { startDate, date });
            }

            StartDate = DateRangeList.Select(value => value[0]).Min().ToString("yyyyMMdd");
            EndDate = DateRangeList.Select(value => value[1]).Max().ToString("yyyyMMdd");
            Const.WorkingFolder = Path.Combine(Directory.CreateDirectory(ConfigurationManager.AppSettings["OUTPUT_PATH"]).FullName, EndDate);
            HelperFunction.EnsureFolderExists(Const.WorkingFolder);
        }

        private List<SingleFileColumnModelList> GetLines(DataViewModel dataModel)
        {
            var result = new List<SingleFileColumnModelList>();
            var start = DateTime.ParseExact(dataModel.StartDate, "yyyyMMdd", null);
            var end = DateTime.ParseExact(dataModel.EndDate, "yyyyMMdd", null);
            var targetDates = HelperFunction.GetDateList(start, end);

            Logger.PrintLogWithTimeStamp(string.Format("Calc data StartDate:{0}, EndDate:{1}.", dataModel.StartDate, dataModel.EndDate));

            var po = new ParallelOptions
            {
                MaxDegreeOfParallelism = 10
            };

            var totalCount = 0;
            Parallel.ForEach(targetDates, po, date =>
            {
                var count = 0;
                SingleFileColumnModelList list = null;
                while (count < 10)
                {
                    var calcModelMain = new CalculateFillRateAndSlippageViewModel(dataModel);
                    list = calcModelMain.GetCalculatedData(date, IsAllProduct);
                    if (list != null) break;
                    count++;
                    System.Threading.Thread.Sleep(5000);
                }

                if (list == null)
                {
                    Logger.Log("ERROR", string.Format("Calculation failed for date {0}", date.ToShortDateString()));
                    return;
                }

                lock (result)
                {
                    result.Add(list);
                    totalCount++;
                    Logger.Log("Debug", string.Format("Finished calculation for date {0} ({1}/{2})", date.ToShortDateString(), totalCount, targetDates.Count));
                }
            });

            return result;
        }
    }
}
