﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Utility.Database;
using CalculateFillRateAndSlippage.Model;
using System.ComponentModel;
using System.Diagnostics;

namespace CalculateFillRateAndSlippage
{
    public class CalculateFillRateAndSlippageViewModel
    {
        #region Properties
        public List<string> Products = new List<string>();
        StringBuilder sbFillRateSpread;
        StringBuilder sbSkippedOrders;

        Dictionary<string, double> prodTickDict { get; set; }
        Dictionary<string, double> highSpreadThresholds { get; set; }
        Dictionary<string, double> lowSpreadThresholds { get; set; }
        Dictionary<string, int> machineProdDateList { get; set; }
        Dictionary<string, double> productDailyRangeDict { get; set; }
        Dictionary<string, double> productDayCloseDict { get; set; }
        Dictionary<string, Dictionary<DateTime, double>> FiveMinuteBars { get; set; }
        List<AumInfoModel> aumInfos { get; set; }
        // signalFileLines is used to store all master signals of master signal report
        List<SingleFileColumnModel> signalFileLines { get; set; }

        private const string TMP_FILE_PATH = "TmpDataFile";
        #endregion

        public CalculateFillRateAndSlippageViewModel(DataViewModel dataModel)
        {
            Products = dataModel.Products;
            prodTickDict = dataModel.ProdTickDict;
            machineProdDateList = dataModel.MachineProdDateList;
            FiveMinuteBars = dataModel.FiveMinuteBars;
            aumInfos = dataModel.AumInfos;
            highSpreadThresholds = dataModel.HighSpreadThresholds;
            lowSpreadThresholds = dataModel.LowSpreadThresholds;
            productDailyRangeDict = dataModel.ProductDailyRangeDict;
            productDayCloseDict = dataModel.ProductDayCloseDict;
            signalFileLines = new List<SingleFileColumnModel>();

            // the log file 
            sbFillRateSpread = new StringBuilder();

            // skipped order file, saving the info of all skipped child orders
            sbSkippedOrders = new StringBuilder();
        }

        public SingleFileColumnModelList GetCalculatedData(DateTime date, bool useTmpFile = true)
        {
            try
            {
                SingleFileColumnModelList list = null;
                if (useTmpFile) list = LoadData(date);
                if (list == null)
                {
                    list = Calculate(date);
                    Logger.Log("Debug", string.Format("Data for date {0} loaded from database", date.ToShortDateString()));
                    if (useTmpFile) WriteData(list);
                    
                }
                return list;
            }
            catch(Exception ex)
            {
                //return null;
            }
            return null;
        }

        public SingleFileColumnModelList Calculate(DateTime date)
        {
            var dateAsString = date.ToString("yyyyMMdd");
            var signalSummaryList = LoadSignalSummary(dateAsString);
            var signalLatencies = LoadSignalLatency(dateAsString);
            var bboList = LoadTradeFeed(dateAsString);
            return BuildSignalSummary(date, signalSummaryList, signalLatencies, bboList);
        }

        private SingleFileColumnModelList BuildSignalSummary(DateTime date, List<SignalSummaryModel> signalSummaryList, List<SignalLatencyModel> signalLatencies, List<BboModel> bboList)
        {
            var list = new SingleFileColumnModelList();
            list.Date = date;
            // Init AlgoReport
            list.AlgoReport = new Dictionary<string, AlgoReportStatModel>();
            foreach (var prodName in Products)
            {
                list.AlgoReport.Add(prodName, new AlgoReportStatModel());
            }

            foreach (var signalSummaryByProduct in signalSummaryList.GroupBy(value => value.ProductName))
            {
                try
                {
                    var prodName = signalSummaryByProduct.Key;
                    if (string.IsNullOrEmpty(prodName)) continue;
                    if (!Products.Contains(prodName)) continue;

                    // get the tick size of the product
                    double tick = prodTickDict[prodName];
                    var bboByProduct = bboList.Where(value => value.ProdName == prodName);

                    foreach (var signalSummaryByMachine in signalSummaryByProduct.GroupBy(value => value.MachineName))
                    {
                        var machineName = signalSummaryByMachine.Key;
                        var bboByMachine = bboByProduct.Where(value => value.MachineName == machineName);

                        var algoGroup = signalSummaryByMachine.GroupBy(value => value.Algo).OrderBy(v => v.Key);
                        foreach (var signalSummaryByAlgo in algoGroup)
                        {
                            if (signalSummaryByAlgo.Count() == 0) continue;
                            var algoName = signalSummaryByAlgo.Key;

                            var bboByAlgo = bboByMachine.Where(bbo => bbo.MachineName.Equals(machineName) && bbo.AlgoEngine.Equals(algoName));
                            var signalLatencyByDate = signalLatencies.Where(latency => latency.MachineName.Equals(machineName) && latency.ProdName.Equals(prodName)).ToList();
                            BuildSignalSummary(date, machineName, algoName, prodName, tick, bboByAlgo.ToList(), signalSummaryByAlgo.ToList(), signalLatencyByDate, list);
                        }

                        // write vfix trades which cannot find signal summary 
                        foreach (var bbo in bboByMachine)
                        {
                            if (!bbo.IsFound)
                            {
                                sbSkippedOrders.AppendLine(prodName + "-" + machineName + "-" + bbo.Date.ToString("yyyyMMdd") + "-" +
                                    bbo.Strategy + "-" + bbo.MasterSignalID + "-" + bbo.ChildSignalID + ", vfix trade not matched with signal summary");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log("ERROR", ex.Message + ex.StackTrace);
                }
            }
            return list;
        }

        public void BuildSignalSummary(DateTime date, string machineName, string algoName, string prodName, double tick, List<BboModel> bboLinesByDate, List<SignalSummaryModel> signalSummaryByDate
            , List<SignalLatencyModel> signalLatenciesByDate, SingleFileColumnModelList list)
        {
            var dateString = date.ToString("yyyyMMdd");
            // check whether current machine, product, algo and date is on production, skip if it is not on production
            string mpd = machineName + "-" + prodName + "-" + dateString;
            if (!machineProdDateList.ContainsKey(mpd)) return;
            var fiveMinuteBars = FiveMinuteBars[prodName];
            // on production
            var firstSniperSignalDict = new Dictionary<string, int>(); // dictionary used to remember the first sniper order of the master signal
            var firstFloatSignalDict = new Dictionary<string, int>(); // dictionary used to remember the first float order of the master signal
            var firstSweepSignalDict = new Dictionary<string, int>(); // dictionary used to remember the first sweep order of the master signal
            var firstChildSignalBBODict = new Dictionary<string, string[]>(); // dictionary used to remember the first child order of the master signal
            var unFullFilledOrders = new List<string[]>(); // unfully filled child orders
            var dailySignalFileLines = new List<SingleFileColumnModel>(); // dailySignalFileLines is used to store all master signals of master signal report of current day

            // calculate average daily range in past 200 days
            double averageDailyRange20DInTicks = 0;
            int numDays = 0;
            DateTime today = date.AddDays(0); // DateTime today = date.AddDays(-1);
            for (int i = 0; i < 20; ++i)  // for (int i = 0; i < 320; ++i)
            {
                if (productDailyRangeDict.ContainsKey(prodName + "-" + today.ToString("yyyyMMdd")))
                {
                    averageDailyRange20DInTicks += productDailyRangeDict[prodName + "-" + today.ToString("yyyyMMdd")] / tick;
                    numDays++;
                }

                if (numDays >= 1)  // if (numDays >= 200)
                {
                    break;
                }
                today = today.AddDays(-1);
            }
            averageDailyRange20DInTicks /= numDays;


            // get current day close price for calculating day close slippage
            double dayClosePrice = 0;
            if (productDayCloseDict.ContainsKey(prodName + "-" + dateString))
            {
                dayClosePrice = productDayCloseDict[prodName + "-" + dateString];
            }


            if (algoName.Equals("Default"))
            {
                // calculate slippage for default algo                               
                CalculateSlippageForDefault(prodName, date, machineName, algoName, bboLinesByDate, dayClosePrice, tick, dailySignalFileLines, averageDailyRange20DInTicks, aumInfos, sbFillRateSpread);
            }
            else
            {
                var algoReport = list.AlgoReport[prodName];
                // mark production machine which has trades
                if (signalSummaryByDate.Count() > 0) machineProdDateList[mpd]++;

                foreach (var signalSummary in signalSummaryByDate.OrderBy(value => value.SignalID))
                {
                    int signalId = signalSummary.SignalID;
                    int masterOrderId = signalSummary.MasterOrderID;
                    int size = signalSummary.Size;
                    int filledSize = signalSummary.FilledSize;
                    double filledFirstPrice = signalSummary.FilledPrice;
                    string buySell = signalSummary.Direction;
                    string strategy = signalSummary.Strategy;
                    var childTypeValue = signalSummary.ChildType;
                    double orderPrice = signalSummary.OrderPrice;

                    if (signalSummary.Status.Equals("Rejected"))
                    {
                        algoReport.NumOrderRejected += 1;
                    }

                    if (signalSummary.ExceedFatFingerLimit > 0) algoReport.NumOrderExceedingFatFingerLimit += 1;
                    if (signalSummary.AbnormalFillPrice > 0) algoReport.NumOrderWithAbnormalFillPrice += 1;

                    if (!algoReport.NumOrderPerDay.ContainsKey(dateString))
                    {
                        algoReport.NumOrderPerDay.Add(dateString, 0);
                    }
                    algoReport.NumOrderPerDay[dateString] += 1;

                    if (signalSummary.Direction.Equals("Buy"))
                    {
                        if (!algoReport.NumBuyOrderPerDay.ContainsKey(dateString))
                        {
                            algoReport.NumBuyOrderPerDay.Add(dateString, 0);
                        }
                        algoReport.NumBuyOrderPerDay[dateString] += 1;
                    }
                    else
                    {
                        if (!algoReport.NumSellOrderPerDay.ContainsKey(dateString))
                        {
                            algoReport.NumSellOrderPerDay.Add(dateString, 0);
                        }
                        algoReport.NumSellOrderPerDay[dateString] += 1;
                    }

                    // get child order bbo
                    double childOrderBestBid = signalSummary.BidPrice;
                    double childOrderBestAsk = signalSummary.AskPrice;
                    int childOrderBestBidSize = signalSummary.BidSize;
                    int childOrderBestAskSize = signalSummary.AskSize;
                    int childOrderSpreadInTicks = (int)((childOrderBestAsk - childOrderBestBid) / tick + 0.1);
                    int childFarTouchSize = signalSummary.IsBuy ? childOrderBestAskSize : childOrderBestBidSize;
                    double childIndicator = childFarTouchSize / (double)(childOrderBestAskSize + childOrderBestBidSize);
                    int pricemove = 0;

                    if (signalSummary.Round0 > 0)
                    {
                        pricemove = 1;
                    }
                    else if (signalSummary.Round0 < 0)
                    {
                        pricemove = -1;
                    }

                    // get child order start time and end time
                    DateTime childOrderStartTime = signalSummary.StartTime;
                    DateTime childOrderEndTime = signalSummary.EndTime;

                    if (childOrderStartTime != DateTime.MinValue)
                    {
                        var fiveMinBarDate = HelperFunction.ConvertToFiveMinuteBarTime(childOrderStartTime);
                        if (!algoReport.MarketVolume.ContainsKey(fiveMinBarDate))
                        {
                            algoReport.MarketVolume.Add(fiveMinBarDate, 0);
                        }
                        if (fiveMinuteBars.ContainsKey(fiveMinBarDate))
                        {
                            algoReport.MarketVolume[fiveMinBarDate] = fiveMinuteBars[fiveMinBarDate];
                        }
                    }

                    if (childOrderEndTime != DateTime.MinValue)
                    {
                        var fiveMinBarDate = HelperFunction.ConvertToFiveMinuteBarTime(childOrderEndTime);
                        if (!algoReport.MarketVolume.ContainsKey(fiveMinBarDate))
                        {
                            algoReport.MarketVolume.Add(fiveMinBarDate, 0);
                        }
                        if (fiveMinuteBars.ContainsKey(fiveMinBarDate))
                        {
                            algoReport.MarketVolume[fiveMinBarDate] = fiveMinuteBars[fiveMinBarDate];
                        }

                        if (!algoReport.ExecutedVolume.ContainsKey(fiveMinBarDate))
                        {
                            algoReport.ExecutedVolume.Add(fiveMinBarDate, filledSize);
                        }
                        else algoReport.ExecutedVolume[fiveMinBarDate] += filledSize;
                    }

                    while (algoReport.OrderSubmitTime.Count > 0)
                    {
                        if ((algoReport.OrderSubmitTime[0] < childOrderStartTime.AddSeconds(-1)) || (algoReport.OrderSubmitTime[0] > childOrderStartTime.AddSeconds(1)))
                        {
                            algoReport.OrderSubmitTime.RemoveAt(0);
                            algoReport.TotalVolumeInOneSecond -= algoReport.OrderSubmitVolume[0];
                            algoReport.OrderSubmitVolume.RemoveAt(0);
                        }
                        else
                        {
                            break;
                        }
                    }
                    algoReport.OrderSubmitTime.Add(childOrderStartTime);
                    algoReport.OrderSubmitVolume.Add(size);
                    algoReport.TotalVolumeInOneSecond += size;

                    algoReport.TotalNumOrder += 1;
                    algoReport.TotalVolumeOrder += size;

                    while (algoReport.DirectionTime.Count > 0)
                    {
                        if ((algoReport.DirectionTime[0] < childOrderStartTime.AddMinutes(-1)) || (algoReport.DirectionTime[0] > childOrderStartTime))
                        {
                            algoReport.DirectionTime.RemoveAt(0);
                            algoReport.Directions.RemoveAt(0);
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (algoReport.Directions.Count > 0)
                    {
                        if (algoReport.Directions[algoReport.Directions.Count - 1] == buySell)
                        {
                            algoReport.Directions.RemoveAt(algoReport.Directions.Count - 1);
                            algoReport.DirectionTime.RemoveAt(algoReport.DirectionTime.Count - 1);
                        }
                    }
                    algoReport.Directions.Add(buySell);
                    algoReport.DirectionTime.Add(childOrderStartTime);
                    if (algoReport.Directions.Count - 1 > algoReport.MaxNumberOfReversalInOneMinute)
                    {
                        algoReport.MaxNumberOfReversalInOneMinute = algoReport.Directions.Count - 1;
                        algoReport.MaxNumberOfReversalTime = algoReport.DirectionTime[0].ToString();
                    }

                    while (algoReport.FillTime.Count > 0)
                    {
                        if ((algoReport.FillTime[0] < childOrderEndTime.AddMinutes(-5)) || (algoReport.FillTime[0] > childOrderEndTime))
                        {
                            if (algoReport.FillDirection[0] == "Buy")
                            {
                                algoReport.BuyQty = Math.Max(0, algoReport.BuyQty - algoReport.FillQty[0]);
                                algoReport.BuyValue = Math.Max(0, algoReport.BuyValue - algoReport.FillQty[0] * algoReport.FillPrice[0]);
                                algoReport.BuyNum = Math.Max(0, algoReport.BuyNum - 1);
                            }
                            else
                            {
                                algoReport.SellQty = Math.Max(0, algoReport.SellQty - algoReport.FillQty[0]);
                                algoReport.SellValue = Math.Max(0, algoReport.SellValue - algoReport.FillQty[0] * algoReport.FillPrice[0]);
                                algoReport.SellNum = Math.Max(0, algoReport.SellNum - 1);
                            }
                            algoReport.FillTime.RemoveAt(0);
                            algoReport.FillQty.RemoveAt(0);
                            algoReport.FillPrice.RemoveAt(0);
                            algoReport.FillDirection.RemoveAt(0);
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (filledSize > 0)
                    {
                        algoReport.FillTime.Add(childOrderEndTime);
                        algoReport.FillQty.Add(filledSize);
                        algoReport.FillPrice.Add(filledFirstPrice);
                        algoReport.FillDirection.Add(buySell);
                        if (buySell == "Buy")
                        {
                            algoReport.BuyQty = algoReport.BuyQty + filledSize;
                            algoReport.BuyValue = algoReport.BuyValue + filledFirstPrice * filledSize;
                            algoReport.BuyNum = algoReport.BuyNum + 1;
                        }
                        else
                        {
                            algoReport.SellQty = algoReport.SellQty + filledSize;
                            algoReport.SellValue = algoReport.SellValue + filledFirstPrice * filledSize;
                            algoReport.SellNum = algoReport.SellNum + 1;
                        }
                        if (Math.Min(algoReport.BuyNum, algoReport.SellNum) > algoReport.MaxNumberOfOrdersMatchedInFiveMinutes)
                        {
                            algoReport.MaxNumberOfOrdersMatchedInFiveMinutes = Math.Min(algoReport.BuyNum, algoReport.SellNum);
                            algoReport.MaxNumberOfOrdersMatchedTime = childOrderEndTime.ToString();
                            if (algoReport.BuyQty > 0 && algoReport.SellQty > 0)
                            {
                                algoReport.PnLOrdersMatchedInFiveMinutes = ((algoReport.SellValue / algoReport.SellQty) - (algoReport.BuyValue / algoReport.BuyQty)) / tick;
                            }
                        }
                    }



                    if (size > algoReport.MaxOrderSizePerOrder)
                    {
                        algoReport.MaxOrderSizePerOrder = size;
                    }

                    if (algoReport.OrderSubmitTime.Count > algoReport.MaxNumOrderPerSecond)
                    {
                        algoReport.MaxNumOrderPerSecond = algoReport.OrderSubmitTime.Count;
                    }

                    if (algoReport.TotalVolumeInOneSecond > algoReport.MaxVolumeSentPerSecond)
                    {
                        algoReport.MaxVolumeSentPerSecond = algoReport.TotalVolumeInOneSecond;
                    }


                    // skip current child order if size is wrong
                    if (size <= 0)
                        continue;

                    list.NumAllOrders++;

                    // get AUM for Prop and Fund products respectively
                    string pftype;
                    if (prodName.Equals("SQIF") || prodName.Equals("HSI") || prodName.Equals("HHI") ||
                        prodName.Equals("TXF") || prodName.Equals("EXF") || prodName.Equals("FXF") ||
                        prodName.Equals("IFS") || prodName.Equals("MEFF") || prodName.Equals("FSMI") ||
                        prodName.Equals("CF") || prodName.Equals("OI") || prodName.Equals("SR") ||
                        prodName.Equals("TA") || prodName.Equals("A") || prodName.Equals("C") ||
                        prodName.Equals("J") || prodName.Equals("L") || prodName.Equals("M") ||
                        prodName.Equals("P") || prodName.Equals("V") || prodName.Equals("Y") ||
                        prodName.Equals("CCU") || prodName.Equals("CRB") || prodName.Equals("RU") ||
                        prodName.Equals("FG") || prodName.Equals("JM") || prodName.Equals("RM") ||
                        prodName.Equals("BB") || prodName.Equals("JD") || prodName.Equals("ME") ||
                        prodName.Equals("ZN"))
                    {
                        pftype = "Prop";
                    }
                    else
                    {
                        pftype = "Fund";
                    }

                    // AUM is different for short term and long term strategies
                    double aumInUSD = 0;
                    var aumLine = aumInfos.FirstOrDefault(aum => aum.Type == pftype && aum.StartDate <= date && aum.EndDate >= date);
                    if (aumLine != null)
                    {
                        if (strategy.Equals("B") || strategy.Equals("E") || strategy.Equals("F") || strategy.Equals("G") || strategy.Equals("H") ||
                            strategy.Equals("P") || strategy.Equals("R") || strategy.Equals("RHK") || strategy.Equals("U") || strategy.Equals("W"))
                        {
                            aumInUSD = aumLine.ST;
                        }
                        else
                        {
                            aumInUSD = aumLine.LT;
                        }
                    }
                    else
                    {
                        sbFillRateSpread.AppendLine("AUM not found for " + prodName + " on " + dateString);
                        Logger.Log("ERROR", "AUM not found for " + prodName + " on " + dateString);
                        //continue;
                    }

                    // first child order bbo, and child type, i.e. "sniper" or "float"    
                    string key = dateString + "-" + masterOrderId.ToString() + "-" + machineName;
                    if (!firstChildSignalBBODict.ContainsKey(key))
                    {
                        string childType = "Sniper";
                        if (childTypeValue.Equals("Float") ||
                            childTypeValue.Equals("Peg") ||
                            childTypeValue.Equals("SupplementFloat") ||
                            childTypeValue.Equals("SepSniperFloat") ||
                            childTypeValue.Equals("MidSniper") ||
                            childTypeValue.Equals("SLMidSniper") ||
                            childTypeValue.Equals("CrossMidSniper"))
                        {
                            childType = "Float";
                        }

                        firstChildSignalBBODict.Add(key, new string[] { childOrderBestBid.ToString(), childOrderBestAsk.ToString(), childOrderBestBidSize.ToString(), childOrderBestAskSize.ToString(), childType, pricemove.ToString() });
                    }
                    /*if (FirstChildSignalBBODict[key][4] != "Sniper")
                    {
                        continue;
                    }*/

                    // unfullfilled orders for calculating cancel cost
                    if (filledSize < size)
                    {
                        unFullFilledOrders.Add(new string[] {masterOrderId.ToString(), signalId.ToString(), childOrderBestBid.ToString(), childOrderBestAsk.ToString(),
                                                    childOrderBestBidSize.ToString(), childOrderBestAskSize.ToString(), childOrderStartTime.ToString("yyyy-MM-dd HH:mm:ss.fff"), childOrderEndTime.ToString("yyyy-MM-dd HH:mm:ss.fff")});

                    }

                    // get all vfix trades of the master signal which current child order belongs to
                    var masterSignalBBOs = bboLinesByDate.Where(bbo => bbo.MasterSignalID.Equals(masterOrderId) && bbo.Strategy.Equals(strategy)).ToList();
                    if (masterSignalBBOs.Count == 0)
                    {
                        // record skipped orders
                        list.NumSkippedOrders++;
                        //Log("ERROR", prodName + "-" + machineName + "-" + dateString + "-" + strategy + "-" + masterOrderId + "-" + signalId + ", cannot find vfix trades");
                        //sbSkippedOrders.AppendLine(prodName + "-" + machineName + "-" + dateString + "-" + strategy + "-" + masterOrderId + "-" + signalId + ", cannot find vfix trades");
                        continue;
                    }


                    // sort vfix trades according to child order ID
                    masterSignalBBOs.Sort(delegate (BboModel a, BboModel b) { return a.ChildSignalID - b.ChildSignalID; });

                    // rewrite column 20 to True because those vfix trades have corresponding signals in signal summray
                    foreach (var bbo in masterSignalBBOs)
                    {
                        bbo.IsFound = true;
                    }

                    // get the arrival bbo, trigger time, benchmark bbo etc
                    var masterSignalBBO = masterSignalBBOs.First();
                    double bestBid = masterSignalBBO.SignalBestBid;
                    double bestAsk = masterSignalBBO.SignalBestAsk;
                    int bestBidSize = masterSignalBBO.SignalBestBidSize;
                    int bestAskSize = masterSignalBBO.SignalBestAskSize;
                    DateTime triggerTime = masterSignalBBO.TriggerTime;
                    double exchangeRate = masterSignalBBO.ExchangeRate;
                    double pointValue = masterSignalBBO.PointValue;
                    string contract = masterSignalBBO.Contract;
                    double risk = masterSignalBBO.Risk;
                    int tagMaxPos = masterSignalBBO.TagMaxPos;

                    if (double.IsNaN(bestBid) || double.IsNaN(bestAsk) || bestBidSize <= 0 || bestAskSize <= 0)
                    {
                        // if arrival bbo is wrong or missing, use first child order bbo as the arrival bbo
                        double firstChildBestBid = double.NaN;
                        double firstChildBestAsk = double.NaN;
                        int firstChildBestBidSize = 0;
                        int firstChildBestAskSize = 0;

                        key = dateString + "-" + masterOrderId.ToString() + "-" + machineName;
                        firstChildBestBid = double.Parse(firstChildSignalBBODict[key][0]);
                        firstChildBestAsk = double.Parse(firstChildSignalBBODict[key][1]);
                        firstChildBestBidSize = int.Parse(firstChildSignalBBODict[key][2]);
                        firstChildBestAskSize = int.Parse(firstChildSignalBBODict[key][3]);


                        if (double.IsNaN(firstChildBestBid) || double.IsNaN(firstChildBestAsk) || double.IsNaN(firstChildBestBidSize) || double.IsNaN(firstChildBestAskSize))
                        {
                            list.NumSkippedOrders++;
                            Logger.Log("ERROR", prodName + "-" + machineName + "-" + dateString + "-" + strategy + "-" + masterOrderId + "-" + signalId + ", both arrival bbo and first child bbo is invalid");
                            sbSkippedOrders.AppendLine(prodName + "-" + machineName + "-" + dateString + "-" + strategy + "-" + masterOrderId + "-" + signalId + ", both arrival bbo and first child bbo is invalid");
                            continue;
                        }
                        else
                        {
                            bestBid = firstChildBestBid;
                            bestAsk = firstChildBestAsk;
                            bestBidSize = firstChildBestBidSize;
                            bestAskSize = firstChildBestAskSize;
                        }
                    }

                    int arrivalSpreadInTicks = (int)((bestAsk - bestBid) / tick + 0.5);
                    /*if (arrivalSpreadInTicks !=1)
                        continue;
                     * */
                    /*double ftind = buySell.Equals("Buy") ? (double)bestAskSize / (bestBidSize + bestAskSize) : (double)bestBidSize / (bestBidSize + bestAskSize);
                    if ((ftind >= 0.1) || (ftind < 0))
                        continue;
                     * */
                    int arrivalTouchSize = bestBidSize + bestAskSize;
                    double midPrice = (bestBid + bestAsk) / 2.0;
                    double NewMidPrice = (bestBid + bestAsk) / 2.0;
                    if (buySell.Equals("Buy"))
                    {
                        if (double.Parse(firstChildSignalBBODict[key][5]) > 0)
                        {
                            NewMidPrice = bestBid;
                        }
                        else if (double.Parse(firstChildSignalBBODict[key][5]) < 0)
                        {
                            NewMidPrice = bestAsk;
                        }
                    }
                    else
                    {
                        if (double.Parse(firstChildSignalBBODict[key][5]) > 0)
                        {
                            NewMidPrice = bestAsk;
                        }
                        else if (double.Parse(firstChildSignalBBODict[key][5]) < 0)
                        {
                            NewMidPrice = bestBid;
                        }
                    }

                    int arrivalFarTouchSize = buySell.Equals("Buy") ? bestAskSize : bestBidSize;

                    double firstFilledSlippage = 0;

                    if ((filledSize > 0) && (filledFirstPrice > 0) && (signalSummary.ChildType.Equals("Float") ||
                        childTypeValue.Equals("Peg")))
                    {
                        firstFilledSlippage = buySell.Equals("Buy") ? -filledSize * (filledFirstPrice - midPrice) / tick : filledSize * (filledFirstPrice - midPrice) / tick;
                    }

                    //double slippage = 0;
                    double slippageInTicks = 0;
                    double slippageInUSD = 0;
                    int masterSignalSize = 0;
                    double theorecticalSlippageInTicks = 0;
                    double duration = 0;

                    int overfilledsize = 0;

                    int CrossSniperSentFarTouch = 0;
                    int CrossSniperFilledFarTouch = 0;
                    int FTCrossSniperSentFarTouch = 0;
                    int FTCrossSniperFilledFarTouch = 0;
                    int FilledFarTouch = 0;

                    int CrossSniperSentFarTouch1 = 0;
                    int CrossSniperFilledFarTouch1 = 0;
                    int FTCrossSniperSentFarTouch1 = 0;
                    int FTCrossSniperFilledFarTouch1 = 0;
                    int FilledFarTouch1 = 0;

                    int fillBetterMidGT1Spread = 0;
                    int fillBetterMid1Spread = 0;
                    int fillAtMid = 0;
                    int fillWorseMid1Spread = 0;
                    int fillWorseMid2Spread = 0;
                    int fillWorseMidGT2Spread = 0;
                    double fillWorseMidGT2SpreadSlippageInTicks = 0;

                    int farTouchRatio = 0;
                    double nearTouchRatio = 0;
                    double bboRatio = 0;
                    double exceedFarTouchSize = 0;

                    double slippageNewInTicks = 0;

                    double slippageNTInTicks = 0;
                    double slippageFTInTicks = 0;

                    double slippageCloseInTicks = 0;

                    int[] filledSizeInSeconds = new int[10];


                    if (arrivalTouchSize <= 0 || arrivalSpreadInTicks <= 0)
                    {
                        continue;
                    }

                    // calculate slippage and fill distribution
                    foreach (var bboLine in masterSignalBBOs)
                    {
                        var filledPrice = bboLine.FilledPrice;
                        var filledLots = bboLine.FilledSize;
                        var filledTime = bboLine.FilledTime;

                        masterSignalSize += filledLots;

                        if (!filledTime.Equals(""))
                        {
                            // calucate fill distribution w.r.t. time from trigger
                            duration = Math.Max(duration, (filledTime.Subtract(triggerTime)).TotalSeconds);

                            int seconds = Math.Max(0, (int)(filledTime.Subtract(triggerTime).TotalSeconds));

                            for (int j = seconds; j < 10; ++j)
                            {
                                filledSizeInSeconds[j] += filledLots;
                            }
                        }

                        if (buySell.Equals("Buy"))
                        {
                            slippageInTicks += -filledLots * (filledPrice - midPrice) / tick;

                            slippageNewInTicks += -filledLots * (filledPrice - NewMidPrice) / tick;

                            slippageNTInTicks += -filledLots * (filledPrice - bestBid) / tick;

                            slippageFTInTicks += -filledLots * (filledPrice - bestAsk) / tick;

                            if (algoName.Contains("TWAP"))
                            {
                                slippageCloseInTicks += -filledLots * (filledPrice - dayClosePrice) / tick;
                            }

                            if (filledPrice < (NewMidPrice - arrivalSpreadInTicks * tick) - 0.1 * tick)
                            {
                                fillBetterMidGT1Spread += filledLots;
                            }
                            else if ((filledPrice >= (NewMidPrice - arrivalSpreadInTicks * tick) - 0.1 * tick)
                                && (filledPrice < (NewMidPrice - 0.1 * tick)))
                            {
                                fillBetterMid1Spread += filledLots;
                            }
                            else if ((filledPrice >= (NewMidPrice - 0.1 * tick))
                                && (filledPrice <= (NewMidPrice + 0.1 * tick)))
                            {
                                fillAtMid += filledLots;
                            }
                            else if ((filledPrice > (NewMidPrice + 0.1 * tick))
                                && (filledPrice <= (NewMidPrice + arrivalSpreadInTicks * tick) + 0.1 * tick))
                            {
                                fillWorseMid1Spread += filledLots;
                            }
                            else if ((filledPrice > (NewMidPrice + arrivalSpreadInTicks * tick) + 0.1 * tick)
                                && (filledPrice <= (NewMidPrice + 2 * arrivalSpreadInTicks * tick) + 0.1 * tick))
                            {
                                fillWorseMid2Spread += filledLots;
                            }
                            else if (filledPrice > (NewMidPrice + 2 * arrivalSpreadInTicks * tick) + 0.1 * tick)
                            {
                                fillWorseMidGT2Spread += filledLots;
                                fillWorseMidGT2SpreadSlippageInTicks += -filledLots * (filledPrice - NewMidPrice) / tick;
                            }

                        }
                        else
                        {
                            slippageInTicks += filledLots * (filledPrice - midPrice) / tick;

                            slippageNewInTicks += filledLots * (filledPrice - NewMidPrice) / tick;

                            slippageNTInTicks += filledLots * (filledPrice - bestAsk) / tick;

                            slippageFTInTicks += filledLots * (filledPrice - bestBid) / tick;

                            if (algoName.Contains("TWAP"))
                            {
                                slippageCloseInTicks += filledLots * (filledPrice - dayClosePrice) / tick;
                            }

                            if (filledPrice > (NewMidPrice + arrivalSpreadInTicks * tick) + 0.1 * tick)
                            {
                                fillBetterMidGT1Spread += filledLots;
                            }
                            else if ((filledPrice <= (NewMidPrice + arrivalSpreadInTicks * tick) + 0.1 * tick)
                                && (filledPrice > (NewMidPrice + 0.1 * tick)))
                            {
                                fillBetterMid1Spread += filledLots;
                            }
                            else if ((filledPrice <= (NewMidPrice + 0.1 * tick))
                                && (filledPrice >= (NewMidPrice - 0.1 * tick)))
                            {
                                fillAtMid += filledLots;
                            }
                            else if ((filledPrice < (NewMidPrice - 0.1 * tick))
                                && (filledPrice >= (NewMidPrice - arrivalSpreadInTicks * tick) - 0.1 * tick))
                            {
                                fillWorseMid1Spread += filledLots;
                            }
                            else if ((filledPrice < (NewMidPrice - arrivalSpreadInTicks * tick) - 0.1 * tick)
                                && (filledPrice >= (NewMidPrice - 2 * arrivalSpreadInTicks * tick) - 0.1 * tick))
                            {
                                fillWorseMid2Spread += filledLots;
                            }
                            else if (filledPrice < (NewMidPrice - 2 * arrivalSpreadInTicks * tick) - 0.1 * tick)
                            {
                                fillWorseMidGT2Spread += filledLots;
                                fillWorseMidGT2SpreadSlippageInTicks += -filledLots * (NewMidPrice - filledPrice) / tick;
                            }

                        }

                    }

                    // calculate the overfill size
                    if ((strategy.Equals("HW1") || strategy.Equals("AC11") || strategy.Equals("BHL1") || algoName.Contains("TWAP")) && (tagMaxPos > 0))
                    {
                        overfilledsize = 0;
                    }
                    else
                    {
                        overfilledsize = Math.Max(0, masterSignalSize - tagMaxPos);
                    }

                    // calculate far touch ratio and theorectical slippage
                    if (buySell.Equals("Buy"))
                    {

                        //farTouchRatio = masterSignalSize <= bestAskSize ? 0 : (masterSignalSize -bestAskSize) / (double)masterSignalSize;
                        //exceedFarTouchSize = masterSignalSize <= bestAskSize ? 0 : (masterSignalSize - bestAskSize);
                        farTouchRatio = masterSignalSize <= bestAskSize ? 0 : 1;
                        nearTouchRatio = masterSignalSize / (double)bestBidSize;
                        bboRatio = masterSignalSize / (double)(bestBidSize + bestAskSize);
                        exceedFarTouchSize = Math.Max(0, masterSignalSize - bestAskSize);

                        double farTouchRation2 = (double)bestAskSize / (bestBidSize + bestAskSize);
                        if (lowSpreadThresholds.ContainsKey(prodName) && highSpreadThresholds.ContainsKey(prodName))
                        {
                            if (arrivalSpreadInTicks <= 1 && farTouchRation2 < lowSpreadThresholds[prodName] || arrivalSpreadInTicks > 1 && farTouchRation2 < highSpreadThresholds[prodName])
                            {
                                theorecticalSlippageInTicks = -(bestAsk - midPrice) / tick * masterSignalSize;
                            }
                            else
                            {
                                theorecticalSlippageInTicks = slippageInTicks;
                            }
                        }
                        else
                        {
                            theorecticalSlippageInTicks = slippageInTicks;
                        }
                    }
                    else
                    {
                        //farTouchRatio = masterSignalSize <= bestBidSize ? 0 : (masterSignalSize - bestBidSize) / (double)masterSignalSize;
                        //exceedFarTouchSize = masterSignalSize <= bestBidSize ? 0 : (masterSignalSize - bestBidSize);
                        farTouchRatio = masterSignalSize <= bestBidSize ? 0 : 1;
                        nearTouchRatio = masterSignalSize / (double)bestAskSize;
                        bboRatio = masterSignalSize / (double)(bestBidSize + bestAskSize);
                        exceedFarTouchSize = Math.Max(0, masterSignalSize - bestBidSize);

                        double farTouchRatio2 = (double)bestBidSize / (bestBidSize + bestAskSize);
                        if (lowSpreadThresholds.ContainsKey(prodName) && highSpreadThresholds.ContainsKey(prodName))
                        {
                            if (arrivalSpreadInTicks <= 1 && farTouchRatio2 < lowSpreadThresholds[prodName] || arrivalSpreadInTicks > 1 && farTouchRatio2 < highSpreadThresholds[prodName])
                            {

                                theorecticalSlippageInTicks = (bestBid - midPrice) / tick * masterSignalSize;
                            }
                            else
                            {
                                theorecticalSlippageInTicks = slippageInTicks;
                            }
                        }
                        else
                        {
                            theorecticalSlippageInTicks = slippageInTicks;
                        }
                    }


                    // slippage in USD
                    slippageInUSD = slippageInTicks * tick * pointValue * exchangeRate;


                    if (double.IsNaN(slippageInTicks))
                    {
                        continue;
                    }

                    // calculate calc price latency
                    var masterSignalLatencies = signalLatenciesByDate.Where(value => value.SignalID == masterOrderId);
                    var latencyList = masterSignalLatencies.Select(value => value.CalcPriceLatency).ToList();
                    var calcPriceLatency = (latencyList.Count > 0) ? latencyList.Average() : 0;

                    // add master signal line
                    var signalFileLine = dailySignalFileLines.FirstOrDefault(item => item.Product == prodName && item.Algo == algoName.Replace("New", "")
                        && item.SignalKey == dateString + "-" + masterOrderId + "-" + machineName + "-" + strategy + "-" + risk.ToString("0.############"));
                    if (signalFileLine == null)
                    {
                        bool isFloat = false;
                        bool unFullFill = false;
                        bool isNormalFloat = true;

                        if (childTypeValue.Equals("Float") ||
                            childTypeValue.Equals("Peg") ||
                            childTypeValue.Equals("SupplementFloat") ||
                            childTypeValue.Equals("SepSniperFloat") ||
                            childTypeValue.Equals("MidSniper") ||
                            childTypeValue.Equals("SLMidSniper") ||
                            childTypeValue.Equals("CrossMidSniper"))
                        {
                            isFloat = true;
                        }
                        if (childTypeValue.Equals("Float") ||
                            childTypeValue.Equals("Peg"))
                        {
                            isNormalFloat = true;
                        }
                        if (filledSize < size)
                        {
                            unFullFill = true;
                        }

                        signalFileLine = new SingleFileColumnModel()
                        {
                            Date = date,
                            Product = prodName, //"Product"
                            Algo = algoName.Replace("New", ""), //"Algo"
                            SignalKey = dateString + "-" + masterOrderId + "-" + machineName + "-" + strategy + "-" + risk.ToString("0.############"), //"SignalKey"
                            Size = masterSignalSize, //"Size"
                            NewSlippageInTicks = slippageNewInTicks, //"NewSlippageInTicks"
                            SlippageInTicks = slippageInTicks, //"SlippageInTicks"
                            NTSlippageInTicks = slippageNTInTicks, //"NTSlippageInTicks"
                            SlippageAsSpreadRatio = (masterSignalSize * arrivalSpreadInTicks), //"SlippageAsSpreadRatio"
                            SpreadInTicks = (masterSignalSize * arrivalSpreadInTicks), //"SpreadInTicks"
                            HighSpreadSize = (arrivalSpreadInTicks > 1) ? masterSignalSize : 0,
                            HighSpreadInTicks = (arrivalSpreadInTicks > 1) ? (masterSignalSize * arrivalSpreadInTicks) : 0,
                            SlippageClosePriceInTicks = slippageCloseInTicks,
                            SlippageInUSDPerTrade = slippageInUSD, //"SlippageClosePriceInTicks" 
                            TotalSlippageInUSD = slippageInUSD, //"TotalSlippageInUSD",
                            SlippageAsAUMRatio = (masterSignalSize * aumInUSD), //"SlippageAsAUMRatio",
                            FiveLargestSlippageInTicks = (slippageInTicks / masterSignalSize),//"5LargestSlippageInTicks",
                            TheoreticalOptimalSlippageInTicks = theorecticalSlippageInTicks,//"TheoreticalOptimalSlippageInTicks",
                            DailyRangeInTicks = (masterSignalSize * averageDailyRange20DInTicks),//"DailyRangeInTicks",
                            SlippageAsDailyRangeRatio = (masterSignalSize * averageDailyRange20DInTicks),//"SlippageAsDailyRangeRatio",
                            OverFilledSize = overfilledsize,//"OverFilledSize",
                            OverFilledRatio = overfilledsize,//"OverFilledRatio",
                            AverageDuration = (duration * masterSignalSize),//"AverageDuration",
                            MaxDuration = duration,//"MaxDuration",
                            FarTouchRatio = farTouchRatio,//"FarTouchRatio",
                            NearTouchRatio = nearTouchRatio,//"NearTouchRatio",
                            BBORatio = bboRatio, //"BBORatio", 
                            ExceedingFarTouchRatio = exceedFarTouchSize, //"ExceedingFarTouchRatio",
                            FillBetterMidGT1Spread = fillBetterMidGT1Spread,//"FillBetterMidGT1Spread",
                            FillBetterMid1Spread = fillBetterMid1Spread,//"FillBetterMid1Spread",
                            FillAtMid = fillAtMid,//"FillAtMid",
                            FillWorseMid1Spread = fillWorseMid1Spread,//"FillWorseMid1Spread"",
                            FillWorseMid2Spread = fillWorseMid2Spread,//"FillWorseMid2Spread",
                            FillWorseMidGT2Spread = fillWorseMidGT2Spread,//"FillWorseMidGT2Spread",
                            FillWorseMidGT2SpreadSlippageAsSpreadRatio = fillWorseMidGT2SpreadSlippageInTicks,//"FillWorseMidGT2SpreadSlippageAsSpreadRatio",                                                                                                   
                            FiveLargestCalcPriceLatency = calcPriceLatency,//"5LargestCalcPriceLatency", 
                            FloatFirstUnFullFilledSize = (isFloat && unFullFill) ? masterSignalSize : 0, //"FloatFirstUnFullFilledSize",
                            FloatFirstUnFullFilledSlippageInTicks = (isFloat && unFullFill) ? slippageInTicks : 0, //"FloatFirstUnFullFilledSlippageInTicks",
                            FloatFirstUnFullFilledSlippageAsSpreadRatio = (isFloat && unFullFill) ? (masterSignalSize * arrivalSpreadInTicks) : 0, //"FloatFirstUnFullFilledSlippageAsSpreadRatio",
                            FloatFirstLowSpreadUnFilledSize = (isNormalFloat && unFullFill && arrivalSpreadInTicks == 1) ? (masterSignalSize - filledSize) : 0, //"FloatFirstLowSpreadUnFilledSize",
                            FloatFirstLowSpreadUnFilledSlippage = (isNormalFloat && unFullFill && arrivalSpreadInTicks == 1) ? (slippageInTicks - firstFilledSlippage) : 0, //"FloatFirstLowSpreadUnFilledSlippage",
                            FloatFirstHighSpreadUnFilledSize = (isNormalFloat && unFullFill && arrivalSpreadInTicks > 1) ? (masterSignalSize - filledSize) : 0, //"FloatFirstLowSpreadUnFilledSize",
                            FloatFirstHighSpreadUnFilledSlippage = (isNormalFloat && unFullFill && arrivalSpreadInTicks > 1) ? (slippageInTicks - firstFilledSlippage) : 0, //"FloatFirstLowSpreadUnFilledSlippage",
                            FloatFirstFullFilledSize = (isFloat && !unFullFill) ? masterSignalSize : 0, //"FloatFirstFullFilledSize",
                            FloatFirstFullFilledSlippageInTicks = (isFloat && !unFullFill) ? slippageInTicks : 0, //"FloatFirstFullFilledSlippageInTicks",
                            FloatFirstFullFilledSlippageAsSpreadRatio = (isFloat && !unFullFill) ? (masterSignalSize * arrivalSpreadInTicks) : 0, //"FloatFirstFullFilledSlippageAsSpreadRatio",
                            SniperFirstUnFullFilledSize = (!isFloat && unFullFill) ? masterSignalSize : 0, //"SniperFirstUnFullFilledSize",
                            SniperFirstUnFullFilledSlippageInTicks = (!isFloat && unFullFill) ? slippageInTicks : 0, //"SniperFirstUnFullFilledSlippageInTicks",
                            SniperFirstUnFullFilledSlippageAsSpreadRatio = (!isFloat && unFullFill) ? (masterSignalSize * arrivalSpreadInTicks) : 0, //"SniperFirstUnFullFilledSlippageAsSpreadRatio",
                            SniperFirstFullFilledSize = (!isFloat && !unFullFill) ? masterSignalSize : 0, //"SniperFirstFullFilledSize",
                            SniperFirstFullFilledSlippageInTicks = (!isFloat && !unFullFill) ? slippageInTicks : 0, //"SniperFirstFullFilledSlippageInTicks",
                            SniperFirstFullFilledSlippageAsSpreadRatio = (!isFloat && !unFullFill) ? (masterSignalSize * arrivalSpreadInTicks) : 0, //"SniperFirstFullFilledSlippageAsSpreadRatio",   
                            FloatFirstSize = isFloat ? size : 0, //"FloatFirstSize", 
                            FloatFirstFillSize = isFloat ? filledSize : 0, //"FloatFirstFillSize", 
                            FloatFirstFillRate = isFloat ? (filledSize / (double)size) : 0, //"FloatFirstFillRate",
                            SniperFirstSize = !isFloat ? size : 0, // "SniperFirstSize", 
                            SniperFirstFillSize = !isFloat ? filledSize : 0, //"SniperFirstFillSize", 
                            SniperFirstFillRate = !isFloat ? (filledSize / (double)size) : 0, //"SniperFirstFillRate",
                            FillIn1S = filledSizeInSeconds[0],//"FillIn1S"
                            FillIn2S = filledSizeInSeconds[1],//"FillIn2S"
                            FillIn3S = filledSizeInSeconds[2],//"FillIn3S"
                            FillIn4S = filledSizeInSeconds[3],//"FillIn4S"
                            FillIn5S = filledSizeInSeconds[4],//"FillIn5S"
                            FillIn6S = filledSizeInSeconds[5],//"FillIn6S"
                            FillIn7S = filledSizeInSeconds[6],//"FillIn7S"
                            FillIn8S = filledSizeInSeconds[7],//"FillIn8S"
                            FillIn9S = filledSizeInSeconds[8],//"FillIn9S"
                            FillIn10S = filledSizeInSeconds[9],//"FillIn10S"
                            RatioOfNormalCrossSniperFilledAtFT = FilledFarTouch,//"RatioOfNormalCrossSniperFilledAtFT",
                            RatioOfFTReductionCrossSniperFilledAtFT = FilledFarTouch,//"RatioOfFTReductionCrossSniperFilledAtFT",
                            RatioOfNormalCrossSniperFilledAtFT1 = FilledFarTouch1,//"RatioOfNormalCrossSniperFilledAtFT+1",
                            RatioOfFTReductionCrossSniperFilledAtFT1 = FilledFarTouch1,//"RatioOfFTReductionCrossSniperFilledAtFT+1",
                            Contract = contract,//"Contract",
                            TriggerTime = triggerTime, //"TriggerTime",
                            ArrivalBestBid = bestBid,//"ArrivalBestBid",
                            ArrivalBestAsk = bestAsk,//"ArrivalBestAsk",
                            ArrivalBestBidSize = bestBidSize,//"ArrivalBestBidSize",
                            ArrialBestAskSize = bestAskSize,//"ArrialBestAskSize",
                            FirstBestBid = childOrderBestBid, //"FirstBestBid",
                            FirstBestAsk = childOrderBestAsk,//"FirstBestAsk",
                            FirstBestBidSize = childOrderBestBidSize,//"FirstBestBidSize",
                            FirstBestAskSize = childOrderBestAskSize//"FirstBestAskSize",
                        };
                        dailySignalFileLines.Add(signalFileLine);
                    }

                    // accumulate master signal fill size to check with vfix trades size
                    signalFileLine.SignalSummarySize += filledSize;

                    if (buySell.Equals("Buy"))
                    {
                        if (childTypeValue.Equals("CrossSniper"))
                        {
                            if (Math.Abs(orderPrice - bestAsk) < 0.1 * tick)
                            {
                                CrossSniperSentFarTouch += size;
                            }
                            if (Math.Abs(filledFirstPrice - bestAsk) < 0.1 * tick)
                            {
                                CrossSniperFilledFarTouch += filledSize;
                            }
                            if (orderPrice > bestAsk + 0.5 * tick && orderPrice < bestAsk + arrivalSpreadInTicks * tick + 0.5 * tick)
                            {
                                CrossSniperSentFarTouch1 += size;
                            }
                            if (filledFirstPrice > bestAsk + 0.5 * tick && filledFirstPrice < bestAsk + arrivalSpreadInTicks * tick + 0.5 * tick)
                            {
                                CrossSniperFilledFarTouch1 += filledSize;
                            }
                        }

                        if (childTypeValue.Equals("FTReductionCrossSniper"))
                        {
                            if (Math.Abs(orderPrice - bestAsk) < 0.1 * tick)
                            {
                                FTCrossSniperSentFarTouch += size;
                            }
                            if (Math.Abs(filledFirstPrice - bestAsk) < 0.1 * tick)
                            {
                                FTCrossSniperFilledFarTouch += filledSize;
                            }
                            if (orderPrice > bestAsk + 0.5 * tick && orderPrice < bestAsk + arrivalSpreadInTicks * tick + 0.5 * tick)
                            {
                                FTCrossSniperSentFarTouch1 += size;
                            }
                            if (filledFirstPrice > bestAsk + 0.5 * tick && filledFirstPrice < bestAsk + arrivalSpreadInTicks * tick + 0.5 * tick)
                            {
                                FTCrossSniperFilledFarTouch1 += filledSize;
                            }
                        }
                    }
                    else
                    {
                        if (childTypeValue.Equals("CrossSniper"))
                        {
                            if (Math.Abs(orderPrice - bestBid) < 0.1 * tick)
                            {
                                CrossSniperSentFarTouch += size;
                            }
                            if (Math.Abs(filledFirstPrice - bestBid) < 0.1 * tick)
                            {
                                CrossSniperFilledFarTouch += filledSize;
                            }
                            if (orderPrice < bestBid - 0.5 * tick && filledFirstPrice > bestBid - arrivalSpreadInTicks * tick - 0.5 * tick)
                            {
                                CrossSniperSentFarTouch1 += size;
                            }
                            if (filledFirstPrice < bestBid - 0.5 * tick && filledFirstPrice > bestBid - arrivalSpreadInTicks * tick - 0.5 * tick)
                            {
                                CrossSniperFilledFarTouch1 += filledSize;
                            }
                        }

                        if (childTypeValue.Equals("FTReductionCrossSniper"))
                        {
                            if (Math.Abs(orderPrice - bestBid) < 0.1 * tick)
                            {
                                FTCrossSniperSentFarTouch += size;
                            }
                            if (Math.Abs(filledFirstPrice - bestBid) < 0.1 * tick)
                            {
                                FTCrossSniperFilledFarTouch += filledSize;
                            }
                            if (orderPrice < bestBid - 0.5 * tick && filledFirstPrice > bestBid - arrivalSpreadInTicks * tick - 0.5 * tick)
                            {
                                FTCrossSniperSentFarTouch1 += size;
                            }
                            if (filledFirstPrice < bestBid - 0.5 * tick && filledFirstPrice > bestBid - arrivalSpreadInTicks * tick - 0.5 * tick)
                            {
                                FTCrossSniperFilledFarTouch1 += filledSize;
                            }
                        }
                    }

                    signalFileLine.LotsOfNormalCrossSniperSentAtFT += CrossSniperSentFarTouch;
                    signalFileLine.LotsOfNormalCrossSniperFilledAtFT += CrossSniperFilledFarTouch;
                    signalFileLine.LotsOfFTReductionCrossSniperSentAtFT += FTCrossSniperSentFarTouch;
                    signalFileLine.LotsOfFTReductionCrossSniperFilledAtFT += FTCrossSniperFilledFarTouch;
                    signalFileLine.LotsOfNormalCrossSniperSentAtFT1 += CrossSniperSentFarTouch1;
                    signalFileLine.LotsOfNormalCrossSniperFilledAtFT1 += CrossSniperFilledFarTouch1;
                    signalFileLine.LotsOfFTReductionCrossSniperSentAtFT1 += FTCrossSniperSentFarTouch1;
                    signalFileLine.LotsOfFTReductionCrossSniperFilledAtFT1 += FTCrossSniperFilledFarTouch1;

                    // calculate signal submit and cancel latency
                    double submitLatency = 0;
                    double cancelLatency = 0;
                    var childSignalLatency = signalLatenciesByDate.FirstOrDefault(value => value.SignalID == signalId);
                    if (childSignalLatency != null)
                    {
                        submitLatency = childSignalLatency.SubmitLatency;
                        cancelLatency = childSignalLatency.CancelLatency;
                    }

                    if (submitLatency > 0)
                    {
                        signalFileLine.NumOfOrderSubmitted++;
                        if (submitLatency > signalFileLine.FiveLargestOrderSubmitLatency)
                        {
                            signalFileLine.FiveLargestOrderSubmitLatency = submitLatency;
                        }
                        signalFileLine.AverageOrderSubmitLatency = (signalFileLine.AverageOrderSubmitLatency + submitLatency);
                    }

                    if (cancelLatency > 0)
                    {
                        signalFileLine.NumOfOrderCancelled++;
                        if (cancelLatency > signalFileLine.FiveLargestOrderCancelLatency)
                        {
                            signalFileLine.FiveLargestOrderCancelLatency = cancelLatency;
                        }
                        signalFileLine.AverageOrderCancelLatency = (signalFileLine.AverageOrderCancelLatency + cancelLatency);
                    }

                    // calculate cancel cost
                    int k = 0;
                    while (k < unFullFilledOrders.Count)
                    {
                        if (unFullFilledOrders[k][0].Equals(masterOrderId.ToString()) && !unFullFilledOrders[k][1].Equals(signalId.ToString()))
                        {
                            double bidPrice = double.Parse(unFullFilledOrders[k][2]);
                            double askPrice = double.Parse(unFullFilledOrders[k][3]);
                            DateTime time = DateTime.Parse(unFullFilledOrders[k][7]);

                            double lag = childOrderStartTime.Subtract(time).TotalMilliseconds;
                            double move = (int)(Math.Abs((childOrderBestBid + childOrderBestAsk) / 2.0 - (bidPrice + askPrice) / 2.0) / tick + 0.5);
                            if (lag >= 0)
                            {
                                signalFileLine.NumOfUnFullFilledOrders += 1;
                                signalFileLine.TimeLag += lag;
                                signalFileLine.PriceMove += move;
                                unFullFilledOrders.RemoveAt(k);
                            }
                            else
                            {
                                k++;
                            }
                        }
                        else
                        {
                            k++;
                        }
                    }

                    // process sniper and float orders, calculate different kinds of fill rates
                    if (childTypeValue.Equals("Sniper") ||
                        childTypeValue.Equals("SLSniper") ||
                        childTypeValue.Equals("SepSniperCross") ||
                        childTypeValue.Equals("CrossSniper") ||
                        childTypeValue.Equals("CrossSepSniper") ||
                        childTypeValue.Equals("FTReductionCrossSniper") ||
                        childTypeValue.Equals("ExceedFTSizeCrossSniper") ||
                        childTypeValue.Equals("Sweep"))
                    {
                        // num of sniper orders
                        signalFileLine.NumOfSniperOrders++;

                        if (childTypeValue.Equals("ExceedFTSizeCrossSniper"))
                        {
                            // fill rate
                            signalFileLine.FTSizeExceedSniperSize += size;
                            signalFileLine.FTSizeExceedSniperFillSize += filledSize;
                            signalFileLine.FTSizeExceedSniperFillRate = signalFileLine.FTSizeExceedSniperFillSize / (double)signalFileLine.FTSizeExceedSniperSize;
                        }

                        // cross sniper fill rate and slippage after 200ms
                        if (childTypeValue.Equals("CrossSniper") || childTypeValue.Equals("FTReductionCrossSniper") || childTypeValue.Equals("CrossSepSniper"))
                        {
                            // fill rate
                            signalFileLine.CrossSniperSize += size;
                            signalFileLine.CrossSniperFillSize += filledSize;
                            signalFileLine.CrossSniperFillRate = signalFileLine.CrossSniperFillSize / (double)signalFileLine.CrossSniperSize;

                            // cross sniper slippage after 200ms
                            var crossSniperBBOs = masterSignalBBOs.Where(bbo => bbo.Date.Equals(date) && bbo.MasterSignalID.Equals(masterOrderId) && bbo.MachineName.Equals(machineName) && bbo.ChildSignalID.Equals(signalId)).ToList();
                            if (crossSniperBBOs != null && crossSniperBBOs.Count > 0)
                            {
                                int crossSniperFillSizeAfterWhile = 0;
                                double crossSniperSlippageAfterWhile = 0;

                                foreach (var bboLine in crossSniperBBOs)
                                {
                                    var filledPrice = bboLine.FilledPrice;
                                    var filledLots = bboLine.FilledSize;
                                    var filledTime = bboLine.FilledTime;
                                    var submitTime = bboLine.SubmitTime;

                                    if (filledTime != DateTime.MinValue && submitTime != DateTime.MinValue)
                                    {
                                        if (filledTime.Subtract(submitTime).TotalMilliseconds > 200)
                                        {
                                            crossSniperFillSizeAfterWhile += filledLots;
                                            if (buySell.Equals("Buy"))
                                            {
                                                crossSniperSlippageAfterWhile += -filledLots * (filledPrice - orderPrice) / tick;
                                            }
                                            else
                                            {
                                                crossSniperSlippageAfterWhile += filledLots * (filledPrice - orderPrice) / tick;
                                            }
                                        }
                                    }

                                }

                                signalFileLine.CrossSniperFillSizeAfterWhile += crossSniperFillSizeAfterWhile;
                                signalFileLine.CrossSniperAfterWhileSlippageInTicks += crossSniperSlippageAfterWhile;
                            }
                        }

                        // sweep fill rate
                        if (childTypeValue.Equals("Sweep") && orderPrice > 0 && childOrderSpreadInTicks > 1)
                        {
                            int interval = 0; //0 at FT, -1 below FT, 1 above FT

                            if (buySell.Equals("Buy"))
                            {
                                if (Math.Abs(orderPrice - bestAsk) < tick / 2)
                                {
                                    interval = 0;
                                }
                                else if (orderPrice > bestAsk + tick / 2)
                                {
                                    interval = 1;
                                }
                                else
                                {
                                    interval = -1;
                                }
                            }
                            else
                            {
                                if (Math.Abs(orderPrice - bestBid) < tick / 2)
                                {
                                    interval = 0;
                                }
                                else if (orderPrice < bestBid - tick / 2)
                                {
                                    interval = 1;
                                }
                                else
                                {
                                    interval = -1;
                                }
                            }

                            if (interval == 0)
                            {
                                // at far touch
                                signalFileLine.SweepAtFTSize += size;
                                signalFileLine.SweepAtFTFillSize += filledSize;
                                signalFileLine.SweepAtFTFillRate = filledSize / (double)signalFileLine.SweepAtFTSize;
                            }
                            else if (interval == 1)
                            {
                                // above far touch
                                signalFileLine.SweepAboveFTSize += size;
                                signalFileLine.SweepAboveFTFillSize += filledSize;
                                signalFileLine.SweepAboveFTFillRate = signalFileLine.SweepAboveFTFillSize / (double)signalFileLine.SweepAboveFTSize;
                            }
                            else
                            {
                                // below far touch
                                signalFileLine.SweepBelowFTSize += size;
                                signalFileLine.SweepBelowFTFillSize += filledSize;
                                signalFileLine.SweepBelowFTFillRate = signalFileLine.SweepBelowFTFillSize / (double)signalFileLine.SweepBelowFTSize;
                            }

                            // first sweep fill rate
                            key = dateString + "-" + masterOrderId.ToString() + "-" + machineName;
                            if (!firstSweepSignalDict.ContainsKey(key))
                            {
                                firstSweepSignalDict.Add(key, 1);
                                signalFileLine.OneSweepSize += size;
                                signalFileLine.OneSweepFillSize += filledSize;
                                signalFileLine.OneSweepFillRate = signalFileLine.OneSweepFillSize / (double)signalFileLine.OneSweepSize;
                            }

                        }

                        // sniper new near touch
                        int sniperNewNTSize = 0;
                        int sniperNewNTFilledSize = 0;
                        var childOrderBBOs = masterSignalBBOs.Where(bbo => bbo.Date.Equals(date) && bbo.MasterSignalID.Equals(masterOrderId) && bbo.MachineName.Equals(machineName) && bbo.ChildSignalID.Equals(signalId)).ToList();
                        if (childOrderBBOs != null && childOrderBBOs.Count > 0)
                        {
                            sniperNewNTSize = size;
                            sniperNewNTFilledSize = 0;
                            foreach (var bboLine in childOrderBBOs)
                            {
                                var filledPrice = bboLine.FilledPrice;
                                var filledLots = bboLine.FilledSize;
                                var filledTime = bboLine.FilledTime;
                                var submitTime = bboLine.SubmitTime;

                                if (filledTime != DateTime.MinValue && submitTime != DateTime.MinValue)
                                {
                                    if (filledTime.Subtract(submitTime).TotalMilliseconds > 500)
                                    {
                                        sniperNewNTFilledSize += filledLots;
                                    }
                                    else
                                    {
                                        sniperNewNTSize -= filledLots;
                                    }
                                }

                            }

                        }
                        else
                        {
                            if (!childOrderStartTime.Equals(DateTime.MinValue) && !childOrderEndTime.Equals(DateTime.MinValue) && childOrderEndTime.Subtract(childOrderStartTime).TotalMilliseconds > 500)
                            {
                                sniperNewNTSize = size;
                                sniperNewNTFilledSize = 0;
                            }
                        }

                        signalFileLine.SniperNewNTSize += sniperNewNTSize;
                        signalFileLine.SniperNewNTFillSize += sniperNewNTFilledSize;
                        signalFileLine.SniperNewNTFillRate = signalFileLine.SniperNewNTSize == 0 ? 0 : signalFileLine.SniperNewNTFillSize / (double)signalFileLine.SniperNewNTSize;

                        // all sniper
                        signalFileLine.AllSniperSize += size;
                        signalFileLine.AllSniperFillSize += filledSize;
                        signalFileLine.AllSniperFillRate = signalFileLine.AllSniperFillSize / (double)signalFileLine.AllSniperSize;

                        key = dateString + "-" + masterOrderId.ToString() + "-" + machineName;
                        if (!firstSniperSignalDict.ContainsKey(key))
                        {
                            firstSniperSignalDict.Add(key, 1);

                            // first sniper
                            signalFileLine.OneSniperSize += size;
                            signalFileLine.OneSniperFillSize += filledSize;
                            signalFileLine.OneSniperFillRate = signalFileLine.OneSniperFillSize / (double)signalFileLine.OneSniperSize;

                            // 1SniperFillAsWholeSize
                            signalFileLine.OneSniperFillAsWholeSize += filledSize;
                        }
                    }
                    else if (childTypeValue.Equals("Float") ||
                        childTypeValue.Equals("Peg") ||
                        childTypeValue.Equals("SupplementFloat") ||
                        childTypeValue.Equals("SepSniperFloat") ||
                        childTypeValue.Equals("MidSniper") ||
                        childTypeValue.Equals("SLMidSniper") ||
                        childTypeValue.Equals("CrossMidSniper"))
                    {
                        // num of float orders
                        if (childTypeValue.Equals("Float"))
                        {
                            signalFileLine.NumOfFloatOrders++;
                        }
                        else
                        {
                            signalFileLine.NumOfPegOrders++;
                        }

                        // num of partial fill orders
                        signalFileLine.NumOfPartialFillOrders += (filledSize > 0 && filledSize < size) ? 1 : 0;

                        bool lowSpread = false;
                        int farTouchSize = 0;

                        // low spread and high spread
                        if (!(double.IsNaN(childOrderBestBid) || double.IsNaN(childOrderBestAsk) || childOrderBestBidSize <= 0 || childOrderBestAskSize <= 0))
                        {
                            // use child order spread
                            if (childOrderSpreadInTicks <= 1)
                            {
                                lowSpread = true;
                            }
                            else
                            {
                                lowSpread = false;
                            }
                            farTouchSize = childFarTouchSize;
                        }
                        else
                        {
                            // use arrival spread
                            if (arrivalSpreadInTicks <= 1)
                            {
                                lowSpread = true;
                            }
                            else
                            {
                                lowSpread = false;
                            }
                            farTouchSize = arrivalFarTouchSize;
                        }

                        int NumOfBin = (int)Math.Floor(childIndicator * 10);

                        NumOfBin = Math.Max(0, Math.Min(9, NumOfBin));

                        if (!firstFloatSignalDict.ContainsKey(key))
                        {
                            signalFileLine.SetSentAndFillSize(NumOfBin, lowSpread, size, filledSize);
                        }
                        // by lots
                        if (lowSpread)
                        {

                            signalFileLine.LowSpreadFloatSizeByLots += size;
                            signalFileLine.LowSpreadFloatFillSizeByLots += filledSize;
                            signalFileLine.LowSpreadFloatFillRateByLots = signalFileLine.LowSpreadFloatFillSizeByLots / (double)signalFileLine.LowSpreadFloatSizeByLots;
                        }
                        else
                        {
                            signalFileLine.HighSpreadFloatSizeByLots += size;
                            signalFileLine.HighSpreadFloatFillSizeByLots += filledSize;
                            signalFileLine.HighSpreadFloatFillRateByLots = signalFileLine.HighSpreadFloatFillSizeByLots / (double)signalFileLine.HighSpreadFloatSizeByLots;
                        }

                        // by FT size
                        if (lowSpread)
                        {
                            signalFileLine.LowSpreadFloatSizeFT++;
                            signalFileLine.LowSpreadFloatFillSizeFT += Math.Min(1, filledSize / (double)(Math.Min(size, farTouchSize)));
                            signalFileLine.LowSpreadFloatFillRateFT = signalFileLine.LowSpreadFloatFillSizeFT / (double)signalFileLine.LowSpreadFloatSizeFT;
                        }
                        else
                        {
                            signalFileLine.HighSpreadFloatSizeFT++;
                            signalFileLine.HighSpreadFloatFillSizeFT += Math.Min(1, filledSize / (double)(Math.Min(size, farTouchSize)));
                            signalFileLine.HighSpreadFloatFillRateFT = signalFileLine.HighSpreadFloatFillSizeFT / (double)signalFileLine.HighSpreadFloatSizeFT;
                        }

                        // by num of orders
                        if (lowSpread)
                        {
                            signalFileLine.LowSpreadFloatSizeByNum++;
                            signalFileLine.LowSpreadFloatFillSizeByNum += filledSize > 0 ? 1 : 0;
                            signalFileLine.LowSpreadFloatFillRateByNum = signalFileLine.LowSpreadFloatFillSizeByNum / (double)signalFileLine.LowSpreadFloatSizeByNum;
                        }
                        else
                        {
                            signalFileLine.HighSpreadFloatSizeByNum++;
                            signalFileLine.HighSpreadFloatFillSizeByNum += filledSize > 0 ? 1 : 0;
                            signalFileLine.HighSpreadFloatFillRateByNum = signalFileLine.HighSpreadFloatFillSizeByNum / (double)signalFileLine.HighSpreadFloatSizeByNum;
                        }

                        // all float

                        // by lots
                        signalFileLine.AllFloatSizeByLots += size;
                        signalFileLine.AllFloatFillSizeByLots += filledSize;
                        signalFileLine.AllFloatFillRateByLots = signalFileLine.AllFloatFillSizeByLots / (double)signalFileLine.AllFloatSizeByLots;

                        // by FT size
                        signalFileLine.AllFloatSizeFT++;
                        signalFileLine.AllFloatFillSizeFT += Math.Min(1, filledSize / (double)(Math.Min(size, farTouchSize)));
                        signalFileLine.AllFloatFillRateFT = signalFileLine.AllFloatFillSizeFT / (double)signalFileLine.AllFloatSizeFT;

                        // by num of orders
                        signalFileLine.AllFloatSizeByNum++;
                        signalFileLine.AllFloatFillSizeByNum += filledSize > 0 ? 1 : 0;
                        signalFileLine.AllFloatFillRateByNum = signalFileLine.AllFloatFillSizeByNum / (double)signalFileLine.AllFloatSizeByNum;

                        // "1FloatFillAsWholeSize"
                        key = dateString + "-" + masterOrderId.ToString() + "-" + machineName;
                        if (!firstFloatSignalDict.ContainsKey(key))
                        {
                            signalFileLine.OneFloatFillAsWholeSize += filledSize;
                        }

                        // first to fifth float                                                        
                        if (!firstFloatSignalDict.ContainsKey(key))
                        {
                            firstFloatSignalDict.Add(key, 0);
                            signalFileLine.SetFloatSizeAndFill(0, size, filledSize);
                        }
                        else
                        {
                            // second,third,fourth,fifth float
                            firstFloatSignalDict[key]++;
                            int index = firstFloatSignalDict[key];
                            if (index < 5)
                            {
                                signalFileLine.SetFloatSizeAndFill(index, size, filledSize);
                            }
                        }


                        // fill rate of float order with fartouchratio > 0.25 but become < 0.25 before finish
                        if (dateString.Equals("20131024") && prodName.Equals("FV") && algoName.Equals("ClimbingGold") && machineName.Equals("QH69") && masterOrderId == 62)
                        {
                            int a = 1;
                        }

                        if (!(double.IsNaN(childOrderBestBid) || double.IsNaN(childOrderBestAsk) || childOrderBestBidSize <= 0 || childOrderBestAskSize <= 0))
                        {
                            double startFarTouchRatio = 0;
                            double NTPrice = 0;
                            if (buySell.Equals("Buy"))
                            {
                                startFarTouchRatio = (double)(childOrderBestAskSize) / (childOrderBestBidSize + childOrderBestAskSize);
                                NTPrice = childOrderBestBid;
                            }
                            else
                            {
                                startFarTouchRatio = (double)(childOrderBestBidSize) / (childOrderBestBidSize + childOrderBestAskSize);
                                NTPrice = childOrderBestAsk;
                            }
                            if (orderPrice <= 0)
                            {
                                orderPrice = NTPrice;
                            }

                            if (startFarTouchRatio > 0.25)
                            {
                                if (lowSpread)
                                {
                                    signalFileLine.LowSpreadFTRatioNeverDropFloatSize += size;
                                    signalFileLine.LowSpreadFTRatioNeverDropFloatFillSize += filledSize;
                                }
                                else
                                {
                                    signalFileLine.HighSpreadFTRatioNeverDropFloatSize += size;
                                    signalFileLine.HighSpreadFTRatioNeverDropFloatFillSize += filledSize;

                                }
                            }
                            else
                            {
                                if (lowSpread)
                                {
                                    signalFileLine.LowSpreadFTRatioNeverDropFloatSize += size;
                                    signalFileLine.LowSpreadFTRatioNeverDropFloatFillSize += filledSize;
                                }
                                else
                                {
                                    signalFileLine.HighSpreadFTRatioNeverDropFloatSize += size;
                                    signalFileLine.HighSpreadFTRatioNeverDropFloatFillSize += filledSize;
                                }
                            }
                        }
                    }
                }
            }

            foreach (var signalLine in dailySignalFileLines)
            {
                var algoReport = list.AlgoReport[signalLine.Product];
                UpdateAlgoReport(signalLine, algoReport);
            }

            // add today's master signal file lines
            list.Data.AddRange(dailySignalFileLines);
        }

        private void CalculateSlippageForDefault(string prodName, DateTime date, string machineName, string algoName, IEnumerable<BboModel> bboLines
                , double dayClosePrice, double tick, List<SingleFileColumnModel> dailySignalFileLines, double averageDailyRange20DInTicks, List<AumInfoModel> aumInfos, StringBuilder sbFillRateSpread)
        {
            //if (!(prodName.Equals("SQIF") && !strategy.Equals("B") && !strategy.Equals("E") && !strategy.Equals("LY")))
            //{
            //    continue;
            //}

            var dateString = date.ToString("yyyyMMdd");
            var bboLinesGroup = bboLines.GroupBy(bbo => bbo.Strategy);
            foreach (var bboByStrategy in bboLinesGroup)
            {
                // get all vfix trades of default algo and regard them as one master signal
                var strategy = bboByStrategy.Key;
                var masterSignalBBOs = bboByStrategy.ToList();
                if (masterSignalBBOs.Count == 0) continue;

                masterSignalBBOs.Sort(delegate (BboModel a, BboModel b) { return a.ChildSignalID - b.ChildSignalID; });
                foreach (var bbo in masterSignalBBOs)
                {
                    bbo.IsFound = true;
                }

                // use the first fill as the arrival bbo
                var masterSignalBBO = masterSignalBBOs.First();
                string buySell = masterSignalBBO.Direction;
                double firstFillPrice = masterSignalBBO.FilledPrice;
                double bestBid = masterSignalBBO.SignalBestBid;
                double bestAsk = masterSignalBBO.SignalBestAsk;
                int bestBidSize = masterSignalBBO.SignalBestBidSize;
                int bestAskSize = masterSignalBBO.SignalBestAskSize;
                DateTime triggerTime = masterSignalBBO.TriggerTime;
                double exchangeRate = masterSignalBBO.ExchangeRate;
                double pointValue = masterSignalBBO.PointValue;
                string contract = masterSignalBBO.Contract;
                double risk = masterSignalBBO.Risk;


                // get AUM respectively for Prop and Fund
                string pftype;
                if (prodName.Equals("SQIF") || prodName.Equals("HSI") || prodName.Equals("HHI") ||
                    prodName.Equals("TXF") || prodName.Equals("EXF") || prodName.Equals("FXF") ||
                    prodName.Equals("IFS") || prodName.Equals("MEFF") || prodName.Equals("FSMI") ||
                    prodName.Equals("CF") || prodName.Equals("OI") || prodName.Equals("SR") ||
                    prodName.Equals("TA") || prodName.Equals("A") || prodName.Equals("C") ||
                    prodName.Equals("J") || prodName.Equals("L") || prodName.Equals("M") ||
                    prodName.Equals("P") || prodName.Equals("V") || prodName.Equals("Y") ||
                    prodName.Equals("CCU") || prodName.Equals("CRB") || prodName.Equals("RU") ||
                    prodName.Equals("FG") || prodName.Equals("JM") || prodName.Equals("RM") ||
                    prodName.Equals("BB") || prodName.Equals("JD") || prodName.Equals("ME") ||
                    prodName.Equals("ZN"))
                {
                    pftype = "Prop";
                }
                else
                {
                    pftype = "Fund";
                }

                // for Fund, short and long term strategy are different
                double aumInUSD = 0;
                var aumLine = aumInfos.FirstOrDefault(aum => aum.Type == pftype && aum.StartDate <= date && aum.EndDate >= date);
                if (aumLine != null)
                {
                    if (strategy.Equals("B") || strategy.Equals("E") || strategy.Equals("F") || strategy.Equals("G") || strategy.Equals("H") ||
                        strategy.Equals("P") || strategy.Equals("R") || strategy.Equals("RHK") || strategy.Equals("U") || strategy.Equals("W"))
                    {
                        aumInUSD = aumLine.ST;
                    }
                    else
                    {
                        aumInUSD = aumLine.LT;
                    }
                }
                else
                {
                    sbFillRateSpread.AppendLine("AUM not found for " + prodName + " on " + dateString);
                    Logger.Log("ERROR", "AUM not found for " + prodName + " on " + dateString);
                    //continue;
                }

                if (double.IsNaN(bestBid) || double.IsNaN(bestAsk) || bestBidSize <= 0 || bestAskSize <= 0)
                {

                    Logger.Log("ERROR", prodName + "-" + machineName + "-" + dateString + "-" + strategy + "-Default, first  bbo is invalid");
                    sbFillRateSpread.AppendLine(prodName + "-" + machineName + "-" + dateString + "-" + strategy + "-Default, first  bbo is invalid");
                    continue;
                }


                int arrivalSpreadInTicks = (int)((bestAsk - bestBid) / tick + 0.5);
                int arrivalTouchSize = bestBidSize + bestAskSize;
                double midPrice = (bestBid + bestAsk) / 2.0;
                double NewMidPrice = (bestBid + bestAsk) / 2.0;
                if (buySell.Equals("Buy"))
                {
                    NewMidPrice = bestBid;
                }
                else
                {
                    NewMidPrice = bestAsk;
                }
                int arrivalFarTouchSize = buySell.Equals("Buy") ? bestAskSize : bestBidSize;



                double slippageInTicks = 0;
                double slippageInUSD = 0;
                int masterSignalSize = 0;
                double theorecticalSlippageInTicks = 0;
                double duration = 0;

                int fillBetterMidGT1Spread = 0;
                int fillBetterMid1Spread = 0;
                int fillAtMid = 0;
                int fillWorseMid1Spread = 0;
                int fillWorseMid2Spread = 0;
                int fillWorseMidGT2Spread = 0;
                double fillWorseMidGT2SpreadSlippageInTicks = 0;

                int exceedFarTouchSize = 0;
                double nearTouchRatio = 0;
                double bboRatio = 0;

                double slippageNTInTicks = 0;
                double slippageFTInTicks = 0;

                double slippageCloseInTicks = 0;

                // calculate slippage and fill distribution
                foreach (var bboLine in masterSignalBBOs)
                {
                    double filledPrice = bboLine.FilledPrice;
                    int filledLots = bboLine.FilledSize;
                    var filledTime = bboLine.FilledTime;


                    masterSignalSize += filledLots;

                    if (!filledTime.Equals(""))
                    {
                        duration = Math.Max(duration, (filledTime.Subtract(triggerTime)).TotalSeconds);
                    }

                    if (buySell.Equals("Buy"))
                    {

                        slippageInTicks += -filledLots * (filledPrice - midPrice) / tick;

                        slippageNTInTicks += -filledLots * (filledPrice - bestBid) / tick;

                        slippageFTInTicks += -filledLots * (filledPrice - bestAsk) / tick;

                        slippageCloseInTicks += -filledLots * (filledPrice - dayClosePrice) / tick;


                        // Kenneth if QB, add on commission of 1.25 USD per lot
                        if (algoName.Contains("QB"))
                        {
                            slippageInTicks += -1.25 * filledLots / pointValue / tick;

                            slippageNTInTicks += -1.25 * filledLots / pointValue / tick;

                            slippageFTInTicks += -1.25 * filledLots / pointValue / tick;

                            slippageCloseInTicks += -1.25 * filledLots / pointValue / tick;
                        }

                        if (filledPrice < (NewMidPrice - arrivalSpreadInTicks * tick) - 0.1 * tick)
                        {
                            fillBetterMidGT1Spread += filledLots;
                        }
                        else if ((filledPrice >= (NewMidPrice - arrivalSpreadInTicks * tick) - 0.1 * tick)
                            && (filledPrice < (NewMidPrice - 0.1 * tick)))
                        {
                            fillBetterMid1Spread += filledLots;
                        }
                        else if ((filledPrice >= (NewMidPrice - 0.1 * tick))
                            && (filledPrice <= (NewMidPrice + 0.1 * tick)))
                        {
                            fillAtMid += filledLots;
                        }
                        else if ((filledPrice > (NewMidPrice + 0.1 * tick))
                            && (filledPrice <= (NewMidPrice + arrivalSpreadInTicks * tick) + 0.1 * tick))
                        {
                            fillWorseMid1Spread += filledLots;
                        }
                        else if ((filledPrice > (NewMidPrice + arrivalSpreadInTicks * tick) + 0.1 * tick)
                            && (filledPrice <= (NewMidPrice + 2 * arrivalSpreadInTicks * tick) + 0.1 * tick))
                        {
                            fillWorseMid2Spread += filledLots;
                        }
                        else if (filledPrice > (NewMidPrice + 2 * arrivalSpreadInTicks * tick) + 0.1 * tick)
                        {
                            fillWorseMidGT2Spread += filledLots;
                            fillWorseMidGT2SpreadSlippageInTicks += -filledLots * (filledPrice - NewMidPrice) / tick;
                        }


                    }
                    else
                    {
                        slippageInTicks += filledLots * (filledPrice - midPrice) / tick;

                        slippageNTInTicks += filledLots * (filledPrice - bestAsk) / tick;

                        slippageFTInTicks += filledLots * (filledPrice - bestBid) / tick;

                        slippageCloseInTicks += filledLots * (filledPrice - dayClosePrice) / tick;

                        // Kenneth if QB, add on commission of 1.25 USD per lot
                        if (algoName.Contains("QB"))
                        {
                            slippageInTicks += -1.25 * filledLots / pointValue / tick;

                            slippageNTInTicks += -1.25 * filledLots / pointValue / tick;

                            slippageFTInTicks += -1.25 * filledLots / pointValue / tick;

                            slippageCloseInTicks += -1.25 * filledLots / pointValue / tick;
                        }


                        if (filledPrice > (NewMidPrice + arrivalSpreadInTicks * tick) + 0.1 * tick)
                        {
                            fillBetterMidGT1Spread += filledLots;
                        }
                        else if ((filledPrice <= (NewMidPrice + arrivalSpreadInTicks * tick) + 0.1 * tick)
                            && (filledPrice > (NewMidPrice + 0.1 * tick)))
                        {
                            fillBetterMid1Spread += filledLots;
                        }
                        else if ((filledPrice <= (NewMidPrice + 0.1 * tick))
                            && (filledPrice >= (NewMidPrice - 0.1 * tick)))
                        {
                            fillAtMid += filledLots;
                        }
                        else if ((filledPrice < (NewMidPrice - 0.1 * tick))
                            && (filledPrice >= (NewMidPrice - arrivalSpreadInTicks * tick) - 0.1 * tick))
                        {
                            fillWorseMid1Spread += filledLots;
                        }
                        else if ((filledPrice < (NewMidPrice - arrivalSpreadInTicks * tick) - 0.1 * tick)
                            && (filledPrice >= (NewMidPrice - 2 * arrivalSpreadInTicks * tick) - 0.1 * tick))
                        {
                            fillWorseMid2Spread += filledLots;
                        }
                        else if (filledPrice < (NewMidPrice - 2 * arrivalSpreadInTicks * tick) - 0.1 * tick)
                        {
                            fillWorseMidGT2Spread += filledLots;
                            fillWorseMidGT2SpreadSlippageInTicks += -filledLots * (NewMidPrice - filledPrice) / tick;
                        }

                    }

                }

                // calculate far touch ratio and theorectical slippage
                if (buySell.Equals("Buy"))
                {
                    //farTouchRatio = masterSignalSize <= bestAskSize ? 0 : (masterSignalSize -bestAskSize) / (double)masterSignalSize;
                    exceedFarTouchSize = masterSignalSize <= bestAskSize ? 0 : (masterSignalSize - bestAskSize);
                    nearTouchRatio = masterSignalSize / (double)bestBidSize;
                    bboRatio = masterSignalSize / (double)(bestBidSize + bestAskSize);

                    double farTouchRation2 = (double)bestAskSize / (bestBidSize + bestAskSize);
                    if (lowSpreadThresholds.ContainsKey(prodName) && highSpreadThresholds.ContainsKey(prodName))
                    {
                        if (arrivalSpreadInTicks <= 1 && farTouchRation2 < lowSpreadThresholds[prodName] || arrivalSpreadInTicks > 1 && farTouchRation2 < highSpreadThresholds[prodName])
                        {
                            theorecticalSlippageInTicks = -(bestAsk - midPrice) / tick * masterSignalSize;
                        }
                        else
                        {
                            theorecticalSlippageInTicks = slippageInTicks;
                        }
                    }
                    else
                    {
                        theorecticalSlippageInTicks = slippageInTicks;
                    }
                }
                else
                {
                    //farTouchRatio = masterSignalSize <= bestBidSize ? 0 : (masterSignalSize - bestBidSize) / (double)masterSignalSize;
                    exceedFarTouchSize = masterSignalSize <= bestBidSize ? 0 : (masterSignalSize - bestBidSize);
                    nearTouchRatio = masterSignalSize / (double)bestAskSize;
                    bboRatio = masterSignalSize / (double)(bestBidSize + bestAskSize);

                    double farTouchRatio2 = (double)bestBidSize / (bestBidSize + bestAskSize);
                    theorecticalSlippageInTicks = slippageInTicks;
                    if (lowSpreadThresholds.ContainsKey(prodName) && highSpreadThresholds.ContainsKey(prodName))
                    {
                        if (arrivalSpreadInTicks <= 1 && farTouchRatio2 < lowSpreadThresholds[prodName] || arrivalSpreadInTicks > 1 && farTouchRatio2 < highSpreadThresholds[prodName])
                        {
                            theorecticalSlippageInTicks = (bestBid - midPrice) / tick * masterSignalSize;
                        }
                        else
                        {
                            theorecticalSlippageInTicks = slippageInTicks;
                        }
                    }
                    else
                    {
                        theorecticalSlippageInTicks = slippageInTicks;
                    }
                }


                // slippage in USD
                slippageInUSD = slippageInTicks * tick * pointValue * exchangeRate;

                if (double.IsNaN(slippageInTicks))
                {
                    continue;
                }

                // add master signal line
                var signalKey = dateString + "-" + strategy + "-" + machineName + "-" + strategy + "-" + risk.ToString("0.############");
                SingleFileColumnModel signalFileLine = dailySignalFileLines.FirstOrDefault(item => item.Product == prodName && item.Algo == algoName
                    && item.SignalKey == signalKey);
                if (signalFileLine == null)
                {
                    signalFileLine = new SingleFileColumnModel()
                    {
                        Date = date,
                        Product = prodName,
                        Algo = algoName,
                        SignalKey = signalKey,
                        Size = masterSignalSize,
                        SlippageInTicks = slippageInTicks,
                        NTSlippageInTicks = slippageNTInTicks,
                        SlippageAsSpreadRatio = (masterSignalSize * arrivalSpreadInTicks),
                        SpreadInTicks = (masterSignalSize * arrivalSpreadInTicks),
                        HighSpreadSize = (arrivalSpreadInTicks > 1) ? masterSignalSize : 0,
                        HighSpreadInTicks = (arrivalSpreadInTicks > 1) ? (masterSignalSize * arrivalSpreadInTicks) : 0,
                        SlippageClosePriceInTicks = slippageCloseInTicks,
                        SlippageInUSDPerTrade = slippageInUSD,
                        TotalSlippageInUSD = slippageInUSD,
                        SlippageAsAUMRatio = (masterSignalSize * aumInUSD),
                        DailyRangeInTicks = (masterSignalSize * averageDailyRange20DInTicks),
                        SlippageAsDailyRangeRatio = (masterSignalSize * averageDailyRange20DInTicks),
                        AverageDuration = (duration * masterSignalSize),
                        MaxDuration = duration,
                        Contract = contract,
                        TriggerTime = triggerTime,
                        ArrivalBestBid = bestBid,
                        ArrivalBestAsk = bestAsk,
                        ArrivalBestBidSize = bestBidSize,
                        ArrialBestAskSize = bestAskSize,
                        FirstBestBid = bestBid,
                        FirstBestAsk = bestAsk,
                        FirstBestBidSize = bestBidSize,
                        FirstBestAskSize = bestAskSize,
                        SignalSummarySize = masterSignalSize
                    };

                    dailySignalFileLines.Add(signalFileLine);
                }
            }
        }

        private List<SignalSummaryModel> LoadSignalSummary(string date)
        {
            var result = new List<SignalSummaryModel>();
            string sql = string.Format(@"
select ProdName, MachineName, AlgoName, Date, SignalID, MasterOrderID, Size, FilledSize, FilledPrice, 
	Direction, StrategyName, ChildType, status, ExceedFatFingerLimit, AbnormalFillPrice,
	OrderPrice, BidPrice, AskPrice, BidSize, AskSize, Round0, StartTime, EndTime
from ConfigManager..SignalSummary where Date = '{0}'", date);
            if (Products.Count == 1) sql += string.Format(" AND ProdName = '{0}'", Products[0]);
            DataTable tb = HelperFunction.ExecuteQuery(sql);

            foreach (DataRow row in tb.Rows)
            {
                var model = new SignalSummaryModel(row);
                result.Add(model);
            }

            return result;
        }

        private List<SignalLatencyModel> LoadSignalLatency(string date)
        {
            var list = new List<SignalLatencyModel>();
            var sql = "select Date, ProdName, MachineName, SignalID, CalcPriceLatency, SubmitLatency, CancelLatency from ConfigManager..SignalLatency where Date = '" + date + "'";
            if (Products.Count == 1) sql += string.Format(" AND ProdName = '{0}'", Products[0]);
            DataTable tb = HelperFunction.ExecuteQuery(sql);
            foreach (DataRow row in tb.Rows)
            {
                try
                {
                    var model = new SignalLatencyModel();
                    model.Date = (DateTime)row["Date"];
                    model.ProdName = row["ProdName"].ToString().ToUpper();
                    model.MachineName = row["MachineName"].ToString().ToUpper();
                    model.SignalID = int.Parse(row["SignalID"].ToString());
                    model.CalcPriceLatency = double.Parse(row["CalcPriceLatency"].ToString());
                    model.SubmitLatency = double.Parse(row["SubmitLatency"].ToString());
                    model.CancelLatency = double.Parse(row["CancelLatency"].ToString());
                    list.Add(model);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return list;
        }

        private List<BboModel> LoadTradeFeed(string date)
        {
            var result = new List<BboModel>();
            string sql = "select TradeDate, FutureSymbol, Quantity, TradePrice, ExecutionAccount, Custom1, Custom3, Custom6, TradeAttribute from VegasoulProd..Vegasoul_TradeFeed where TradeAttribute <> 'MARCO_B' AND TradeDate = '" + date + "' AND ExecutionAccount <> 'NETOFF'";
            if (Products.Count == 1) sql += string.Format(" AND FutureSymbol = '{0}'", Products[0]);
            DataTable tb = HelperFunction.ExecuteQuery(sql);

            foreach (DataRow row in tb.Rows)
            {
                var model = new BboModel();
                model.Load(row);

                // filter manual trades; if fill price and size are not matched with the ones in custom3, we consider this trade to be manual trade
                //if (Math.Abs(model.FilledPrice - model.FilledPriceInCustom3) > 1e-6 || model.FilledSize != model.FilledSizeInCustom3)
                //{
                //    continue;
                //}

                // save the vfix trades and currency info in bboLines and crcyPtvalLines for further use
                result.Add(model);

                // if master signal id parsed from vfix trade is 0, then search and replace it with the one in the signal summary
                if (model.MasterSignalID <= 0)
                {
                    try
                    {
                        string sql2 = "select MasterOrderID from ConfigManager..SignalSummary where MachineName = '"
                            + model.MachineName + "' and ProdName = '" + model.ProdName + "' and AlgoName = '" + model.AlgoEngine + "' and Date = '" + model.Date + "' and SignalID = '" + model.ChildSignalID + "' ";
                        var tb2 = HelperFunction.ExecuteQuery(sql2);
                        if (tb2.Rows.Count > 0)
                        {
                            model.MasterSignalID = int.Parse(tb2.Rows[0]["MasterOrderID"].ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        Log("ERROR", "Exception occurs when quering MasterOrderID for vfix trades 0 id Signal. " + ex.ToString());
                    }
                }
            }

            return result;
        }

        private void UpdateAlgoReport(SingleFileColumnModel signalLine, AlgoReportStatModel algoReport)
        {
            algoReport.AUM = signalLine.SlippageAsAUMRatio / signalLine.Size;

            string[] keys = signalLine.SignalKey.Split('-');
            if (!algoReport.Strategies.ContainsKey(keys[3]))
            {
                algoReport.Strategies.Add(keys[3], 0);
            }
            try
            {
                algoReport.Strategies[keys[3]] = double.Parse(keys[4]);
            }
            catch (Exception e)
            {
                algoReport.Strategies[keys[3]] = 0;
            }

            algoReport.TotalDurationInSecond += Math.Max(signalLine.MaxDuration, 1);
            algoReport.MaxNumOrderPerSignal = Math.Max(signalLine.NumOfOrderSubmitted, algoReport.MaxNumOrderPerSignal);
            algoReport.MaxNumOrderCancelledPerSignal = Math.Max(signalLine.NumOfOrderCancelled, algoReport.MaxNumOrderCancelledPerSignal);
            var slippageInTicks = signalLine.SlippageInTicks / signalLine.Size;
            var SlippageInSpread = slippageInTicks / (signalLine.SlippageAsSpreadRatio / signalLine.Size);
            if (!signalLine.Algo.Equals("TWAPMOC"))
            {
                if (slippageInTicks < algoReport.MaxSlippageInTicks)
                {
                    algoReport.MaxSlippageInTicks = slippageInTicks;
                    algoReport.MaxSlippageInSpread = SlippageInSpread;
                    algoReport.MaxSlippageDay = keys[0];
                }
                if (slippageInTicks > algoReport.MinSlippageInTicks)
                {
                    algoReport.MinSlippageInTicks = slippageInTicks;
                    algoReport.MinSlippageInSpread = SlippageInSpread;
                    algoReport.MinSlippageDay = keys[0];
                }
                algoReport.TotalSlippage += slippageInTicks * signalLine.Size;
                algoReport.TotalSlippageSize += signalLine.Size;
            }

            if (signalLine.Size > algoReport.MaxSizePerSignal)
            {
                algoReport.MaxSizePerSignal = signalLine.Size;
            }

            algoReport.TotalSize += signalLine.Size;
            algoReport.TotalNumSignal += 1;
        }

        private void WriteData(SingleFileColumnModelList list)
        {
            var group = list.Data.GroupBy(value => value.Date);
            HelperFunction.EnsureFolderExists(TMP_FILE_PATH);
            var filePath = Path.Combine(TMP_FILE_PATH, list.Date.ToString("yyyyMMdd") + ".txt");
            HelperFunction.EnsureFileExists(filePath);
            File.WriteAllText(filePath, list.ToString());
        }

        private SingleFileColumnModelList LoadData(DateTime date)
        {
            HelperFunction.EnsureFolderExists(TMP_FILE_PATH);
            var filePath = Path.Combine(TMP_FILE_PATH, date.ToString("yyyyMMdd") + ".txt");
            if (!File.Exists(filePath)) return null;
            var content = File.ReadAllText(filePath);
            var list = SingleFileColumnModelList.FromString(content);
            //Logger.PrintLogWithTimeStamp(string.Format("{0} records loaded from temp files between {1} and {2}", list.Count, startDate.ToString("yyyyMMdd"), endDate.ToString("yyyyMMdd")));
            return list;
        }

        public void Log(string type, string logmessage)
        {
            Logger.Log(type, logmessage);
        }
    }
}