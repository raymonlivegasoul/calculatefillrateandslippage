﻿using System;
using System.Collections.Generic;
using System.Linq;
using CalculateFillRateAndSlippage.Model;
using System.Data;
using Utility.Database;

namespace CalculateFillRateAndSlippage
{
    public class DataViewModel
    {
        #region Properties
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public List<string> Products { get; set; }
        public long NumBBOEqualOrders { get; set; }
        public Dictionary<string, double> ProdTickDict { get; set; }
        public Dictionary<string, int> MachineProdDateList { get; set; }
        public Dictionary<string, Dictionary<DateTime, double>> FiveMinuteBars { get; set; }
        public List<AumInfoModel> AumInfos { get; set; }
        public Dictionary<string, double> HighSpreadThresholds { get; set; }
        public Dictionary<string, double> LowSpreadThresholds { get; set; }
        public Dictionary<string, double> ProductDailyRangeDict { get; set; }
        public Dictionary<string, double> ProductDayCloseDict { get; set; }
        public Dictionary<string, Dictionary<string, List<SignalSummaryModel>>> SignalSummaryList { get; set; }
        public Dictionary<string, List<BboModel>> BboList { get; set; }
        public Dictionary<string, Dictionary<string, List<SignalLatencyModel>>> SignalLatencies { get; set; }

        #endregion

        public bool LoadData()
        {
            Init();

            GetProduct();
            if (Products.Count == 0)
            {
                Log("ERROR", "No valid product, program stopped.");
                return false;
            }

            // read high and low spread far touch ratio threshold for each product from FarTouchRatioThreshold.csv which is used for calculating the theorectical optimal slippage
            GetProductSpread(HighSpreadThresholds, LowSpreadThresholds);

            // get daily range for each product from Analytics_VTSDailyBar table in database
            // use a dictionary to store the daily range, and use product name and date as the key
            GetProductDailyRange(ProductDailyRangeDict, ProductDayCloseDict);

            // Read meta data from DB
            GetProductTick(ProdTickDict);

            // read machine-production info from VMDailysummaries so that we know which machine is on production, 
            // use a dictionary to store all the production machine, and use the machine name, product name and date as the key
            GetProductionMachineInfo();

            // read AUM info from database, which is the short and long term AUM of each month
            // users need to insert the AUM numbers into the table at the beginning of each month
            GetAum(AumInfos);

            LoadProductFiveMinuteBars();

            Logger.PrintLogWithTimeStamp("Basic data loaded.");

            return true;
        }

        private void GetProduct()
        {
            var products = new List<string>();
            string sql = string.Format("select DISTINCT ProdName from ConfigManager..SignalSummary where Date BETWEEN '{0}' AND '{1}' ORDER BY ProdName", StartDate, EndDate);
            var db = DBManager.Default;
            db.RunQuery(sql);
            var tb = db.ExecuteDataTable();
            foreach (DataRow row in tb.Rows)
            {
                var prodName = row["ProdName"].ToString();
                if (string.IsNullOrEmpty(prodName)) continue;
                // Testing, remove crypto products
                if (prodName.Contains("USD") && prodName.Length >= 5) continue;
                if (prodName.Contains("BTC") && prodName.Length >= 4) continue;
                products.Add(prodName);
            }
            if (Products.Count != 0)
            {
                products = products.Intersect(Products).ToList();
                Products.Clear();
            }
            Products.AddRange(products);
        }

        public void GetProductTick(Dictionary<string, double> prodTickDict)
        {
            try
            {
                string sql = "select prodName,minmov from vegasoul..Prod";
                var db = DBManager.Default;
                db.RunQuery(sql);
                var tb = db.ExecuteDataTable();

                foreach (DataRow row in tb.Rows)
                {
                    string prodName = row["prodName"].ToString().ToUpper();
                    if (prodName == "TX")
                    {
                        prodName = "TXF";
                    }
                    if (prodName == "TXFI")
                    {
                        prodName = "FXF";
                    }
                    if (prodName == "TE")
                    {
                        prodName = "EXF";
                    }
                    if (prodName == "JNM")
                    {
                        prodName = "NM";
                    }
                    double minmove = double.Parse(row["minmov"].ToString());
                    if (Products.Contains(prodName)
                        && !prodTickDict.ContainsKey(prodName))
                    {
                        prodTickDict.Add(prodName, minmove);
                    }
                }

                var missingProducts = Products.Except(prodTickDict.Keys).ToList();
                if (missingProducts.Count > 0)
                {
                    Log("ERROR", string.Format("Cannot find tick for {0}.", string.Join(", ", missingProducts)));
                    // Remove the product with no tick record
                    missingProducts.ForEach(value => Products.Remove(value));
                }
            }
            catch (Exception ex)
            {
                OnDBConnectionFailed(ex);
            }
        }

        private void GetProductionMachineInfo()
        {
            MachineProdDateList = new Dictionary<string, int>();
            try
            {
                string sql = "select PCName, ProdName, Date from VMDailysummaries where Mode = 'P' and StgyName not in ('MARCO_B','BHL1','SC','TPX','PMLS','TP') and Date >= '" + StartDate + "' and Date <='" + EndDate + "' group by PCName, ProdName, Date order by ProdName, Date";
                var db = DBManager.Default;
                db.RunQuery(sql);
                var tb = db.ExecuteDataTable();

                foreach (DataRow row in tb.Rows)
                {
                    string machineName = row["PCName"].ToString().ToUpper();
                    string prodName = row["ProdName"].ToString().ToUpper();
                    string date = DateTime.Parse(row["Date"].ToString()).ToString("yyyyMMdd");
                    MachineProdDateList.Add(machineName + "-" + prodName + "-" + date, 0);
                }
            }
            catch (Exception ex)
            {
                OnDBConnectionFailed(ex);
            }
        }

        private void GetAum(List<AumInfoModel> aumInfos)
        {
            try
            {
                string sql = "select TYPE, ST, LT, StartDate, EndDate from AUM order by StartDate";
                var db = DBManager.Default;
                db.RunQuery(sql);
                var tb = db.ExecuteDataTable();
                foreach (DataRow row in tb.Rows)
                {
                    try
                    {
                        var model = new AumInfoModel();
                        model.Type = row["TYPE"].ToString();
                        model.ST = double.Parse(row["ST"].ToString());
                        model.LT = double.Parse(row["LT"].ToString());
                        model.StartDate = (DateTime)row["StartDate"];
                        model.EndDate = (DateTime)row["EndDate"];
                        aumInfos.Add(model);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            catch (Exception ex)
            {
                OnDBConnectionFailed(ex);
            }
        }

        
        private void GetProductDailyRange(Dictionary<string, double> productDailyRangeDict, Dictionary<string, double> productDayCloseDict)
        {
            try
            {
                string sql = "select Prodname, Date, H, L, C from vegasoul..Analytics_VTSDailyBar where Date > '20161001' order by Date";
                var tb = HelperFunction.ExecuteQuery(sql);

                foreach (DataRow row in tb.Rows)
                {
                    string prodName = row["Prodname"].ToString().ToUpper();
                    string date = ((DateTime)row["Date"]).ToString("yyyyMMdd");
                    double H = double.Parse(row["H"].ToString());
                    double L = double.Parse(row["L"].ToString());
                    double C = double.Parse(row["C"].ToString());
                    productDailyRangeDict.Add(prodName + "-" + date, H - L);
                    productDayCloseDict.Add(prodName + "-" + date, C);
                }
            }
            catch (Exception ex)
            {
                OnDBConnectionFailed(ex);
            }
        }

        private void GetProductSpread(Dictionary<string, double> highSpreadThresholds, Dictionary<string, double> lowSpreadThresholds)
        {
            try
            {
                string sql = "select Product, ConfigKey, ConfigValue, AlgoName from vegasoul..VTS_ExecutionAlgo_Config where ConfigKey in (3,4) and AlgoName not in ('HSQIF','TWAPMOC','SQIF','ClimbingGoldNew') order by Product";
                var tb = HelperFunction.ExecuteQuery(sql);
                foreach (DataRow row in tb.Rows)
                {
                    string prodName = row["Product"].ToString().ToUpper();
                    int configkey = Int32.Parse(row["ConfigKey"].ToString());
                    double configvalue = double.Parse(row["ConfigValue"].ToString());
                    string algoname = row["AlgoName"].ToString().ToUpper();

                    if (algoname != "PASSIVEMULTIFLOATSNIPERGOLDNEW")
                    {
                        if (configkey == 3)
                        {
                            if (!lowSpreadThresholds.Keys.Contains(prodName))
                            {
                                lowSpreadThresholds.Add(prodName, configvalue);
                            }
                            else
                            {
                                lowSpreadThresholds[prodName] = configvalue;
                            }
                        }
                        if (configkey == 4)
                        {
                            if (!highSpreadThresholds.Keys.Contains(prodName))
                            {
                                highSpreadThresholds.Add(prodName, configvalue);
                            }
                            else
                            {
                                highSpreadThresholds[prodName] = configvalue;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                OnDBConnectionFailed(ex);
            }
        }

        private Dictionary<string, List<SignalSummaryModel>> LoadSingalSummaryByProduct(string prodName)
        {
            var currentMachineName = string.Empty;
            var currentMachineList = new Dictionary<string, List<SignalSummaryModel>>();
            var currentList = new List<SignalSummaryModel>();

            string sql = string.Format(@"
select ProdName, MachineName, AlgoName, Date, SignalID, MasterOrderID, Size, FilledSize, FilledPrice, 
	Direction, StrategyName, ChildType, status, ExceedFatFingerLimit, AbnormalFillPrice,
	OrderPrice, BidPrice, AskPrice, BidSize, AskSize, Round0, StartTime, EndTime
from ConfigManager..SignalSummary where ProdName='{0}' AND Date BETWEEN '{1}' AND '{2}' ORDER BY MachineName", prodName, StartDate, EndDate);
            DataTable tb = HelperFunction.TryExecuteQuery(sql);
            if (tb == null)
            {
                //Log("Error", "Load SignalSummary failed for product " + prodName);
                return null;
            }
            foreach (DataRow row in tb.Rows)
            {
                var model = new SignalSummaryModel(row);
                if (currentMachineName != model.MachineName)
                {
                    currentList = new List<SignalSummaryModel>();
                    currentMachineList.Add(model.MachineName, currentList);
                    currentMachineName = model.MachineName;
                }
                currentList.Add(model);
            }
            return currentMachineList;
        }

        private void LoadProductFiveMinuteBars()
        {
            FiveMinuteBars = new Dictionary<string, Dictionary<DateTime, double>>();
            foreach (var prodName in Products)
            {
                FiveMinuteBars.Add(prodName, new Dictionary<DateTime, double>());
            }

            var productQuery = string.Format("ProductName IN ('{0}')", string.Join("','", Products));
            var currentProductName = string.Empty;
            var bars = new Dictionary<DateTime, double>();
            var sql = "select ProductName, TradeDate, EndTime, Volume from ConfigManager..FiveMinuteBar where " + productQuery + " and TradeDate >= '" + StartDate + "' and TradeDate <= '" + EndDate + "' ORDER BY ProductName";
            var db = DBManager.Default;
            db.RunQuery(sql);
            var tb = db.ExecuteDataTable();

            foreach (DataRow row in tb.Rows)
            {
                try
                {
                    var prodName = row["ProductName"].ToString();
                    var date = (DateTime)row["TradeDate"];
                    var time = (DateTime)row["EndTime"];
                    var dateTime = date + time.TimeOfDay;
                    double vol = double.Parse(row["Volume"].ToString());

                    if (currentProductName != prodName)
                    {
                        bars = FiveMinuteBars[prodName];
                        currentProductName = prodName;
                    }

                    if (!bars.ContainsKey(dateTime))
                    {
                        bars.Add(dateTime, vol);
                    }
                    else bars[dateTime] = vol;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        
        private void Init()
        {
            ProdTickDict = new Dictionary<string, double>();
            MachineProdDateList = new Dictionary<string, int>();
            new List<string[]>();
            SignalSummaryList = new Dictionary<string, Dictionary<string, List<SignalSummaryModel>>>();
            AumInfos = new List<AumInfoModel>();
            FiveMinuteBars = new Dictionary<string, Dictionary<DateTime, double>>();
            BboList = new Dictionary<string, List<BboModel>>();
            SignalLatencies = new Dictionary<string, Dictionary<string, List<SignalLatencyModel>>>();
            HighSpreadThresholds = new Dictionary<string, double>();
            LowSpreadThresholds = new Dictionary<string, double>();
            ProductDailyRangeDict = new Dictionary<string, double>();
            ProductDayCloseDict = new Dictionary<string, double>();
        }

        private void OnDBConnectionFailed(Exception ex)
        {
            Log("Error", "fail to connect DB server. Exception: " + ex.ToString());
        }

        public void Log(string type, string logmessage)
        {
            Logger.Log(type, logmessage);
        }
    }
}
