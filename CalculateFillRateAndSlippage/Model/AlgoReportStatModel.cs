﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateFillRateAndSlippage.Model
{
    public class AlgoReportStatModel
    {
        #region Properties
        public string TestDescription;
        public double AUM;
        public Dictionary<string, double> Strategies;
        public List<DateTime> OrderSubmitTime;
        public List<double> OrderSubmitVolume;
        public double TotalVolumeInOneSecond;
        public Dictionary<string, double> NumOrderPerDay;
        public Dictionary<string, double> NumBuyOrderPerDay;
        public Dictionary<string, double> NumSellOrderPerDay;
        public List<DateTime> DirectionTime;
        public List<string> Directions;

        public List<DateTime> FillTime;
        public List<int> FillQty;
        public List<double> FillPrice;
        public List<string> FillDirection;
        public int BuyQty;
        public int BuyNum;
        public int SellQty;
        public int SellNum;
        public double BuyValue;
        public double SellValue;

        public double TotalNumOrder;
        public double TotalVolumeOrder;
        public double TotalDurationInSecond;
        public double TotalNumSignal;
        public double TotalSlippage;
        public double TotalSlippageSize;
        public double TotalSize;

        public Dictionary<DateTime, double> MarketVolume;
        public Dictionary<DateTime, double> ExecutedVolume;

        public double MaxNumOrderPerSecond;
        public double AvgNumOrderPerSecond;
        public double MaxVolumeSentPerSecond;
        public double AvgVolumeSentPerSecond;
        public double AvgMarketParticipationRate;
        public double MaxMarketParticipationRate;
        public string MaxParticipationTime;
        public double MaxNumOrderPerDay;
        public double AvgNumOrderPerDay;
        public double MaxNumOrderPerSignal;
        public double AvgNumOrderPerSignal;
        public double MaxNumOrderCancelledPerSignal;
        public double MaxOrderSizePerOrder;
        public double AvgOrderSizePerOrder;
        public double MaxSlippageInTicks;
        public double MaxSlippageInSpread;
        public string MaxSlippageDay;
        public double MinSlippageInTicks;
        public double MinSlippageInSpread;
        public string MinSlippageDay;
        public double AvgSlippage;
        public double MaxSizePerSignal;
        public double AvgSizePerSignal;
        public double NumOrderWithAbnormalFillPrice;
        public double MaxNumBuyOrderPerDay;
        public double MaxNumSellOrderPerDay;
        public double MaxNumberOfReversalInOneMinute;
        public string MaxNumberOfReversalTime;
        public double MaxNumberOfOrdersMatchedInFiveMinutes;
        public string MaxNumberOfOrdersMatchedTime;
        public double PnLOrdersMatchedInFiveMinutes;
        public double MaxExecutionDurationPerSignal;
        public string MaxExecutionDurationPerSignalTime;
        public double LongestCalcPriceInterval;
        public string LongestCalcPriceIntervalTime;
        public double LongestUseSimBBOInterval;
        public string LongestUseSimBBOIntervalTime;
        public double NumOrderRejected;
        public double NumOrderExceedingFatFingerLimit;
        public bool IsNotCrashing;

        public List<double> ListMaxNumOfOrderPerSecond;
        public List<double> ListAvgNumOfOrderPerSecond;
        public List<double> ListMaxVolPerSecond;
        public List<double> ListAvgVolPerSecond;
        public List<double> ListAvg5MinParticipationRate;
        public List<double> ListMax5MinParticipationRate;
        public List<double> ListNumOfOrderPerDay;
        public List<double> ListMaxNumOfOrderPerSignal;
        public List<double> ListAvgNumOfOrderPerSignal;
        public List<double> ListMaxOrderSizePerOrder;
        public List<double> ListAvgOrderSizePerOrder;
        public List<double> ListMaxSizePerSignal;
        public List<double> ListAvgSizePerSignal;
        public List<double> ListNumOfOrderWithAbnormalFillPrice;
        public List<double> ListNumOfBuyOrders;
        public List<double> ListNumOfSellOrders;
        public List<double> ListNumOfOrderDirectionReversalInOneMinute;
        public List<double> ListNumOfRejectOrders;
        public List<double> ListNumOfOrdersExceedingFatFingerLimit;
        #endregion

        public AlgoReportStatModel()
        {
            TestDescription = "";
            Strategies = new Dictionary<string, double>();
            OrderSubmitTime = new List<DateTime>();
            OrderSubmitVolume = new List<double>();
            NumOrderPerDay = new Dictionary<string, double>();
            NumBuyOrderPerDay = new Dictionary<string, double>();
            NumSellOrderPerDay = new Dictionary<string, double>();
            Directions = new List<string>();
            DirectionTime = new List<DateTime>();

            FillDirection = new List<string>();
            FillQty = new List<int>();
            FillPrice = new List<double>();
            FillTime = new List<DateTime>();

            BuyQty = 0;
            BuyNum = 0;
            SellQty = 0;
            SellNum = 0;
            BuyValue = 0;
            SellValue = 0;

            MarketVolume = new Dictionary<DateTime, double>();
            ExecutedVolume = new Dictionary<DateTime, double>();

            TotalVolumeInOneSecond = 0;
            TotalNumOrder = 0;
            TotalVolumeOrder = 0;
            TotalDurationInSecond = 0;
            TotalNumSignal = 0;
            MaxSlippageInTicks = 9999;
            MaxSlippageInSpread = 9999;
            MinSlippageInTicks = -9999;
            MinSlippageInSpread = -9999;
            MaxSlippageDay = "";
            MinSlippageDay = "";
            MaxNumberOfReversalInOneMinute = 0;
            MaxNumberOfReversalTime = "";
            MaxNumberOfOrdersMatchedInFiveMinutes = 0;
            PnLOrdersMatchedInFiveMinutes = 0;
            MaxNumberOfOrdersMatchedTime = "";
            MaxExecutionDurationPerSignal = 0;
            MaxExecutionDurationPerSignalTime = "";
            TotalSlippage = 0;
            TotalSize = 0;
            TotalSlippageSize = 0;
            MaxNumOrderPerSecond = 0;
            AvgNumOrderPerSecond = 0;
            MaxVolumeSentPerSecond = 0;
            AvgVolumeSentPerSecond = 0;
            AvgMarketParticipationRate = 0;
            MaxMarketParticipationRate = 0;
            MaxParticipationTime = "";
            MaxNumOrderPerDay = 0;
            AvgNumOrderPerDay = 0;
            MaxNumOrderPerSignal = 0;
            AvgNumOrderPerSignal = 0;
            MaxNumOrderCancelledPerSignal = 0;
            MaxOrderSizePerOrder = 0;
            AvgOrderSizePerOrder = 0;
            AvgSlippage = 0;
            MaxSizePerSignal = 0;
            AvgSizePerSignal = 0;
            NumOrderWithAbnormalFillPrice = 0;
            MaxNumBuyOrderPerDay = 0;
            MaxNumSellOrderPerDay = 0;
            NumOrderRejected = 0;
            NumOrderExceedingFatFingerLimit = 0;
            IsNotCrashing = true;

            LongestCalcPriceInterval = 0;
            LongestCalcPriceIntervalTime = "";
            LongestUseSimBBOInterval = 0;
            LongestUseSimBBOIntervalTime = "";

            ListMaxNumOfOrderPerSecond = new List<double>();
            ListAvgNumOfOrderPerSecond = new List<double>();
            ListMaxVolPerSecond = new List<double>();
            ListAvgVolPerSecond = new List<double>();
            ListAvg5MinParticipationRate = new List<double>();
            ListMax5MinParticipationRate = new List<double>();
            ListNumOfOrderPerDay = new List<double>();
            ListMaxNumOfOrderPerSignal = new List<double>();
            ListAvgNumOfOrderPerSignal = new List<double>();
            ListMaxOrderSizePerOrder = new List<double>();
            ListAvgOrderSizePerOrder = new List<double>();
            ListMaxSizePerSignal = new List<double>();
            ListAvgSizePerSignal = new List<double>();
            ListNumOfOrderWithAbnormalFillPrice = new List<double>();
            ListNumOfBuyOrders = new List<double>();
            ListNumOfSellOrders = new List<double>();
            ListNumOfOrderDirectionReversalInOneMinute = new List<double>();
            ListNumOfRejectOrders = new List<double>();
            ListNumOfOrdersExceedingFatFingerLimit = new List<double>();
        }

        public static Dictionary<string, AlgoReportStatModel> Merge(List<Dictionary<string, AlgoReportStatModel>> list)
        {
            var result = new Dictionary<string, AlgoReportStatModel>();
            var groupByProd = new Dictionary<string, List<AlgoReportStatModel>>();
            foreach(var dic in list)
            {
                foreach(var pair in dic)
                {
                    List<AlgoReportStatModel> currentList = new List<AlgoReportStatModel>();
                    if (!groupByProd.ContainsKey(pair.Key))
                    {
                        groupByProd.Add(pair.Key, currentList);
                    }
                    else currentList = groupByProd[pair.Key];
                    currentList.Add(pair.Value);
                }
            }

            foreach(var pair in groupByProd)
            {
                if (pair.Value.Count == 0) continue;
                var mergedModel = Merge(pair.Value);
                result.Add(pair.Key, mergedModel);
            }

            return result;
        }

        public static AlgoReportStatModel Merge(List<AlgoReportStatModel> list)
        {
            var algoReport = new AlgoReportStatModel();

            algoReport.MaxNumOrderPerDay = 0;
            algoReport.AvgNumOrderPerDay = 0;
            algoReport.MaxNumBuyOrderPerDay = 0;
            algoReport.MaxNumSellOrderPerDay = 0;
            int count = 0;
            double total_market_volume = 0;
            double total_executed_volume = 0;
            foreach (var model in list)
            {
                algoReport.NumOrderRejected += model.NumOrderRejected;
                algoReport.NumOrderExceedingFatFingerLimit += model.NumOrderExceedingFatFingerLimit;
                algoReport.NumOrderWithAbnormalFillPrice += model.NumOrderWithAbnormalFillPrice;

                if (model.NumOrderPerDay.Values.Count() > 0)
                {
                    var numOrderPerDay = model.NumOrderPerDay.Values.First();
                    algoReport.MaxNumOrderPerDay = Math.Max(numOrderPerDay, algoReport.MaxNumOrderPerDay);
                    algoReport.AvgNumOrderPerDay += numOrderPerDay;
                    count++;
                }

                if (model.NumBuyOrderPerDay.Values.Count() > 0)
                    algoReport.MaxNumBuyOrderPerDay = Math.Max(model.NumBuyOrderPerDay.Values.First(), algoReport.MaxNumBuyOrderPerDay);

                if (model.NumSellOrderPerDay.Values.Count() > 0)
                    algoReport.MaxNumSellOrderPerDay = Math.Max(model.NumSellOrderPerDay.Values.First(), algoReport.MaxNumSellOrderPerDay);

                total_market_volume += model.MarketVolume.Values.Sum();
                foreach (var marketVolumePair in model.MarketVolume)
                {
                    if (model.ExecutedVolume.ContainsKey(marketVolumePair.Key))
                    {
                        var executedVolume = model.ExecutedVolume[marketVolumePair.Key];
                        total_executed_volume += executedVolume;
                        if (marketVolumePair.Value > 0)
                        {
                            if (executedVolume / marketVolumePair.Value > algoReport.MaxMarketParticipationRate)
                            {
                                algoReport.MaxMarketParticipationRate = executedVolume / marketVolumePair.Value;
                                algoReport.MaxParticipationTime = marketVolumePair.Key.ToString("yyyyMMdd HH:mm");
                            }
                        }
                    }
                }

                algoReport.OrderSubmitTime.AddRange(model.OrderSubmitTime);
                algoReport.OrderSubmitVolume.AddRange(model.OrderSubmitVolume);
                algoReport.TotalVolumeInOneSecond += model.TotalVolumeInOneSecond;

                algoReport.TotalNumOrder += model.TotalNumOrder;
                algoReport.TotalVolumeOrder += model.TotalVolumeOrder;

                if (model.MaxNumberOfReversalInOneMinute > algoReport.MaxNumberOfReversalInOneMinute)
                {
                    algoReport.MaxNumberOfReversalInOneMinute = model.MaxNumberOfReversalInOneMinute;
                    algoReport.MaxNumberOfReversalTime = model.MaxNumberOfReversalTime;
                }

                algoReport.BuyQty += model.BuyQty;
                algoReport.BuyValue += model.BuyValue;
                algoReport.BuyNum += model.BuyNum;
                algoReport.SellQty += model.SellQty;
                algoReport.SellValue += model.SellValue;
                algoReport.SellNum += model.SellNum;
                if (model.MaxNumberOfOrdersMatchedInFiveMinutes > algoReport.MaxNumberOfOrdersMatchedInFiveMinutes)
                {
                    algoReport.MaxNumberOfOrdersMatchedInFiveMinutes = model.MaxNumberOfOrdersMatchedInFiveMinutes;
                    algoReport.MaxNumberOfOrdersMatchedTime = model.MaxNumberOfOrdersMatchedTime;
                    //if (model.BuyQty > 0 && model.SellQty > 0)
                    //{
                        algoReport.PnLOrdersMatchedInFiveMinutes = model.PnLOrdersMatchedInFiveMinutes;
                    //}
                }

                if (model.MaxOrderSizePerOrder > algoReport.MaxOrderSizePerOrder)
                {
                    algoReport.MaxOrderSizePerOrder = model.MaxOrderSizePerOrder;
                }

                if (model.MaxNumOrderPerSecond > algoReport.MaxNumOrderPerSecond)
                {
                    algoReport.MaxNumOrderPerSecond = model.MaxNumOrderPerSecond;
                }

                if (algoReport.MaxVolumeSentPerSecond < model.MaxVolumeSentPerSecond)
                {
                    algoReport.MaxVolumeSentPerSecond = model.MaxVolumeSentPerSecond;
                }

                foreach(var pair in model.Strategies)
                {
                    if (!algoReport.Strategies.ContainsKey(pair.Key))
                    {
                        algoReport.Strategies.Add(pair.Key, pair.Value);
                    }
                }
                algoReport.TotalDurationInSecond += model.TotalDurationInSecond;
                if (algoReport.MaxNumOrderPerSignal < model.MaxNumOrderPerSignal)
                {
                    algoReport.MaxNumOrderPerSignal = model.MaxNumOrderPerSignal;
                }
                if (algoReport.MaxNumOrderCancelledPerSignal < model.MaxNumOrderCancelledPerSignal)
                {
                    algoReport.MaxNumOrderCancelledPerSignal = model.MaxNumOrderCancelledPerSignal;
                }
                if (algoReport.MaxSlippageInTicks > model.MaxSlippageInTicks)
                {
                    algoReport.MaxSlippageInTicks = model.MaxSlippageInTicks;
                    algoReport.MaxSlippageInSpread = model.MaxSlippageInSpread;
                    algoReport.MaxSlippageDay = model.MaxSlippageDay;
                }
                if (algoReport.MinSlippageInTicks < model.MinSlippageInTicks)
                {
                    algoReport.MinSlippageInTicks = model.MinSlippageInTicks;
                    algoReport.MinSlippageInSpread = model.MinSlippageInSpread;
                    algoReport.MinSlippageDay = model.MinSlippageDay;
                }
                algoReport.TotalSlippage += model.TotalSlippage;
                algoReport.TotalSlippageSize += model.TotalSlippageSize;
                if (algoReport.MaxSizePerSignal < model.MaxSizePerSignal)
                {
                    algoReport.MaxSizePerSignal = model.MaxSizePerSignal;
                }
                algoReport.TotalSize += model.TotalSize;
                algoReport.TotalNumSignal += model.TotalNumSignal;
            }

            algoReport.AUM = list.Last().AUM;

            if (count > 0)
            {
                algoReport.AvgNumOrderPerDay = algoReport.AvgNumOrderPerDay / count;
            }

            if (total_market_volume > 0)
            {
                algoReport.AvgMarketParticipationRate = total_executed_volume / total_market_volume;
            }

            if (algoReport.TotalNumOrder > 0.1)
            {
                algoReport.AvgOrderSizePerOrder = algoReport.TotalVolumeOrder / algoReport.TotalNumOrder;
                algoReport.AvgNumOrderPerSecond = algoReport.TotalNumOrder / Math.Max(1, algoReport.TotalDurationInSecond);
                algoReport.AvgVolumeSentPerSecond = algoReport.TotalVolumeOrder / Math.Max(1, algoReport.TotalDurationInSecond);
                algoReport.AvgNumOrderPerSignal = algoReport.TotalNumOrder / algoReport.TotalNumSignal;
                algoReport.AvgSizePerSignal = algoReport.TotalSize / algoReport.TotalNumSignal;
            }

            if (algoReport.TotalSlippageSize > 0.1)
            {
                algoReport.AvgSlippage = algoReport.TotalSlippage / algoReport.TotalSlippageSize;
            }


            //while (algoReport.FillTime.Count > 0)
            //{
            //    if ((algoReport.FillTime[0] < childOrderEndTime.AddMinutes(-5)) || (algoReport.FillTime[0] > childOrderEndTime))
            //    {
            //        if (algoReport.FillDirection[0] == "Buy")
            //        {
            //            algoReport.BuyQty = Math.Max(0, algoReport.BuyQty - algoReport.FillQty[0]);
            //            algoReport.BuyValue = Math.Max(0, algoReport.BuyValue - algoReport.FillQty[0] * algoReport.FillPrice[0]);
            //            algoReport.BuyNum = Math.Max(0, algoReport.BuyNum - 1);
            //        }
            //        else
            //        {
            //            algoReport.SellQty = Math.Max(0, algoReport.SellQty - algoReport.FillQty[0]);
            //            algoReport.SellValue = Math.Max(0, algoReport.SellValue - algoReport.FillQty[0] * algoReport.FillPrice[0]);
            //            algoReport.SellNum = Math.Max(0, algoReport.SellNum - 1);
            //        }
            //        algoReport.FillTime.RemoveAt(0);
            //        algoReport.FillQty.RemoveAt(0);
            //        algoReport.FillPrice.RemoveAt(0);
            //        algoReport.FillDirection.RemoveAt(0);
            //    }
            //    else
            //    {
            //        break;
            //    }
            //}

            return algoReport;
        }
    }
}
