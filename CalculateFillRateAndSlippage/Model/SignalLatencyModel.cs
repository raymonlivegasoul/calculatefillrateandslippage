﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateFillRateAndSlippage.Model
{
    public class SignalLatencyModel
    {
        public DateTime Date { get; set; }
        public string ProdName { get; set; }
        public string MachineName { get; set; }
        public int SignalID { get; set; }
        public double CalcPriceLatency { get; set; }
        public double SubmitLatency { get; set; }
        public double CancelLatency { get; set; }
    }
}
