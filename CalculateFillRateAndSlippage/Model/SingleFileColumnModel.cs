﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace CalculateFillRateAndSlippage.Model
{
    public class SingleFileColumnModel
    {
        #region Reporting Properties
        public string Product { get; set; }
        public string Algo { get; set; }
        public string SignalKey { get; set; }
        public double Size { get; set; }
        public double NewSlippageInTicks { get; set; }
        public double SlippageInTicks { get; set; }
        public double NTSlippageInTicks { get; set; }
        public double SlippageAsSpreadRatio { get; set; }
        public double SpreadInTicks { get; set; }
        public double HighSpreadSize { get; set; }
        public double HighSpreadInTicks { get; set; }
        public double SlippageClosePriceInTicks { get; set; }
        public double SlippageInUSDPerTrade { get; set; }
        public double TotalSlippageInUSD { get; set; }
        public double SlippageAsAUMRatio { get; set; }
        public double FiveLargestSlippageInTicks { get; set; }
        public double TheoreticalOptimalSlippageInTicks { get; set; }
        public double DailyRangeInTicks { get; set; }
        public double SlippageAsDailyRangeRatio { get; set; }
        public double OverFilledSize { get; set; }
        public double OverFilledRatio { get; set; }
        public double CrossSniperFillSizeAfterWhile { get; set; }
        public double CrossSniperAfterWhileSlippageInTicks { get; set; }
        public double AverageDuration { get; set; }
        public double MaxDuration { get; set; }
        public double FarTouchRatio { get; set; }
        public double NearTouchRatio { get; set; }
        public double BBORatio { get; set; }
        public double ExceedingFarTouchRatio { get; set; }
        public double FillBetterMidGT1Spread { get; set; }
        public double FillBetterMid1Spread { get; set; }
        public double FillAtMid { get; set; }
        public double FillWorseMid1Spread { get; set; }
        public double FillWorseMid2Spread { get; set; }
        public double FillWorseMidGT2Spread { get; set; }
        public double FillWorseMidGT2SpreadSlippageAsSpreadRatio { get; set; }
        public double NumOfSniperOrders { get; set; }
        public double NumOfFloatOrders { get; set; }
        public double NumOfPegOrders { get; set; }
        public double NumOfPartialFillOrders { get; set; }
        public double NumOfUnFullFilledOrders { get; set; }
        public double TimeLag { get; set; }
        public double PriceMove { get; set; }
        public int LowSpreadFTRatioDropFloatSize { get; set; }
        public int LowSpreadFTRatioDropFloatFillSize { get; set; }
        public double LowSpreadFTRatioDropFloatFillRate { get; set; }
        public int LowSpreadFTRatioBeforeDropFloatFillSize { get; set; }
        public int LowSpreadFTRatioNeverDropFloatSize { get; set; }
        public double LowSpreadFTRatioNeverDropFloatFillSize { get; set; }
        public int HighSpreadFTRatioDropFloatSize { get; set; }
        public int HighSpreadFTRatioDropFloatFillSize { get; set; }
        public double HighSpreadFTRatioDropFloatFillRate { get; set; }
        public int HighSpreadFTRatioBeforeDropFloatFillSize { get; set; }
        public int HighSpreadFTRatioNeverDropFloatSize { get; set; }
        public double HighSpreadFTRatioNeverDropFloatFillSize { get; set; }
        public double FiveLargestCalcPriceLatency { get; set; }
        public double NumOfOrderSubmitted { get; set; }
        public double FiveLargestOrderSubmitLatency { get; set; }
        public double AverageOrderSubmitLatency { get; set; }
        public double NumOfOrderCancelled { get; set; }
        public double FiveLargestOrderCancelLatency { get; set; }
        public double AverageOrderCancelLatency { get; set; }
        public int FloatFirstUnFullFilledSize { get; set; }
        public double FloatFirstUnFullFilledSlippageInTicks { get; set; }
        public double FloatFirstUnFullFilledSlippageAsSpreadRatio { get; set; }
        public int FloatFirstLowSpreadUnFilledSize { get; set; }
        public double FloatFirstLowSpreadUnFilledSlippage { get; set; }
        public int FloatFirstHighSpreadUnFilledSize { get; set; }
        public double FloatFirstHighSpreadUnFilledSlippage { get; set; }
        public int FloatFirstFullFilledSize { get; set; }
        public double FloatFirstFullFilledSlippageInTicks { get; set; }
        public double FloatFirstFullFilledSlippageAsSpreadRatio { get; set; }
        public int SniperFirstUnFullFilledSize { get; set; }
        public double SniperFirstUnFullFilledSlippageInTicks { get; set; }
        public double SniperFirstUnFullFilledSlippageAsSpreadRatio { get; set; }
        public int SniperFirstFullFilledSize { get; set; }
        public double SniperFirstFullFilledSlippageInTicks { get; set; }
        public double SniperFirstFullFilledSlippageAsSpreadRatio { get; set; }
        public int FloatFirstSize { get; set; }
        public int FloatFirstFillSize { get; set; }
        public double FloatFirstFillRate { get; set; }
        public int SniperFirstSize { get; set; }
        public int SniperFirstFillSize { get; set; }
        public double SniperFirstFillRate { get; set; }
        public int CrossSniperSize { get; set; }
        public int CrossSniperFillSize { get; set; }
        public double CrossSniperFillRate { get; set; }
        public int FTSizeExceedSniperSize { get; set; }
        public int FTSizeExceedSniperFillSize { get; set; }
        public double FTSizeExceedSniperFillRate { get; set; }
        public int SweepBelowFTSize { get; set; }
        public int SweepBelowFTFillSize { get; set; }
        public double SweepBelowFTFillRate { get; set; }
        public int SweepAtFTSize { get; set; }
        public int SweepAtFTFillSize { get; set; }
        public double SweepAtFTFillRate { get; set; }
        public int SweepAboveFTSize { get; set; }
        public int SweepAboveFTFillSize { get; set; }
        public double SweepAboveFTFillRate { get; set; }
        public int OneSweepSize { get; set; }
        public int OneSweepFillSize { get; set; }
        public double OneSweepFillRate { get; set; }
        public int AllSniperSize { get; set; }
        public int AllSniperFillSize { get; set; }
        public double AllSniperFillRate { get; set; }
        public int OneSniperSize { get; set; }
        public int OneSniperFillSize { get; set; }
        public double OneSniperFillRate { get; set; }
        public int SniperNewNTSize { get; set; }
        public int SniperNewNTFillSize { get; set; }
        public double SniperNewNTFillRate { get; set; }
        public int OneSniperFillAsWholeSize { get; set; }
        public int OneFloatFillAsWholeSize { get; set; }
        public double FillIn1S { get; set; }
        public double FillIn2S { get; set; }
        public double FillIn3S { get; set; }
        public double FillIn4S { get; set; }
        public double FillIn5S { get; set; }
        public double FillIn6S { get; set; }
        public double FillIn7S { get; set; }
        public double FillIn8S { get; set; }
        public double FillIn9S { get; set; }
        public double FillIn10S { get; set; }
        public double LowSpreadFloatSizeFT { get; set; }
        public double LowSpreadFloatFillSizeFT { get; set; }
        public double LowSpreadFloatFillRateFT { get; set; }
        public double HighSpreadFloatSizeFT { get; set; }
        public double HighSpreadFloatFillSizeFT { get; set; }
        public double HighSpreadFloatFillRateFT { get; set; }
        public double AllFloatSizeFT { get; set; }
        public double AllFloatFillSizeFT { get; set; }
        public double AllFloatFillRateFT { get; set; }
        public double LowSpreadFloatSizeByNum { get; set; }
        public double LowSpreadFloatFillSizeByNum { get; set; }
        public double LowSpreadFloatFillRateByNum { get; set; }
        public double HighSpreadFloatSizeByNum { get; set; }
        public double HighSpreadFloatFillSizeByNum { get; set; }
        public double HighSpreadFloatFillRateByNum { get; set; }
        public double AllFloatSizeByNum { get; set; }
        public double AllFloatFillSizeByNum { get; set; }
        public double AllFloatFillRateByNum { get; set; }
        public double LowSpreadFloatSizeByLots { get; set; }
        public double LowSpreadFloatFillSizeByLots { get; set; }
        public double LowSpreadFloatFillRateByLots { get; set; }
        public double HighSpreadFloatSizeByLots { get; set; }
        public double HighSpreadFloatFillSizeByLots { get; set; }
        public double HighSpreadFloatFillRateByLots { get; set; }
        public double AllFloatSizeByLots { get; set; }
        public double AllFloatFillSizeByLots { get; set; }
        public double AllFloatFillRateByLots { get; set; }
        public double FloatSize1 { get; set; }
        public double FloatFillSize1 { get; set; }
        public double FloatFillRate1 { get; set; }
        public double FloatSize2 { get; set; }
        public double FloatFillSize2 { get; set; }
        public double FloatFillRate2 { get; set; }
        public double FloatSize3 { get; set; }
        public double FloatFillSize3 { get; set; }
        public double FloatFillRate3 { get; set; }
        public double FloatSize4 { get; set; }
        public double FloatFillSize4 { get; set; }
        public double FloatFillRate4 { get; set; }
        public double FloatSize5 { get; set; }
        public double FloatFillSize5 { get; set; }
        public double FloatFillRate5 { get; set; }
        public double LowSentSize0 { get; set; }
        public double LowFillSize0 { get; set; }
        public double LowUnFillSize0 { get; set; }
        public double LowUnFillSlippage0 { get; set; }
        public double HighSentSize0 { get; set; }
        public double HighFillSize0 { get; set; }
        public double HighUnFillSize0 { get; set; }
        public double HighUnFillSlippage0 { get; set; }
        public double LowSentSize1 { get; set; }
        public double LowFillSize1 { get; set; }
        public double LowUnFillSize1 { get; set; }
        public double LowUnFillSlippage1 { get; set; }
        public double HighSentSize1 { get; set; }
        public double HighFillSize1 { get; set; }
        public double HighUnFillSize1 { get; set; }
        public double HighUnFillSlippage1 { get; set; }
        public double LowSentSize2 { get; set; }
        public double LowFillSize2 { get; set; }
        public double LowUnFillSize2 { get; set; }
        public double LowUnFillSlippage2 { get; set; }
        public double HighSentSize2 { get; set; }
        public double HighFillSize2 { get; set; }
        public double HighUnFillSize2 { get; set; }
        public double HighUnFillSlippage2 { get; set; }
        public double LowSentSize3 { get; set; }
        public double LowFillSize3 { get; set; }
        public double LowUnFillSize3 { get; set; }
        public double LowUnFillSlippage3 { get; set; }
        public double HighSentSize3 { get; set; }
        public double HighFillSize3 { get; set; }
        public double HighUnFillSize3 { get; set; }
        public double HighUnFillSlippage3 { get; set; }
        public double LowSentSize4 { get; set; }
        public double LowFillSize4 { get; set; }
        public double LowUnFillSize4 { get; set; }
        public double LowUnFillSlippage4 { get; set; }
        public double HighSentSize4 { get; set; }
        public double HighFillSize4 { get; set; }
        public double HighUnFillSize4 { get; set; }
        public double HighUnFillSlippage4 { get; set; }
        public double LowSentSize5 { get; set; }
        public double LowFillSize5 { get; set; }
        public double LowUnFillSize5 { get; set; }
        public double LowUnFillSlippage5 { get; set; }
        public double HighSentSize5 { get; set; }
        public double HighFillSize5 { get; set; }
        public double HighUnFillSize5 { get; set; }
        public double HighUnFillSlippage5 { get; set; }
        public double LowSentSize6 { get; set; }
        public double LowFillSize6 { get; set; }
        public double LowUnFillSize6 { get; set; }
        public double LowUnFillSlippage6 { get; set; }
        public double HighSentSize6 { get; set; }
        public double HighFillSize6 { get; set; }
        public double HighUnFillSize6 { get; set; }
        public double HighUnFillSlippage6 { get; set; }
        public double LowSentSize7 { get; set; }
        public double LowFillSize7 { get; set; }
        public double LowUnFillSize7 { get; set; }
        public double LowUnFillSlippage7 { get; set; }
        public double HighSentSize7 { get; set; }
        public double HighFillSize7 { get; set; }
        public double HighUnFillSize7 { get; set; }
        public double HighUnFillSlippage7 { get; set; }
        public double LowSentSize8 { get; set; }
        public double LowFillSize8 { get; set; }
        public double LowUnFillSize8 { get; set; }
        public double LowUnFillSlippage8 { get; set; }
        public double HighSentSize8 { get; set; }
        public double HighFillSize8 { get; set; }
        public double HighUnFillSize8 { get; set; }
        public double HighUnFillSlippage8 { get; set; }
        public double LowSentSize9 { get; set; }
        public double LowFillSize9 { get; set; }
        public double LowUnFillSize9 { get; set; }
        public double LowUnFillSlippage9 { get; set; }
        public double HighSentSize9 { get; set; }
        public double HighFillSize9 { get; set; }
        public double HighUnFillSize9 { get; set; }
        public double HighUnFillSlippage9 { get; set; }
        public double LotsOfNormalCrossSniperSentAtFT { get; set; }
        public double LotsOfNormalCrossSniperFilledAtFT { get; set; }
        public int RatioOfNormalCrossSniperFilledAtFT { get; set; }
        public double LotsOfFTReductionCrossSniperSentAtFT { get; set; }
        public double LotsOfFTReductionCrossSniperFilledAtFT { get; set; }
        public int RatioOfFTReductionCrossSniperFilledAtFT { get; set; }
        public double LotsOfNormalCrossSniperSentAtFT1 { get; set; }
        public double LotsOfNormalCrossSniperFilledAtFT1 { get; set; }
        public int RatioOfNormalCrossSniperFilledAtFT1 { get; set; }
        public double LotsOfFTReductionCrossSniperSentAtFT1 { get; set; }
        public double LotsOfFTReductionCrossSniperFilledAtFT1 { get; set; }
        public int RatioOfFTReductionCrossSniperFilledAtFT1 { get; set; }
        public string Contract { get; set; }
        public DateTime TriggerTime { get; set; }
        public double ArrivalBestBid { get; set; }
        public double ArrivalBestAsk { get; set; }
        public double ArrivalBestBidSize { get; set; }
        public double ArrialBestAskSize { get; set; }
        public double FirstBestBid { get; set; }
        public double FirstBestAsk { get; set; }
        public double FirstBestBidSize { get; set; }
        public double FirstBestAskSize { get; set; }
        public double SignalSummarySize { get; set; }
        public double LowStaticThreshold { get; set; }
        public double HighStaticThreshold { get; set; }
        #endregion

        // Non reporting properties starting from here
        public DateTime Date { get; set; }

        public static SingleFileColumnModel FromString(string content)
        {
            return JsonConvert.DeserializeObject<SingleFileColumnModel>(content);
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }

        public void SetFloatSizeAndFill(int index, double size, double filledSize)
        {
            switch (index)
            {
                case 0:
                    FloatSize1 += size;
                    FloatFillSize1 += filledSize;
                    FloatFillRate1 = FloatFillSize1 / (double)FloatSize1;
                    break;
                case 1:
                    FloatSize2 += size;
                    FloatFillSize2 += filledSize;
                    FloatFillRate2 = FloatFillSize2 / (double)FloatSize2;
                    break;
                case 2:
                    FloatSize3 += size;
                    FloatFillSize3 += filledSize;
                    FloatFillRate3 = FloatFillSize3 / (double)FloatSize3;
                    break;
                case 3:
                    FloatSize4 += size;
                    FloatFillSize4 += filledSize;
                    FloatFillRate4 = FloatFillSize4 / (double)FloatSize4;
                    break;
                case 4:
                    FloatSize5 += size;
                    FloatFillSize5 += filledSize;
                    FloatFillRate5 = FloatFillSize5 / (double)FloatSize5;
                    break;
            }
        }

        public void SetSentAndFillSize(int index, bool isLowSpread, double size, double filledSize)
        {
            switch (index)
            {
                case 0:
                    if (isLowSpread)
                    {
                        LowSentSize0 += size;
                        LowFillSize0 += filledSize;
                        LowUnFillSize0 = FloatFirstLowSpreadUnFilledSize;
                        LowUnFillSlippage0 = FloatFirstLowSpreadUnFilledSlippage;
                    }
                    else
                    {
                        HighSentSize0 += size;
                        HighFillSize0 += filledSize;
                        HighUnFillSize0 = FloatFirstHighSpreadUnFilledSize;
                        HighUnFillSlippage0 = FloatFirstHighSpreadUnFilledSlippage;
                    }
                    break;
                case 1:
                    if (isLowSpread)
                    {
                        LowSentSize1 += size;
                        LowFillSize1 += filledSize;
                        LowUnFillSize1 = FloatFirstLowSpreadUnFilledSize;
                        LowUnFillSlippage1 = FloatFirstLowSpreadUnFilledSlippage;
                    }
                    else
                    {
                        HighSentSize1 += size;
                        HighFillSize1 += filledSize;
                        HighUnFillSize1 = FloatFirstHighSpreadUnFilledSize;
                        HighUnFillSlippage1 = FloatFirstHighSpreadUnFilledSlippage;
                    }
                    break;
                case 2:
                    if (isLowSpread)
                    {
                        LowSentSize2 += size;
                        LowFillSize2 += filledSize;
                        LowUnFillSize2 = FloatFirstLowSpreadUnFilledSize;
                        LowUnFillSlippage2 = FloatFirstLowSpreadUnFilledSlippage;
                    }
                    else
                    {
                        HighSentSize2 += size;
                        HighFillSize2 += filledSize;
                        HighUnFillSize2 = FloatFirstHighSpreadUnFilledSize;
                        HighUnFillSlippage2 = FloatFirstHighSpreadUnFilledSlippage;
                    }
                    break;
                case 3:
                    if (isLowSpread)
                    {
                        LowSentSize3 += size;
                        LowFillSize3 += filledSize;
                        LowUnFillSize3 = FloatFirstLowSpreadUnFilledSize;
                        LowUnFillSlippage3 = FloatFirstLowSpreadUnFilledSlippage;
                    }
                    else
                    {
                        HighSentSize3 += size;
                        HighFillSize3 += filledSize;
                        HighUnFillSize3 = FloatFirstHighSpreadUnFilledSize;
                        HighUnFillSlippage3 = FloatFirstHighSpreadUnFilledSlippage;
                    }
                    break;
                case 4:
                    if (isLowSpread)
                    {
                        LowSentSize4 += size;
                        LowFillSize4 += filledSize;
                        LowUnFillSize4 = FloatFirstLowSpreadUnFilledSize;
                        LowUnFillSlippage4 = FloatFirstLowSpreadUnFilledSlippage;
                    }
                    else
                    {
                        HighSentSize4 += size;
                        HighFillSize4 += filledSize;
                        HighUnFillSize4 = FloatFirstHighSpreadUnFilledSize;
                        HighUnFillSlippage4 = FloatFirstHighSpreadUnFilledSlippage;
                    }
                    break;
                case 5:
                    if (isLowSpread)
                    {
                        LowSentSize5 += size;
                        LowFillSize5 += filledSize;
                        LowUnFillSize5 = FloatFirstLowSpreadUnFilledSize;
                        LowUnFillSlippage5 = FloatFirstLowSpreadUnFilledSlippage;
                    }
                    else
                    {
                        HighSentSize5 += size;
                        HighFillSize5 += filledSize;
                        HighUnFillSize5 = FloatFirstHighSpreadUnFilledSize;
                        HighUnFillSlippage5 = FloatFirstHighSpreadUnFilledSlippage;
                    }
                    break;
                case 6:
                    if (isLowSpread)
                    {
                        LowSentSize6 += size;
                        LowFillSize6 += filledSize;
                        LowUnFillSize6 = FloatFirstLowSpreadUnFilledSize;
                        LowUnFillSlippage6 = FloatFirstLowSpreadUnFilledSlippage;
                    }
                    else
                    {
                        HighSentSize6 += size;
                        HighFillSize6 += filledSize;
                        HighUnFillSize6 = FloatFirstHighSpreadUnFilledSize;
                        HighUnFillSlippage6 = FloatFirstHighSpreadUnFilledSlippage;
                    }
                    break;
                case 7:
                    if (isLowSpread)
                    {
                        LowSentSize7 += size;
                        LowFillSize7 += filledSize;
                        LowUnFillSize7 = FloatFirstLowSpreadUnFilledSize;
                        LowUnFillSlippage7 = FloatFirstLowSpreadUnFilledSlippage;
                    }
                    else
                    {
                        HighSentSize7 += size;
                        HighFillSize7 += filledSize;
                        HighUnFillSize7 = FloatFirstHighSpreadUnFilledSize;
                        HighUnFillSlippage7 = FloatFirstHighSpreadUnFilledSlippage;
                    }
                    break;
                case 8:
                    if (isLowSpread)
                    {
                        LowSentSize8 += size;
                        LowFillSize8 += filledSize;
                        LowUnFillSize8 = FloatFirstLowSpreadUnFilledSize;
                        LowUnFillSlippage8 = FloatFirstLowSpreadUnFilledSlippage;
                    }
                    else
                    {
                        HighSentSize8 += size;
                        HighFillSize8 += filledSize;
                        HighUnFillSize8 = FloatFirstHighSpreadUnFilledSize;
                        HighUnFillSlippage8 = FloatFirstHighSpreadUnFilledSlippage;
                    }
                    break;
                case 9:
                    if (isLowSpread)
                    {
                        LowSentSize9 += size;
                        LowFillSize9 += filledSize;
                        LowUnFillSize9 = FloatFirstLowSpreadUnFilledSize;
                        LowUnFillSlippage9 = FloatFirstLowSpreadUnFilledSlippage;
                    }
                    else
                    {
                        HighSentSize9 += size;
                        HighFillSize9 += filledSize;
                        HighUnFillSize9 = FloatFirstHighSpreadUnFilledSize;
                        HighUnFillSlippage9 = FloatFirstHighSpreadUnFilledSlippage;
                    }
                    break;
            }
        }
    }

    public class SingleFileColumnModelList
    {
        public DateTime Date { get; set; }
        public List<SingleFileColumnModel> Data { get; set; }
        public string Product { get; set; }
        public string Key
        {
            get { return Product + "-" + Date.ToString("yyyyMMdd"); }
        }
        public Dictionary<string, AlgoReportStatModel> AlgoReport { get; set; }
        public int NumAllOrders { get; set; }
        public int NumSkippedOrders { get; set; }

        public override string ToString()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };
            return JsonConvert.SerializeObject(this);
        }

        public static SingleFileColumnModelList FromString(string content)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };
            return JsonConvert.DeserializeObject<SingleFileColumnModelList>(content);
        }

        public SingleFileColumnModelList()
        {
            AlgoReport = new Dictionary<string, AlgoReportStatModel>();
            Data = new List<SingleFileColumnModel>();
        }
    }
}
