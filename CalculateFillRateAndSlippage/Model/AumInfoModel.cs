﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateFillRateAndSlippage.Model
{
    public class AumInfoModel
    {
        public string Type { get; set; }
        public double ST { get; set; }
        public double LT { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
