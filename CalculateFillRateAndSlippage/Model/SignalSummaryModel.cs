﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateFillRateAndSlippage.Model
{
    public class SignalSummaryModel
    {
        public string ProductName { get; set; }
        public string MachineName { get; set; }
        public string Algo { get; set; }
        public DateTime Date { get; set; }
        public int SignalID { get; set; }
        public int MasterOrderID { get; set; }
        public int Size { get; set; }
        public int FilledSize { get; set; }
        public double FilledPrice { get; set; }
        public string Direction { get; set; }
        public string Strategy { get; set; }
        public string ChildType { get; set; }
        public string Status { get; set; }
        public int ExceedFatFingerLimit { get; set; }
        public double AbnormalFillPrice { get; set; }
        public double OrderPrice { get; set; }
        public double BidPrice { get; set; }
        public double AskPrice { get; set; }
        public int BidSize { get; set; }
        public int AskSize { get; set; }
        public double Round0 { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public bool IsBuy
        {
            get { return Direction.ToUpper() == "BUY"; }
        }

        public SignalSummaryModel(DataRow row)
        {
            try
            {
                ProductName = row["ProdName"].ToString();
                MachineName = row["MachineName"].ToString().ToUpper();
                Algo = row["AlgoName"].ToString();
                Date = (DateTime)row["Date"];
                SignalID = int.Parse(row["SignalID"].ToString());
                MasterOrderID = int.Parse(row["MasterOrderID"].ToString());
                Size = (int)(double.Parse(row["Size"].ToString()) + 0.5);
                if (!string.IsNullOrEmpty(row["FilledSize"].ToString())) FilledSize = (int)(double.Parse(row["FilledSize"].ToString()) + 0.5);
                if (!string.IsNullOrEmpty(row["FilledPrice"].ToString())) FilledPrice = double.Parse(row["FilledPrice"].ToString());
                Direction = row["Direction"].ToString();
                Strategy = row["StrategyName"].ToString();
                ChildType = row["ChildType"].ToString();
                Status = row["Status"].ToString();
                ExceedFatFingerLimit = (int)double.Parse(row["ExceedFatFingerLimit"].ToString());
                AbnormalFillPrice = double.Parse(row["AbnormalFillPrice"].ToString());
                OrderPrice = double.Parse(row["OrderPrice"].ToString());
                BidPrice = double.Parse(row["BidPrice"].ToString());
                AskPrice = double.Parse(row["AskPrice"].ToString());
                BidSize = (int)double.Parse(row["BidSize"].ToString());
                AskSize = (int)double.Parse(row["AskSize"].ToString());
                Round0 = double.Parse(row["Round0"].ToString());
                StartTime = (DateTime)row["StartTime"];
                EndTime = (DateTime)row["EndTime"];
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
