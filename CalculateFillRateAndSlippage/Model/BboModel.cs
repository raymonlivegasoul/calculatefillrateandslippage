﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculateFillRateAndSlippage.Model
{
    public class BboModel
    {
        #region Properties
        public DateTime Date { get; set; }                  // 00
        public int MasterSignalID { get; set; }             // 01
        public string MachineName { get; set; }             // 02
        public double SignalBestBid { get; set; }           // 03
        public double SignalBestAsk { get; set; }           // 04
        public int SignalBestBidSize { get; set; }       // 05
        public int SignalBestAskSize { get; set; }       // 06
        public int ChildSignalID { get; set; }           // 07
        public double FilledPrice { get; set; }             // 08
        public int FilledSize { get; set; }              // 09
        public DateTime TriggerTime { get; set; }           // 10
        public DateTime FilledTime { get; set; }            // 11
        public DateTime SubmitTime { get; set; }            // 12
        public string AlgoEngine { get; set; }              // 13
        public string Strategy { get; set; }                // 14
        public string Direction { get; set; }               // 15
        public double Risk { get; set; }                    // 16
        public int TagMaxPos { get; set; }                  // 17
        public bool IsFound { get; set; }                   // 18
        public string ProdName { get; set; }
        public double ExchangeRate { get; set; }
        public double PointValue { get; set; }
        public string Contract { get; set; }
        public string Aum { get; set; }
        public string Custom3 { get; set; }
        public string Custom6 { get; set; }
        public bool IsEntry { get; set; }
        public double FilledPriceInCustom3 { get; set; }
        public double FilledSizeInCustom3 { get; set; }
        #endregion

        public void Load(DataRow row)
        {
            Date = DateTime.Parse(row["TradeDate"].ToString());
            ProdName = row["FutureSymbol"].ToString();
            Risk = double.Parse(row["Custom1"].ToString());
            Custom3 = row["Custom3"].ToString();
            Custom6 = row["Custom6"].ToString();
            Strategy = row["TradeAttribute"].ToString();
            //string executionAccount = row["ExecutionAccount"].ToString();

            // remove the "V" prefix of the strategy name
            if (Strategy.StartsWith("V"))
            {
                Strategy = Strategy.Substring(1);
            }

            MasterSignalID = 0; // master order ID
            ChildSignalID = 0; // child order ID
            double arrivalpx = 0; // arrival price saved in custom3, not used
            SignalBestBid = 0; // arrival best bid price
            SignalBestAsk = 0; // arrival best ask price
            SignalBestBidSize = 0; // arrival best bid size
            SignalBestAskSize = 0; // arrival best ask size
            FilledPrice = 0; // fill price
            FilledSize = 0; // fill size                           
            FilledPriceInCustom3 = 0; //fill price in custom3
            FilledSizeInCustom3 = 0; // fill size in custom3
            IsEntry = true; // entry or exit order
            Direction = ""; // Buy or Sell
            TriggerTime = DateTime.MinValue; // trigger time of master order
            FilledTime = DateTime.MinValue; // order fill time
            SubmitTime = DateTime.MinValue; // order submit time
            AlgoEngine = ""; // algo name  
            TagMaxPos = 0;

            PointValue = 0; // point value
            ExchangeRate = 0; // currency rate
            MachineName = ""; // machine name
            Contract = ""; // contract name
            Aum = "0"; // AUM

            // parsing fill price and fill size
            FilledPrice = double.Parse(row["TradePrice"].ToString());
            FilledSize = (int)(Math.Abs(double.Parse(row["Quantity"].ToString())) + 0.5);

            // parsing columns in custom3
            string[] items = Custom3.Split(';');
            foreach (string thisItem in items)
            {
                var array = thisItem.Split('=');
                if (array.Length < 2) continue;
                var value = array[1];
                try
                {
                    switch (array[0])
                    {
                        case "MasterSignalID":
                            MasterSignalID = int.Parse(value);
                            break;
                        case "ChildSignalID":
                            ChildSignalID = int.Parse(value);
                            break;
                        case "ArrivalPrice":
                            arrivalpx = double.Parse(value);
                            break;
                        case "SignalBestBid":
                            SignalBestBid = double.Parse(value);
                            break;
                        case "SignalBestAsk":
                            SignalBestAsk = double.Parse(value);
                            break;
                        case "SignalBestBidSize":
                            SignalBestBidSize = int.Parse(value);
                            break;
                        case "SignalBestAskSize":
                            SignalBestAskSize = int.Parse(value);
                            break;
                        case "FilledPrice":
                            FilledPriceInCustom3 = double.Parse(value);
                            break;
                        case "FilledSize":
                            FilledSizeInCustom3 = int.Parse(value);
                            break;
                        case "Direction":
                            Direction = value;
                            break;
                        case "Entry":
                            IsEntry = value.Equals("True");
                            break;
                        case "TriggerTime":
                            TriggerTime = DateTime.Parse(value);
                            break;
                        case "FilledTime":
                            FilledTime = DateTime.Parse(value);
                            break;
                        case "SubmitTime":
                            SubmitTime = DateTime.Parse(value);
                            break;
                        case "AlgoEngine":
                            AlgoEngine = value;
                            break;
                        case "TagMaxPos":
                            TagMaxPos = int.Parse(value);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            // parsing custom6
            items = Custom6.Split(';');
            foreach (string thisItem in items)
            {
                var array = thisItem.Split('=');
                if (array.Length < 2) continue;
                var value = array[1];
                switch (array[0])
                {
                    case "PointValue":
                        PointValue = double.Parse(value);
                        break;
                    case "CurrencyRate":
                        ExchangeRate = double.Parse(value);
                        break;
                    case "MachineName":
                        MachineName = value.Trim().ToUpper();
                        break;
                    case "Contract":
                        Contract = value.Trim().ToUpper();
                        break;
                    case "Aum":
                        Aum = value.Trim().ToUpper();
                        break;
                }
            }
        }
    }

}
