﻿using CalculateFillRateAndSlippage.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Configuration;
using Utility.Database;

namespace CalculateFillRateAndSlippage
{
    public class ReportViewModel
    {
        #region Properties
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        // signalFileLines is used to store all master signals of master signal report
        List<SingleFileColumnModel> signalFileLines = new List<SingleFileColumnModel>();
        // signalFileColumnNames is the columns of master signal report
        List<string> signalFileColumnNames = new List<string>();
        // fileLines is used to store the final slippage report
        List<string[]> fileLines = new List<string[]>();
        // fileColumnNames is the columns of master signal report
        List<string> fileColumnNames = new List<string>();
        // maskfileColumnNames is the columns we don't want to show in the final slippage report
        List<string> maskfileColumnNames = new List<string>();
        string fileNamePostfix = string.Empty;

        string outputPath = string.Empty;
        // num of all child orders in signal summary
        long numAllOrders = 0;
        // num of child orders need to be skipped
        long numSkippedOrders = 0;
        long numBBOEqualOrders = 0;
        Dictionary<string, AlgoReportStatModel> AlgoReport;
        StringBuilder sbFillRateSpread;
        StringBuilder sbSkippedOrders;

        Dictionary<string, int> machineProdDateList = new Dictionary<string, int>();
        Dictionary<string, double> productDailyRangeDict = new Dictionary<string, double>();
        Dictionary<string, double> productDayCloseDict = new Dictionary<string, double>();
        Dictionary<string, double> prodSlippageDict = new Dictionary<string, double>();

        #endregion

        public void BuildReport()
        {
            // summation and maximization on all master signals
            SummarizeMasterSingal();
            Logger.PrintLogWithTimeStamp("Master signal processed.");

            // calculate final result (most of them are average) for each columns
            CalculateFinalResult();
            Logger.PrintLogWithTimeStamp("Final result processed.");

            // write all master signal files
            WriteReportFiles();
            Logger.PrintLogWithTimeStamp("Report generated.");
        }

        public void Init(string startDate, string endDate, DataViewModel dataModel, List<SingleFileColumnModelList> list)
        {
            StartDate = startDate;
            EndDate = endDate;
            fileNamePostfix = (StartDate.Equals(EndDate) ? StartDate : StartDate + "-" + EndDate);
            outputPath = Const.WorkingFolder;

            machineProdDateList = dataModel.MachineProdDateList;
            numBBOEqualOrders = dataModel.NumBBOEqualOrders;
            productDailyRangeDict = dataModel.ProductDailyRangeDict;
            productDayCloseDict = dataModel.ProductDayCloseDict;
            var algoReports = new List<Dictionary<string, AlgoReportStatModel>>();
            signalFileLines = new List<SingleFileColumnModel>();
            foreach(var item in list)
            {
                algoReports.Add(item.AlgoReport);
                signalFileLines.AddRange(item.Data);
            }
            AlgoReport = AlgoReportStatModel.Merge(algoReports);
            InitAlgoReport();

            // the log file 
            sbFillRateSpread = new StringBuilder();
            // skipped order file, saving the info of all skipped child orders
            sbSkippedOrders = new StringBuilder();

            signalFileColumnNames = new List<string>();
            fileLines = new List<string[]>();
            fileColumnNames = new List<string>();
            maskfileColumnNames = new List<string>();

            #region
            signalFileColumnNames = new List<string>(new string[] {
                "Product",
                "Algo",
                "SignalKey",
                "Size",
                "NewSlippageInTicks",
                "SlippageInTicks",
                "NTSlippageInTicks",
                "SlippageAsSpreadRatio",
                "SpreadInTicks",
                "HighSpreadSize",
                "HighSpreadInTicks",
                "SlippageClosePriceInTicks",
                "SlippageInUSDPerTrade",
                "TotalSlippageInUSD",
                "SlippageAsAUMRatio",
                "5LargestSlippageInTicks",
                "TheoreticalOptimalSlippageInTicks",
                "DailyRangeInTicks",
                "SlippageAsDailyRangeRatio",
                "OverFilledSize",
                "OverFilledRatio",
                "CrossSniperFillSizeAfterWhile",
                "CrossSniperAfterWhileSlippageInTicks",
                "AverageDuration",
                "MaxDuration",
                "FarTouchRatio",
                "NearTouchRatio",
                "BBORatio",
                "ExceedingFarTouchRatio",
                "FillBetterMidGT1Spread",
                "FillBetterMid1Spread",
                "FillAtMid",
                "FillWorseMid1Spread",
                "FillWorseMid2Spread",
                "FillWorseMidGT2Spread",
                "FillWorseMidGT2SpreadSlippageAsSpreadRatio",
                "NumOfSniperOrders",
                "NumOfFloatOrders",
                "NumOfPegOrders",
                "NumOfPartialFillOrders",
                "NumOfUnFullFilledOrders",
                "TimeLag",
                "PriceMove",
                "LowSpreadFTRatioDropFloatSize",
                "LowSpreadFTRatioDropFloatFillSize",
                "LowSpreadFTRatioDropFloatFillRate",
                "LowSpreadFTRatioBeforeDropFloatFillSize",
                "LowSpreadFTRatioNeverDropFloatSize",
                "LowSpreadFTRatioNeverDropFloatFillSize",
                "HighSpreadFTRatioDropFloatSize",
                "HighSpreadFTRatioDropFloatFillSize",
                "HighSpreadFTRatioDropFloatFillRate",
                "HighSpreadFTRatioBeforeDropFloatFillSize",
                "HighSpreadFTRatioNeverDropFloatSize",
                "HighSpreadFTRatioNeverDropFloatFillSize",
                "5LargestCalcPriceLatency",
                "NumOfOrderSubmitted",
                "5LargestOrderSubmitLatency",
                "AverageOrderSubmitLatency",
                "NumOfOrderCancelled",
                "5LargestOrderCancelLatency",
                "AverageOrderCancelLatency",
                "FloatFirstUnFullFilledSize",
                "FloatFirstUnFullFilledSlippageInTicks",
                "FloatFirstUnFullFilledSlippageAsSpreadRatio",
                "FloatFirstLowSpreadUnFilledSize",
                "FloatFirstLowSpreadUnFilledSlippage",
                "FloatFirstHighSpreadUnFilledSize",
                "FloatFirstHighSpreadUnFilledSlippage",
                "FloatFirstFullFilledSize",
                "FloatFirstFullFilledSlippageInTicks",
                "FloatFirstFullFilledSlippageAsSpreadRatio",
                "SniperFirstUnFullFilledSize",
                "SniperFirstUnFullFilledSlippageInTicks",
                "SniperFirstUnFullFilledSlippageAsSpreadRatio",
                "SniperFirstFullFilledSize",
                "SniperFirstFullFilledSlippageInTicks",
                "SniperFirstFullFilledSlippageAsSpreadRatio",
                "FloatFirstSize",
                "FloatFirstFillSize",
                "FloatFirstFillRate",
                "SniperFirstSize",
                "SniperFirstFillSize",
                "SniperFirstFillRate",
                "CrossSniperSize",
                "CrossSniperFillSize",
                "CrossSniperFillRate",
                "FTSizeExceedSniperSize",
                "FTSizeExceedSniperFillSize",
                "FTSizeExceedSniperFillRate",
                "SweepBelowFTSize",
                "SweepBelowFTFillSize",
                "SweepBelowFTFillRate",
                "SweepAtFTSize",
                "SweepAtFTFillSize",
                "SweepAtFTFillRate",
                "SweepAboveFTSize",
                "SweepAboveFTFillSize",
                "SweepAboveFTFillRate",
                "1SweepSize",
                "1SweepFillSize",
                "1SweepFillRate",
                "AllSniperSize",
                "AllSniperFillSize",
                "AllSniperFillRate",
                "1SniperSize",
                "1SniperFillSize",
                "1SniperFillRate",
                "SniperNewNTSize",
                "SniperNewNTFillSize",
                "SniperNewNTFillRate",
                "1SniperFillAsWholeSize",
                "1FloatFillAsWholeSize",
                "FillIn1S",
                "FillIn2S",
                "FillIn3S",
                "FillIn4S",
                "FillIn5S",
                "FillIn6S",
                "FillIn7S",
                "FillIn8S",
                "FillIn9S",
                "FillIn10S",
                "LowSpreadFloatSizeFT",
                "LowSpreadFloatFillSizeFT",
                "LowSpreadFloatFillRateFT",
                "HighSpreadFloatSizeFT",
                "HighSpreadFloatFillSizeFT",
                "HighSpreadFloatFillRateFT",
                "AllFloatSizeFT",
                "AllFloatFillSizeFT",
                "AllFloatFillRateFT",
                "LowSpreadFloatSizeByNum",
                "LowSpreadFloatFillSizeByNum",
                "LowSpreadFloatFillRateByNum",
                "HighSpreadFloatSizeByNum",
                "HighSpreadFloatFillSizeByNum",
                "HighSpreadFloatFillRateByNum",
                "AllFloatSizeByNum",
                "AllFloatFillSizeByNum",
                "AllFloatFillRateByNum",
                "LowSpreadFloatSizeByLots",
                "LowSpreadFloatFillSizeByLots",
                "LowSpreadFloatFillRateByLots",
                "HighSpreadFloatSizeByLots",
                "HighSpreadFloatFillSizeByLots",
                "HighSpreadFloatFillRateByLots",
                "AllFloatSizeByLots",
                "AllFloatFillSizeByLots",
                "AllFloatFillRateByLots",
                "1FloatSize",
                "1FloatFillSize",
                "1FloatFillRate",
                "2FloatSize",
                "2FloatFillSize",
                "2FloatFillRate",
                "3FloatSize",
                "3FloatFillSize",
                "3FloatFillRate",
                "4FloatSize",
                "4FloatFillSize",
                "4FloatFillRate",
                "5FloatSize",
                "5FloatFillSize",
                "5FloatFillRate",
                "0LowSentSize",
                "0LowFillSize",
                "0LowUnFillSize",
                "0LowUnFillSlippage",
                "0HighSentSize",
                "0HighFillSize",
                "0HighUnFillSize",
                "0HighUnFillSlippage",
                "1LowSentSize",
                "1LowFillSize",
                "1LowUnFillSize",
                "1LowUnFillSlippage",
                "1HighSentSize",
                "1HighFillSize",
                "1HighUnFillSize",
                "1HighUnFillSlippage",
                "2LowSentSize",
                "2LowFillSize",
                "2LowUnFillSize",
                "2LowUnFillSlippage",
                "2HighSentSize",
                "2HighFillSize",
                "2HighUnFillSize",
                "2HighUnFillSlippage",
                "3LowSentSize",
                "3LowFillSize",
                "3LowUnFillSize",
                "3LowUnFillSlippage",
                "3HighSentSize",
                "3HighFillSize",
                "3HighUnFillSize",
                "3HighUnFillSlippage",
                "4LowSentSize",
                "4LowFillSize",
                "4LowUnFillSize",
                "4LowUnFillSlippage",
                "4HighSentSize",
                "4HighFillSize",
                "4HighUnFillSize",
                "4HighUnFillSlippage",
                "5LowSentSize",
                "5LowFillSize",
                "5LowUnFillSize",
                "5LowUnFillSlippage",
                "5HighSentSize",
                "5HighFillSize",
                "5HighUnFillSize",
                "5HighUnFillSlippage",
                "6LowSentSize",
                "6LowFillSize",
                "6LowUnFillSize",
                "6LowUnFillSlippage",
                "6HighSentSize",
                "6HighFillSize",
                "6HighUnFillSize",
                "6HighUnFillSlippage",
                "7LowSentSize",
                "7LowFillSize",
                "7LowUnFillSize",
                "7LowUnFillSlippage",
                "7HighSentSize",
                "7HighFillSize",
                "7HighUnFillSize",
                "7HighUnFillSlippage",
                "8LowSentSize",
                "8LowFillSize",
                "8LowUnFillSize",
                "8LowUnFillSlippage",
                "8HighSentSize",
                "8HighFillSize",
                "8HighUnFillSize",
                "8HighUnFillSlippage",
                "9LowSentSize",
                "9LowFillSize",
                "9LowUnFillSize",
                "9LowUnFillSlippage",
                "9HighSentSize",
                "9HighFillSize",
                "9HighUnFillSize",
                "9HighUnFillSlippage",
                "LotsOfNormalCrossSniperSentAtFT",
                "LotsOfNormalCrossSniperFilledAtFT",
                "RatioOfNormalCrossSniperFilledAtFT",
                "LotsOfFTReductionCrossSniperSentAtFT",
                "LotsOfFTReductionCrossSniperFilledAtFT",
                "RatioOfFTReductionCrossSniperFilledAtFT",
                "LotsOfNormalCrossSniperSentAtFT+1",
                "LotsOfNormalCrossSniperFilledAtFT+1",
                "RatioOfNormalCrossSniperFilledAtFT+1",
                "LotsOfFTReductionCrossSniperSentAtFT+1",
                "LotsOfFTReductionCrossSniperFilledAtFT+1",
                "RatioOfFTReductionCrossSniperFilledAtFT+1",
                "Contract",
                "TriggerTime",
                "ArrivalBestBid",
                "ArrivalBestAsk",
                "ArrivalBestBidSize",
                "ArrialBestAskSize",
                "FirstBestBid",
                "FirstBestAsk",
                "FirstBestBidSize",
                "FirstBestAskSize",
                "SignalSummarySize",
                "LowStaticThreshold",
                "HighStaticThreshold"
            });

            fileColumnNames = new List<string>(new string[]{
                "Product",
                "Algo",
                "NumOfMasterOrders",
                "Size",
                "NewSlippageInTicks",
                "SlippageInTicks",
                "NTSlippageInTicks",
                "SlippageAsSpreadRatio",
                "SpreadInTicks",
                "HighSpreadSize",
                "HighSpreadInTicks",
                "SlippageClosePriceInTicks",
                "SlippageInUSDPerTrade",
                "TotalSlippageInUSD",
                "SlippageAsAUMRatio",
                "5LargestSlippageInTicks",
                "TheoreticalOptimalSlippageInTicks",
                "DailyRangeInTicks",
                "SlippageAsDailyRangeRatio",
                "OverFilledSize",
                "OverFilledRatio",
                "CrossSniperFillSizeAfterWhile",
                "CrossSniperAfterWhileSlippageInTicks",
                "AverageDuration",
                "MaxDuration",
                "FarTouchRatio",
                "NearTouchRatio",
                "BBORatio",
                "ExceedingFarTouchRatio",
                "FillBetterMidGT1Spread",
                "FillBetterMid1Spread",
                "FillAtMid",
                "FillWorseMid1Spread",
                "FillWorseMid2Spread",
                "FillWorseMidGT2Spread",
                "FillWorseMidGT2SpreadSlippageAsSpreadRatio",
                "AverageNumOfSniperOrders",
                "AverageNumOfFloatOrders",
                "AverageNumOfPegOrders",
                "PercentOfPartialFillOrders",
                "NumOfUnFullFilledOrders",
                "TimeLag",
                "PriceMove",
                "LowSpreadFTRatioDropFloatSize",
                "LowSpreadFTRatioDropFloatFillSize",
                "LowSpreadFTRatioDropFloatFillRate",
                "LowSpreadFTRatioBeforeDropFloatFillSize",
                "LowSpreadFTRatioNeverDropFloatSize",
                "LowSpreadFTRatioNeverDropFloatFillSize",
                "HighSpreadFTRatioDropFloatSize",
                "HighSpreadFTRatioDropFloatFillSize",
                "HighSpreadFTRatioDropFloatFillRate",
                "HighSpreadFTRatioBeforeDropFloatFillSize",
                "HighSpreadFTRatioNeverDropFloatSize",
                "HighSpreadFTRatioNeverDropFloatFillSize",
                "5LargestCalcPriceLatency",
                "NumOfOrderSubmitted",
                "5LargestOrderSubmitLatency",
                "AverageOrderSubmitLatency",
                "NumOfOrderCancelled",
                "5LargestOrderCancelLatency",
                "AverageOrderCancelLatency",
                "FloatFirstUnFullFilledSize",
                "FloatFirstUnFullFilledSlippageInTicks",
                "FloatFirstUnFullFilledSlippageAsSpreadRatio",
                "FloatFirstLowSpreadUnFilledSize",
                "FloatFirstLowSpreadUnFilledSlippage",
                "FloatFirstHighSpreadUnFilledSize",
                "FloatFirstHighSpreadUnFilledSlippage",
                "FloatFirstFullFilledSize",
                "FloatFirstFullFilledSlippageInTicks",
                "FloatFirstFullFilledSlippageAsSpreadRatio",
                "SniperFirstUnFullFilledSize",
                "SniperFirstUnFullFilledSlippageInTicks",
                "SniperFirstUnFullFilledSlippageAsSpreadRatio",
                "SniperFirstFullFilledSize",
                "SniperFirstFullFilledSlippageInTicks",
                "SniperFirstFullFilledSlippageAsSpreadRatio",
                "FloatFirstSize",
                "FloatFirstFillSize",
                "FloatFirstFillRate",
                "SniperFirstSize",
                "SniperFirstFillSize",
                "SniperFirstFillRate",
                "CrossSniperSize",
                "CrossSniperFillSize",
                "CrossSniperFillRate",
                "FTSizeExceedSniperSize",
                "FTSizeExceedSniperFillSize",
                "FTSizeExceedSniperFillRate",
                "SweepBelowFTSize",
                "SweepBelowFTFillSize",
                "SweepBelowFTFillRate",
                "SweepAtFTSize",
                "SweepAtFTFillSize",
                "SweepAtFTFillRate",
                "SweepAboveFTSize",
                "SweepAboveFTFillSize",
                "SweepAboveFTFillRate",
                "1SweepSize",
                "1SweepFillSize",
                "1SweepFillRate",
                "AllSniperSize",
                "AllSniperFillSize",
                "AllSniperFillRate",
                "1SniperSize",
                "1SniperFillSize",
                "1SniperFillRate",
                "SniperNewNTSize",
                "SniperNewNTFillSize",
                "SniperNewNTFillRate",
                "1SniperFillAsWholeSize",
                "1FloatFillAsWholeSize",
                "FillIn1S",
                "FillIn2S",
                "FillIn3S",
                "FillIn4S",
                "FillIn5S",
                "FillIn6S",
                "FillIn7S",
                "FillIn8S",
                "FillIn9S",
                "FillIn10S",
                "LowSpreadFloatSizeFT",
                "LowSpreadFloatFillSizeFT",
                "LowSpreadFloatFillRateFT",
                "HighSpreadFloatSizeFT",
                "HighSpreadFloatFillSizeFT",
                "HighSpreadFloatFillRateFT",
                "AllFloatSizeFT",
                "AllFloatFillSizeFT",
                "AllFloatFillRateFT",
                "LowSpreadFloatSizeByNum",
                "LowSpreadFloatFillSizeByNum",
                "LowSpreadFloatFillRateByNum",
                "HighSpreadFloatSizeByNum",
                "HighSpreadFloatFillSizeByNum",
                "HighSpreadFloatFillRateByNum",
                "AllFloatSizeByNum",
                "AllFloatFillSizeByNum",
                "AllFloatFillRateByNum",
                "LowSpreadFloatSizeByLots",
                "LowSpreadFloatFillSizeByLots",
                "LowSpreadFloatFillRateByLots",
                "HighSpreadFloatSizeByLots",
                "HighSpreadFloatFillSizeByLots",
                "HighSpreadFloatFillRateByLots",
                "AllFloatSizeByLots",
                "AllFloatFillSizeByLots",
                "AllFloatFillRateByLots",
                "1FloatSize",
                "1FloatFillSize",
                "1FloatFillRate",
                "2FloatSize",
                "2FloatFillSize",
                "2FloatFillRate",
                "3FloatSize",
                "3FloatFillSize",
                "3FloatFillRate",
                "4FloatSize",
                "4FloatFillSize",
                "4FloatFillRate",
                "5FloatSize",
                "5FloatFillSize",
                "5FloatFillRate",
                "0LowSentSize",
                "0LowFillSize",
                "0LowUnFillSize",
                "0LowUnFillSlippage",
                "0HighSentSize",
                "0HighFillSize",
                "0HighUnFillSize",
                "0HighUnFillSlippage",
                "1LowSentSize",
                "1LowFillSize",
                "1LowUnFillSize",
                "1LowUnFillSlippage",
                "1HighSentSize",
                "1HighFillSize",
                "1HighUnFillSize",
                "1HighUnFillSlippage",
                "2LowSentSize",
                "2LowFillSize",
                "2LowUnFillSize",
                "2LowUnFillSlippage",
                "2HighSentSize",
                "2HighFillSize",
                "2HighUnFillSize",
                "2HighUnFillSlippage",
                "3LowSentSize",
                "3LowFillSize",
                "3LowUnFillSize",
                "3LowUnFillSlippage",
                "3HighSentSize",
                "3HighFillSize",
                "3HighUnFillSize",
                "3HighUnFillSlippage",
                "4LowSentSize",
                "4LowFillSize",
                "4LowUnFillSize",
                "4LowUnFillSlippage",
                "4HighSentSize",
                "4HighFillSize",
                "4HighUnFillSize",
                "4HighUnFillSlippage",
                "5LowSentSize",
                "5LowFillSize",
                "5LowUnFillSize",
                "5LowUnFillSlippage",
                "5HighSentSize",
                "5HighFillSize",
                "5HighUnFillSize",
                "5HighUnFillSlippage",
                "6LowSentSize",
                "6LowFillSize",
                "6LowUnFillSize",
                "6LowUnFillSlippage",
                "6HighSentSize",
                "6HighFillSize",
                "6HighUnFillSize",
                "6HighUnFillSlippage",
                "7LowSentSize",
                "7LowFillSize",
                "7LowUnFillSize",
                "7LowUnFillSlippage",
                "7HighSentSize",
                "7HighFillSize",
                "7HighUnFillSize",
                "7HighUnFillSlippage",
                "8LowSentSize",
                "8LowFillSize",
                "8LowUnFillSize",
                "8LowUnFillSlippage",
                "8HighSentSize",
                "8HighFillSize",
                "8HighUnFillSize",
                "8HighUnFillSlippage",
                "9LowSentSize",
                "9LowFillSize",
                "9LowUnFillSize",
                "9LowUnFillSlippage",
                "9HighSentSize",
                "9HighFillSize",
                "9HighUnFillSize",
                "9HighUnFillSlippage",
                "LotsOfNormalCrossSniperSentAtFT",
                "LotsOfNormalCrossSniperFilledAtFT",
                "RatioOfNormalCrossSniperFilledAtFT",
                "LotsOfFTReductionCrossSniperSentAtFT",
                "LotsOfFTReductionCrossSniperFilledAtFT",
                "RatioOfFTReductionCrossSniperFilledAtFT",
                "LotsOfNormalCrossSniperSentAtFT+1",
                "LotsOfNormalCrossSniperFilledAtFT+1",
                "RatioOfNormalCrossSniperFilledAtFT+1",
                "LotsOfFTReductionCrossSniperSentAtFT+1",
                "LotsOfFTReductionCrossSniperFilledAtFT+1",
                "RatioOfFTReductionCrossSniperFilledAtFT+1",
                "LowStaticThreshold",
                "HighStaticThreshold",

            });

            maskfileColumnNames = new List<string>(new string[]{
                "FillOutFarTouch1Tick","FillOutFarTouch2Tick","FillOutFarTouchGT2Tick","FillOutFarTouchGT2TickSlippageInTicks","FillOutFarTouchGT2SpreadSlippageInTicks",
                "CrossSniperFillSizeAfterWhile","CrossSniperAfterWhileSlippageInTicks",
                "NumOfOrderSubmitted", "NumOfOrderCancelled",
                "FloatFirstUnFullFilledSize", "FloatFirstUnFullFilledSlippageInTicks", "FloatFirstFullFilledSize", "FloatFirstFullFilledSlippageInTicks",
                "SniperFirstUnFullFilledSize", "SniperFirstUnFullFilledSlippageInTicks", "SniperFirstFullFilledSize", "SniperFirstFullFilledSlippageInTicks",
                "CrossSniperSize", "CrossSniperFillSize",
                "SweepBelowFTSize", "SweepBelowFTFillSize",
                "SweepAtFTSize", "SweepAtFTFillSize",
                "SweepAboveFTSize", "SweepAboveFTFillSize",
                "1SweepSize", "1SweepFillSize",
                "FloatFirstSize", "FloatFirstFillSize",
                "SniperFirstSize", "SniperFirstFillSize",
                "0LowSentSize",
                "0LowFillSize",
                "0LowUnFillSize",
                "0LowUnFillSlippage",
                "0HighSentSize",
                "0HighFillSize",
                "0HighUnFillSize",
                "0HighUnFillSlippage",
                "1LowSentSize",
                "1LowFillSize",
                "1LowUnFillSize",
                "1LowUnFillSlippage",
                "1HighSentSize",
                "1HighFillSize",
                "1HighUnFillSize",
                "1HighUnFillSlippage",
                "2LowSentSize",
                "2LowFillSize",
                "2LowUnFillSize",
                "2LowUnFillSlippage",
                "2HighSentSize",
                "2HighFillSize",
                "2HighUnFillSize",
                "2HighUnFillSlippage",
                "3LowSentSize",
                "3LowFillSize",
                "3LowUnFillSize",
                "3LowUnFillSlippage",
                "3HighSentSize",
                "3HighFillSize",
                "3HighUnFillSize",
                "3HighUnFillSlippage",
                "4LowSentSize",
                "4LowFillSize",
                "4LowUnFillSize",
                "4LowUnFillSlippage",
                "4HighSentSize",
                "4HighFillSize",
                "4HighUnFillSize",
                "4HighUnFillSlippage",
                "5LowSentSize",
                "5LowFillSize",
                "5LowUnFillSize",
                "5LowUnFillSlippage",
                "5HighSentSize",
                "5HighFillSize",
                "5HighUnFillSize",
                "5HighUnFillSlippage",
                "6LowSentSize",
                "6LowFillSize",
                "6LowUnFillSize",
                "6LowUnFillSlippage",
                "6HighSentSize",
                "6HighFillSize",
                "6HighUnFillSize",
                "6HighUnFillSlippage",
                "7LowSentSize",
                "7LowFillSize",
                "7LowUnFillSize",
                "7LowUnFillSlippage",
                "7HighSentSize",
                "7HighFillSize",
                "7HighUnFillSize",
                "7HighUnFillSlippage",
                "8LowSentSize",
                "8LowFillSize",
                "8LowUnFillSize",
                "8LowUnFillSlippage",
                "8HighSentSize",
                "8HighFillSize",
                "8HighUnFillSize",
                "8HighUnFillSlippage",
                "9LowSentSize",
                "9LowFillSize",
                "9LowUnFillSize",
                "9LowUnFillSlippage",
                "9HighSentSize",
                "9HighFillSize",
                "9HighUnFillSize",
                "9HighUnFillSlippage",
                "LowStaticThreshold",
                "HighStaticThreshold"
            });
            #endregion
        }

        private void InitAlgoReport()
        {
            

            var db = DBManager.Default;
            db.RunQuery(string.Format(@"
;WITH cte AS
(
   SELECT *,
         ROW_NUMBER() OVER (PARTITION BY ProdName ORDER BY EndTime - StartTime DESC) AS rn
   FROM (
		SELECT ProdName, MachineName, StartTime, EndTime, EndTime - StartTime Duration FROM ConfigManager..AlgoSummary where Date BETWEEN '{0}' AND '{1}' 
	) a
)
SELECT *
FROM cte
WHERE rn = 1",
                StartDate, EndDate));
            var tb = db.ExecuteDataTable();

            foreach (DataRow row in tb.Rows)
            {
                var prodName = row["ProdName"].ToString();
                if (!AlgoReport.ContainsKey(prodName)) continue;
                var machine = row["MachineName"].ToString();
                //if (!SignalSummaryList[prodName].Keys.Contains(machine)) continue;
                var startTime = (DateTime)row["StartTime"];
                var duration = (DateTime)row["Duration"];
                var algoReport = AlgoReport[prodName];
                if (duration.TimeOfDay.TotalSeconds > algoReport.MaxExecutionDurationPerSignal)
                {
                    algoReport.MaxExecutionDurationPerSignal = duration.TimeOfDay.TotalSeconds;
                    algoReport.MaxExecutionDurationPerSignalTime = startTime.ToString();
                }
            }

            db = DBManager.Default;
            db.RunQuery(string.Format(@"
;WITH cte AS
(
   SELECT *,
         ROW_NUMBER() OVER (PARTITION BY Product ORDER BY LongestCalcPriceInterval DESC) AS rn
   FROM (
		SELECT Product, Machine, Date, LongestCalcPriceInterval FROM ConfigManager..MarketDataSummary where Date BETWEEN '{0}' AND '{1}'
	) a
)
SELECT *
FROM cte
WHERE rn = 1",
                StartDate, EndDate));
            tb = db.ExecuteDataTable();

            foreach (DataRow row in tb.Rows)
            {
                var prodName = row["Product"].ToString();
                if (!AlgoReport.ContainsKey(prodName)) continue;
                var machine = row["Machine"].ToString();
                //if (!SignalSummaryList[prodName].Keys.Contains(machine)) continue;
                var date = (DateTime)row["date"];
                var longestCalcPriceInterval = double.Parse(row["LongestCalcPriceInterval"].ToString());
                var algoReport = AlgoReport[prodName];
                if (longestCalcPriceInterval > algoReport.MaxExecutionDurationPerSignal)
                {
                    algoReport.LongestCalcPriceInterval = longestCalcPriceInterval;
                    algoReport.LongestCalcPriceIntervalTime = date.ToString();
                }
            }

            db = DBManager.Default;
            db.RunQuery(string.Format(@"
;WITH cte AS
(
   SELECT *,
         ROW_NUMBER() OVER (PARTITION BY Product ORDER BY LongestUseSimBBOInterval DESC) AS rn
   FROM (
		SELECT Product, Machine, Date, LongestUseSimBBOInterval FROM ConfigManager..MarketDataSummary where Date BETWEEN '{0}' AND '{1}'
	) a
)
SELECT *
FROM cte
WHERE rn = 1",
                StartDate, EndDate));
            tb = db.ExecuteDataTable();

            foreach (DataRow row in tb.Rows)
            {
                var prodName = row["Product"].ToString();
                if (!AlgoReport.ContainsKey(prodName)) continue;
                var machine = row["Machine"].ToString();
                // if (!SignalSummaryList[prodName].Keys.Contains(machine)) continue;
                var date = (DateTime)row["Date"];
                var longestUseSimBBOInterval = double.Parse(row["LongestUseSimBBOInterval"].ToString());
                var algoReport = AlgoReport[prodName];
                if (longestUseSimBBOInterval > algoReport.MaxExecutionDurationPerSignal)
                {
                    algoReport.LongestUseSimBBOInterval = longestUseSimBBOInterval;
                    algoReport.LongestUseSimBBOIntervalTime = date.ToString();
                }
            }
        }

        private void SummarizeMasterSingal()
        {
            foreach (var signalLine in signalFileLines)
            {
                if (signalLine == null) continue;
                string prodName = signalLine.Product;
                string algoName = signalLine.Algo;

                string[] fileLine = fileLines.Find(delegate (string[] items)
                {
                    return items[0].Equals(prodName) && items[1].Equals(algoName);
                });

                if (fileLine == null)
                {
                    fileLine = new string[fileColumnNames.Count];

                    for (int i = 0; i < fileLine.Length; ++i)
                    {
                        fileLine[i] = "0";
                    }

                    fileLine[0] = prodName;
                    fileLine[1] = algoName;

                    fileLines.Add(fileLine);
                }
                int numSignals = int.Parse(fileLine[2]);
                fileLine[2] = (numSignals + 1).ToString();

                var properties = TypeDescriptor.GetProperties(signalLine).OfType<PropertyDescriptor>().ToList();
                for (int i = 3; i < signalFileColumnNames.IndexOf("Contract"); ++i)
                {
                    if (i == signalFileColumnNames.IndexOf("MaxDuration"))
                    {
                        // maximization
                        double maxDuration = double.Parse(fileLine[i]);
                        double val = double.Parse(properties[i].GetValue(signalLine).ToString());
                        maxDuration = Math.Max(maxDuration, val);
                        fileLine[i] = maxDuration.ToString();
                    }
                    else
                    {
                        // summation
                        double sum = double.Parse(fileLine[i]);
                        double val = double.Parse(properties[i].GetValue(signalLine).ToString());
                        sum += val;
                        fileLine[i] = sum.ToString();
                    }
                }
            }
        }

        private void CalculateFinalResult()
        {
            foreach (string[] line in fileLines)
            {
                string product = line[fileColumnNames.IndexOf("Product")];
                string algo = line[fileColumnNames.IndexOf("Algo")];
                int numOfMasterOrders = int.Parse(line[fileColumnNames.IndexOf("NumOfMasterOrders")]);
                int totalSize = int.Parse(line[fileColumnNames.IndexOf("Size")]);
                for (int i = 0; i < line.Length; ++i)
                {
                    if (i == fileColumnNames.IndexOf("NewSlippageInTicks"))
                    {
                        // average slippage in ticks
                        line[i] = (double.Parse(line[i]) / totalSize).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("SlippageInTicks"))
                    {
                        // average slippage in ticks
                        line[i] = (double.Parse(line[i]) / totalSize).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("NTSlippageInTicks"))
                    {
                        // average slippage w.r.t NT price in ticks
                        line[i] = (double.Parse(line[i]) / totalSize).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("SlippageAsSpreadRatio"))
                    {
                        // slippage as spread
                        line[i] = (double.Parse(line[fileColumnNames.IndexOf("SlippageInTicks")]) * totalSize / double.Parse(line[i])).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("SpreadInTicks"))
                    {
                        // average spread in ticks
                        line[i] = (double.Parse(line[i]) / totalSize).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("HighSpreadInTicks"))
                    {
                        if (double.Parse(line[i - 1]) > 0)
                        {
                            line[i] = (double.Parse(line[i]) / double.Parse(line[i - 1])).ToString();
                        }
                    }

                    if (i == fileColumnNames.IndexOf("SlippageClosePriceInTicks"))
                    {
                        // average slippage w.r.t. Close price in ticks, for Default and TWAPMOC only
                        line[i] = (double.Parse(line[i]) / totalSize).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("SlippageInUSDPerTrade"))
                    {   // slippage in USD
                        line[i] = (double.Parse(line[i]) / totalSize).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("TotalSlippageInUSD"))
                    {
                        if (!prodSlippageDict.ContainsKey(product))
                        {
                            prodSlippageDict.Add(product, 0);
                        }
                        prodSlippageDict[product] += double.Parse(line[i]);
                    }

                    if (i == fileColumnNames.IndexOf("SlippageAsAUMRatio"))
                    {
                        line[i] = (double.Parse(line[fileColumnNames.IndexOf("TotalSlippageInUSD")]) / (double.Parse(line[i]) / totalSize)).ToString();
                    }

                    // calculate average of 5 largest slippages
                    if (i == fileColumnNames.IndexOf("5LargestSlippageInTicks"))
                    {
                        if (algo.Equals("Default"))
                        {
                            line[i] = "0";
                        }
                        else
                        {
                            if (product.Equals("KOSPI") && algo.Equals("MultiFloatSniperGold"))
                            {
                                int a = 1;
                            }
                            var prodAlgoLines = signalFileLines
                                .Where(signalLine => signalLine != null && signalLine.Product == product && signalLine.Algo == algo)
                                .OrderBy(item => item.FiveLargestSlippageInTicks)
                                .ToList();

                            double fiveLargestSize = 0;
                            double fiveLargestSlippage = 0;

                            for (int k = 0; k < Math.Min(5, prodAlgoLines.Count); ++k)
                            {
                                fiveLargestSize += prodAlgoLines[k].Size;
                                fiveLargestSlippage += prodAlgoLines[k].SlippageInTicks;
                            }
                            fiveLargestSlippage /= fiveLargestSize;
                            line[i] = fiveLargestSlippage.ToString();
                        }
                    }

                    if (i == fileColumnNames.IndexOf("TheoreticalOptimalSlippageInTicks"))
                    {
                        // TheoreticalOptimalSlippageInTicks
                        line[i] = (double.Parse(line[i]) / totalSize).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("DailyRangeInTicks"))
                    {
                        // TheoreticalOptimalSlippageInTicks
                        line[i] = (double.Parse(line[i]) / totalSize).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("SlippageAsDailyRangeRatio"))
                    {
                        // slippage as daily range
                        line[i] = (double.Parse(line[fileColumnNames.IndexOf("NTSlippageInTicks")]) * totalSize / double.Parse(line[i])).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("OverFilledRatio"))
                    {
                        line[i] = (double.Parse(line[i]) / totalSize).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("CrossSniperAfterWhileSlippageInTicks"))
                    {
                        // cross sniper slippage after 200ms 
                        int crossSniperFillSize = int.Parse(line[fileColumnNames.IndexOf("CrossSniperFillSizeAfterWhile")]);
                        if (crossSniperFillSize > 0)
                        {
                            line[i] = (double.Parse(line[i]) / crossSniperFillSize).ToString();
                        }
                    }

                    if (i == fileColumnNames.IndexOf("AverageDuration"))
                    {
                        line[i] = (double.Parse(line[fileColumnNames.IndexOf("AverageDuration")]) / totalSize).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("FarTouchRatio"))
                    {
                        // volume weighted far touch ratio 
                        line[i] = (double.Parse(line[i]) / numOfMasterOrders).ToString();
                    }
                    if (i == fileColumnNames.IndexOf("NearTouchRatio") || i == fileColumnNames.IndexOf("BBORatio"))
                    {
                        // NearTouchRatio and BBORatio
                        line[i] = (double.Parse(line[i]) / numOfMasterOrders).ToString();
                    }
                    if (i == fileColumnNames.IndexOf("ExceedingFarTouchRatio"))
                    {
                        line[i] = (double.Parse(line[i]) / totalSize).ToString();
                    }

                    if (i >= fileColumnNames.IndexOf("FillBetterMidGT1Spread") && i <= fileColumnNames.IndexOf("FillWorseMidGT2Spread"))
                    {
                        // FillFarTouch,FillNearTouch,FillOutFarTouch1Tick,FillOutFarTouch1Spread,FillOutFarTouch2Tick,FillOutFarTouch2Spread,FillOutFarTouchGT2Tick,FillOutFarTouchGT2Spread
                        line[i] = (double.Parse(line[i]) / (double)totalSize).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("FillWorseMidGT2SpreadSlippageAsSpreadRatio"))
                    {
                        // FillOutFarTouchGT2SlippageInTicks
                        if (double.Parse(line[fileColumnNames.IndexOf("FillWorseMidGT2Spread")]) * totalSize > 0.5)
                        {
                            line[i] = (double.Parse(line[i]) / (double.Parse(line[fileColumnNames.IndexOf("FillWorseMidGT2Spread")]) * totalSize)).ToString();
                        }
                    }

                    if (i == fileColumnNames.IndexOf("FillOutFarTouchGT2SpreadSlippageInTicks"))
                    {
                        // FillOutFarTouchGT2SlippageInTicks
                        if (double.Parse(line[fileColumnNames.IndexOf("FillOutFarTouchGT2Spread")]) * totalSize > 0.5)
                        {
                            line[i] = (double.Parse(line[i]) / (double.Parse(line[fileColumnNames.IndexOf("FillOutFarTouchGT2Spread")]) * totalSize)).ToString();
                        }
                    }

                    if (i == fileColumnNames.IndexOf("FillOutFarTouchGT2SpreadSlippageAsSpreadRatio"))
                    {
                        // FillOutFarTouchGT2SlippageAsSpreadRatio
                        if (double.Parse(line[fileColumnNames.IndexOf("FillOutFarTouchGT2Spread")]) * totalSize > 0.5)
                        {
                            line[i] = (double.Parse(line[fileColumnNames.IndexOf("FillOutFarTouchGT2SpreadSlippageInTicks")]) * double.Parse(line[fileColumnNames.IndexOf("FillOutFarTouchGT2Spread")]) * totalSize / double.Parse(line[i])).ToString();
                        }
                    }

                    if (i >= fileColumnNames.IndexOf("AverageNumOfSniperOrders") && i <= fileColumnNames.IndexOf("AverageNumOfPegOrders"))
                    {
                        // NumOfSniperOrders, NumOfFloatOrders
                        line[i] = (double.Parse(line[i]) / (double)numOfMasterOrders).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("PercentOfPartialFillOrders"))
                    {
                        if (double.Parse(line[i]) > 0)
                        {
                            line[i] = (double.Parse(line[i]) / ((double.Parse(line[fileColumnNames.IndexOf("AverageNumOfFloatOrders")]) + double.Parse(line[fileColumnNames.IndexOf("AverageNumOfPegOrders")])) * numOfMasterOrders)).ToString();
                        }
                        else
                        {
                            line[i] = "0";
                        }
                    }

                    if ((i == fileColumnNames.IndexOf("RatioOfNormalCrossSniperFilledAtFT"))
                        || (i == fileColumnNames.IndexOf("RatioOfFTReductionCrossSniperFilledAtFT"))
                        || (i == fileColumnNames.IndexOf("RatioOfNormalCrossSniperFilledAtFT+1"))
                        || (i == fileColumnNames.IndexOf("RatioOfFTReductionCrossSniperFilledAtFT+1")))
                    {
                        if (double.Parse(line[i]) > 0)
                        {
                            line[i] = (double.Parse(line[i - 1]) / double.Parse(line[i])).ToString();
                        }
                        else
                        {
                            line[i] = "0";
                        }
                    }

                    // "NumOfUnFullFilledOrders", "TimeLag", "PriceMove",
                    if (i >= fileColumnNames.IndexOf("TimeLag") && i <= fileColumnNames.IndexOf("PriceMove"))
                    {
                        double numOfUnFullFilledOrders = double.Parse(line[fileColumnNames.IndexOf("NumOfUnFullFilledOrders")]);
                        if (numOfUnFullFilledOrders > 0)
                        {
                            line[i] = (double.Parse(line[i]) / numOfUnFullFilledOrders).ToString();
                        }
                    }


                    if (i == fileColumnNames.IndexOf("LowSpreadFTRatioDropFloatFillRate"))
                    {
                        if (double.Parse(line[i - 2]) > 0)
                        {
                            line[i] = (double.Parse(line[i - 1]) / double.Parse(line[i - 2])).ToString();
                        }
                        else
                        {
                            line[i] = "0";
                        }
                    }

                    if (i == fileColumnNames.IndexOf("HighSpreadFTRatioDropFloatFillRate"))
                    {
                        if (double.Parse(line[i - 2]) > 0)
                        {
                            line[i] = (double.Parse(line[i - 1]) / double.Parse(line[i - 2])).ToString();
                        }
                        else
                        {
                            line[i] = "0";
                        }
                    }

                    // calculate average of 5 largest latency
                    if (i == fileColumnNames.IndexOf("5LargestCalcPriceLatency") || i == fileColumnNames.IndexOf("5LargestOrderSubmitLatency") || i == fileColumnNames.IndexOf("5LargestOrderCancelLatency"))
                    {
                        double fiveLargestSize = 0;
                        double fiveLargestLatency = 0;
                        var prodAlgoLines = signalFileLines.Where(signalLine => signalLine != null && signalLine.Product == product && signalLine.Algo == algo).ToList();
                        var latencyList = new List<double>();
                        if (product.Equals("KOSPI") && algo.Equals("MultiFloatSniperGold"))
                        {
                            int a = 1;
                        }

                        if (i == fileColumnNames.IndexOf("5LargestCalcPriceLatency"))
                        {
                            latencyList = prodAlgoLines.Select(item => item.FiveLargestCalcPriceLatency).ToList();
                        }
                        else if (i == fileColumnNames.IndexOf("5LargestOrderSubmitLatency"))
                        {
                            latencyList = prodAlgoLines.Select(item => item.FiveLargestOrderSubmitLatency).ToList();
                        }
                        else if (i == fileColumnNames.IndexOf("5LargestOrderCancelLatency"))
                        {
                            latencyList = prodAlgoLines.Select(item => item.FiveLargestOrderCancelLatency).ToList();
                        }

                        latencyList.Sort();
                        latencyList.Reverse();
                        for (int k = 0; k < Math.Min(5, latencyList.Count); ++k)
                        {
                            double latency = latencyList[k];
                            if (latency > 0)
                            {
                                fiveLargestSize++;
                                fiveLargestLatency += latency;
                            }
                        }
                        if (fiveLargestSize > 0)
                        {
                            fiveLargestLatency /= fiveLargestSize;
                            line[i] = fiveLargestLatency.ToString();
                        }
                        else
                        {
                            line[i] = "0";
                        }
                    }

                    if (i == fileColumnNames.IndexOf("AverageOrderSubmitLatency"))
                    {
                        if (double.Parse(line[fileColumnNames.IndexOf("NumOfOrderSubmitted")]) > 0)
                        {
                            line[i] = (double.Parse(line[i]) / double.Parse(line[fileColumnNames.IndexOf("NumOfOrderSubmitted")])).ToString();
                        }
                        else
                        {
                            line[i] = "0";
                        }
                    }

                    if (i == fileColumnNames.IndexOf("AverageOrderCancelLatency"))
                    {
                        if (double.Parse(line[fileColumnNames.IndexOf("NumOfOrderCancelled")]) > 0)
                        {
                            line[i] = (double.Parse(line[i]) / double.Parse(line[fileColumnNames.IndexOf("NumOfOrderCancelled")])).ToString();
                        }
                        else
                        {
                            line[i] = "0";
                        }
                    }


                    if (i == fileColumnNames.IndexOf("FloatFirstUnFullFilledSlippageInTicks") || i == fileColumnNames.IndexOf("FloatFirstFullFilledSlippageInTicks") ||
                        i == fileColumnNames.IndexOf("SniperFirstUnFullFilledSlippageInTicks") || i == fileColumnNames.IndexOf("SniperFirstFullFilledSlippageInTicks") ||
                        i == fileColumnNames.IndexOf("FloatFirstLowSpreadUnFilledSlippage") || i == fileColumnNames.IndexOf("FloatFirstHighSpreadUnFilledSlippage") ||
                        i == fileColumnNames.IndexOf("0LowUnFillSlippage") || i == fileColumnNames.IndexOf("0HighUnFillSlippage") ||
                        i == fileColumnNames.IndexOf("1LowUnFillSlippage") || i == fileColumnNames.IndexOf("1HighUnFillSlippage") ||
                        i == fileColumnNames.IndexOf("2LowUnFillSlippage") || i == fileColumnNames.IndexOf("2HighUnFillSlippage") ||
                        i == fileColumnNames.IndexOf("3LowUnFillSlippage") || i == fileColumnNames.IndexOf("3HighUnFillSlippage") ||
                        i == fileColumnNames.IndexOf("4LowUnFillSlippage") || i == fileColumnNames.IndexOf("4HighUnFillSlippage") ||
                        i == fileColumnNames.IndexOf("5LowUnFillSlippage") || i == fileColumnNames.IndexOf("5HighUnFillSlippage") ||
                        i == fileColumnNames.IndexOf("6LowUnFillSlippage") || i == fileColumnNames.IndexOf("6HighUnFillSlippage") ||
                        i == fileColumnNames.IndexOf("7LowUnFillSlippage") || i == fileColumnNames.IndexOf("7HighUnFillSlippage") ||
                        i == fileColumnNames.IndexOf("8LowUnFillSlippage") || i == fileColumnNames.IndexOf("8HighUnFillSlippage") ||
                        i == fileColumnNames.IndexOf("9LowUnFillSlippage") || i == fileColumnNames.IndexOf("9HighUnFillSlippage")
                        )
                    {
                        if (double.Parse(line[i - 1]) > 0)
                        {
                            line[i] = (double.Parse(line[i]) / double.Parse(line[i - 1])).ToString();
                        }
                    }

                    if (i == fileColumnNames.IndexOf("FloatFirstUnFullFilledSlippageAsSpreadRatio") || i == fileColumnNames.IndexOf("FloatFirstFullFilledSlippageAsSpreadRatio") ||
                       i == fileColumnNames.IndexOf("SniperFirstUnFullFilledSlippageAsSpreadRatio") || i == fileColumnNames.IndexOf("SniperFirstFullFilledSlippageAsSpreadRatio"))
                    {
                        if (double.Parse(line[i - 2]) > 0)
                        {
                            line[i] = ((double.Parse(line[i - 1]) * double.Parse(line[i - 2])) / double.Parse(line[i])).ToString();
                        }
                    }


                    if (i == fileColumnNames.IndexOf("FloatFirstFillRate") || i == fileColumnNames.IndexOf("SniperFirstFillRate"))
                    {
                        if (double.Parse(line[i - 2]) > 0)
                        {
                            line[i] = (double.Parse(line[i - 1]) / double.Parse(line[i - 2])).ToString();
                        }
                    }

                    if (i == fileColumnNames.IndexOf("CrossSniperFillRate"))
                    {
                        double size2 = double.Parse(line[i - 2]);
                        double size1 = double.Parse(line[i - 1]);
                        if (size2 > 0.5)
                        {
                            line[i] = (size1 / size2).ToString();
                        }
                    }

                    if (i == fileColumnNames.IndexOf("FTSizeExceedSniperFillRate"))
                    {
                        double size2 = double.Parse(line[i - 2]);
                        double size1 = double.Parse(line[i - 1]);
                        if (size2 > 0.5)
                        {
                            line[i] = (size1 / size2).ToString();
                        }
                    }

                    if (i == fileColumnNames.IndexOf("SweepBelowFTFillRate") ||
                        i == fileColumnNames.IndexOf("SweepAtFTFillRate") ||
                        i == fileColumnNames.IndexOf("SweepAboveFTFillRate") ||
                        i == fileColumnNames.IndexOf("1SweepFillRate"))
                    {
                        double size2 = double.Parse(line[i - 2]);
                        double size1 = double.Parse(line[i - 1]);
                        if (size2 > 0.5)
                        {
                            line[i] = (size1 / size2).ToString();
                        }
                    }




                    if (i >= fileColumnNames.IndexOf("AllSniperSize") && i <= fileColumnNames.IndexOf("SniperNewNTFillRate") && (i - fileColumnNames.IndexOf("AllSniperSize") + 1) % 3 == 0)
                    {
                        //from column AllSniperSize to column 1SniperFillRate
                        double size2 = double.Parse(line[i - 2]);
                        double size1 = double.Parse(line[i - 1]);
                        if (size2 > 0.5)
                        {
                            line[i] = (size1 / size2).ToString();
                        }
                    }

                    if (i == fileColumnNames.IndexOf("1SniperFillAsWholeSize"))
                    {
                        line[i] = (double.Parse(line[i]) / totalSize).ToString();
                    }

                    if (i == fileColumnNames.IndexOf("1FloatFillAsWholeSize"))
                    {
                        line[i] = (double.Parse(line[i]) / totalSize).ToString();
                    }

                    if (i >= fileColumnNames.IndexOf("FillIn1S") && i <= fileColumnNames.IndexOf("FillIn10S"))
                    {
                        line[i] = (double.Parse(line[i]) / totalSize).ToString();
                    }

                    if (i >= fileColumnNames.IndexOf("LowSpreadFloatSizeFT") && i <= fileColumnNames.IndexOf("5FloatFillRate") && (i - fileColumnNames.IndexOf("LowSpreadFloatSizeFT") + 1) % 3 == 0)
                    {
                        //from column LowSpreadFloatSize to the end
                        double size2 = double.Parse(line[i - 2]);
                        double size1 = double.Parse(line[i - 1]);
                        if (size2 > 0.5)
                        {
                            line[i] = (size1 / size2).ToString();
                        }
                    }
                }

                List<double> LowSentSize = new List<double>();
                List<double> LowFillSize = new List<double>();
                List<double> LowUnFillSize = new List<double>();
                List<double> LowUnFillSlippage = new List<double>();
                List<double> HighSentSize = new List<double>();
                List<double> HighFillSize = new List<double>();
                List<double> HighUnFillSize = new List<double>();
                List<double> HighUnFillSlippage = new List<double>();
                int startIdx = fileColumnNames.IndexOf("0LowSentSize");
                for (int j = 0; j < 10; j++)
                {
                    LowSentSize.Add(double.Parse(line[startIdx]));
                    startIdx++;
                    LowFillSize.Add(double.Parse(line[startIdx]));
                    startIdx++;
                    LowUnFillSize.Add(double.Parse(line[startIdx]));
                    startIdx++;
                    LowUnFillSlippage.Add(double.Parse(line[startIdx]));
                    startIdx++;
                    HighSentSize.Add(double.Parse(line[startIdx]));
                    startIdx++;
                    HighFillSize.Add(double.Parse(line[startIdx]));
                    startIdx++;
                    HighUnFillSize.Add(double.Parse(line[startIdx]));
                    startIdx++;
                    HighUnFillSlippage.Add(double.Parse(line[startIdx]));
                    startIdx++;
                }

                double slipLow = double.Parse(line[fileColumnNames.IndexOf("FloatFirstLowSpreadUnFilledSlippage")]);
                double slipHigh = double.Parse(line[fileColumnNames.IndexOf("FloatFirstHighSpreadUnFilledSlippage")]);
                double spreadHigh = double.Parse(line[fileColumnNames.IndexOf("HighSpreadInTicks")]);

                double maxPnL = -0.5;
                int maxJ = 10;

                for (int j1 = 0; j1 <= 9; j1++)
                {
                    double tmpPnL = 0;
                    double tmpSize = 0;
                    for (int j2 = 0; j2 < j1; j2++)
                    {
                        tmpPnL = tmpPnL + (-0.5) * LowSentSize[j2];
                        tmpSize = tmpSize + LowSentSize[j2];
                    }
                    for (int j2 = j1; j2 <= 9; j2++)
                    {
                        tmpPnL = tmpPnL + LowFillSize[j2] * 0.5 + (LowSentSize[j2] - LowFillSize[j2]) * LowUnFillSlippage[j2];
                        tmpSize = tmpSize + LowSentSize[j2];
                    }
                    if (tmpSize > 0)
                    {
                        if (tmpPnL / tmpSize > maxPnL)
                        {
                            maxPnL = tmpPnL / tmpSize;
                            maxJ = j1;
                        }
                    }
                }
                double lowstaticthreshold = maxJ * 0.1;
                line[fileColumnNames.IndexOf("LowStaticThreshold")] = lowstaticthreshold.ToString();

                maxPnL = -spreadHigh * 0.5;
                maxJ = 10;
                for (int j1 = 0; j1 <= 9; j1++)
                {
                    double tmpPnL = 0;
                    double tmpSize = 0;
                    for (int j2 = 0; j2 < j1; j2++)
                    {
                        tmpPnL = tmpPnL + (-spreadHigh * 0.5) * HighSentSize[j2];
                        tmpSize = tmpSize + HighSentSize[j2];
                    }
                    for (int j2 = j1; j2 <= 9; j2++)
                    {
                        tmpPnL = tmpPnL + HighFillSize[j2] * (0.5 * spreadHigh - 1) + (HighSentSize[j2] - HighFillSize[j2]) * HighUnFillSlippage[j2];
                        tmpSize = tmpSize + HighSentSize[j2];
                    }
                    if (tmpSize > 0)
                    {
                        if (tmpPnL / tmpSize > maxPnL)
                        {
                            maxPnL = tmpPnL / tmpSize;
                            maxJ = j1;
                        }
                    }
                }
                double highstaticthreshold = maxJ * 0.1;
                line[fileColumnNames.IndexOf("HighStaticThreshold")] = highstaticthreshold.ToString();
            }
        }

        private void WriteReportFiles()
        {

            StreamWriter fileWriter = File.CreateText(outputPath + "\\FillReportSignals_" + fileNamePostfix + ".csv");

            for (int i = 0; i < signalFileColumnNames.Count - 1; ++i)
            {
                fileWriter.Write(signalFileColumnNames[i] + ",");
            }
            if (signalFileColumnNames.Count > 0)
            {
                fileWriter.WriteLine(signalFileColumnNames[signalFileColumnNames.Count - 1]);
            }

            if (signalFileLines.Count > 0)
            {
                var list = new List<string>();
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(signalFileLines[0]))
                {
                    list.Add(descriptor.Name);
                }
                fileWriter.Write(string.Join(",", list));
                fileWriter.Write(Environment.NewLine);

                foreach (var line in signalFileLines)
                {
                    list = new List<string>();
                    foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(line))
                    {
                        object value = descriptor.GetValue(line);
                        if (value is DateTime) list.Add(((DateTime)value).ToString("yyyyMMdd"));
                        else list.Add(value.ToString());
                    }
                    fileWriter.Write(string.Join(",", list));
                    fileWriter.Write(Environment.NewLine);
                }
            }

            fileWriter.Close();


            // write total file
            fileWriter = File.CreateText(outputPath + "\\FillReportTotal_" + fileNamePostfix + ".csv");

            for (int i = 0; i < fileColumnNames.Count - 1; ++i)
            {
                if (!maskfileColumnNames.Contains(fileColumnNames[i]))
                {
                    fileWriter.Write(fileColumnNames[i] + ",");
                }
            }
            if (fileColumnNames.Count > 0)
            {
                if (!maskfileColumnNames.Contains(fileColumnNames[fileColumnNames.Count - 1]))
                {
                    fileWriter.WriteLine(fileColumnNames[fileColumnNames.Count - 1]);
                }
                else
                {
                    fileWriter.WriteLine();
                }
            }

            foreach (string[] line in fileLines)
            {
                for (int i = 0; i < line.Length - 1; ++i)
                {
                    if (!maskfileColumnNames.Contains(fileColumnNames[i]))
                    {
                        fileWriter.Write(line[i] + ",");
                    }
                }
                if (line.Length > 0)
                {
                    if (!maskfileColumnNames.Contains(fileColumnNames[line.Length - 1]))
                    {
                        fileWriter.WriteLine(line[line.Length - 1]);
                    }
                    else
                    {
                        fileWriter.WriteLine();
                    }
                }
            }

            fileWriter.Close();

            DateTime dateS = new DateTime(int.Parse(EndDate.Substring(0, 4)), int.Parse(EndDate.Substring(4, 2)), int.Parse(EndDate.Substring(6, 2)));
            dateS = dateS.AddYears(-1);

            try
            {
                string sql = "select * from PostTradeAnalysis where Date < '" + EndDate + "' and Date >= '" + dateS.ToString("yyyyMMdd") + "';";
                var tb = HelperFunction.ExecuteQuery(sql);
                foreach (DataRow row in tb.Rows)
                {
                    string product = row["Product"].ToString().ToUpper();
                    if (!AlgoReport.ContainsKey(product)) continue;
                    var algoReport = AlgoReport[product];
                    double tmp = double.Parse(row["MaxNumOfOrderPerSecond"].ToString());
                    algoReport.ListMaxNumOfOrderPerSecond.Add(tmp);
                    tmp = double.Parse(row["AvgNumOfOrderPerSecond"].ToString());
                    algoReport.ListAvgNumOfOrderPerSecond.Add(tmp);
                    tmp = double.Parse(row["MaxVolPerSecond"].ToString());
                    algoReport.ListMaxVolPerSecond.Add(tmp);
                    tmp = double.Parse(row["AvgVolPerSecond"].ToString());
                    algoReport.ListAvgVolPerSecond.Add(tmp);
                    tmp = double.Parse(row["Avg5MinParticipationRate"].ToString());
                    algoReport.ListAvg5MinParticipationRate.Add(tmp);
                    tmp = double.Parse(row["Max5MinParticipationRate"].ToString());
                    algoReport.ListMax5MinParticipationRate.Add(tmp);
                    tmp = double.Parse(row["NumOfOrderPerDay"].ToString());
                    algoReport.ListNumOfOrderPerDay.Add(tmp);
                    tmp = double.Parse(row["MaxNumOfOrderPerSignal"].ToString());
                    algoReport.ListMaxNumOfOrderPerSignal.Add(tmp);
                    tmp = double.Parse(row["AvgNumOfOrderPerSignal"].ToString());
                    algoReport.ListAvgNumOfOrderPerSignal.Add(tmp);
                    tmp = double.Parse(row["MaxOrderSizePerOrder"].ToString());
                    algoReport.ListMaxOrderSizePerOrder.Add(tmp);
                    tmp = double.Parse(row["AvgOrderSizePerOrder"].ToString());
                    algoReport.ListAvgOrderSizePerOrder.Add(tmp);
                    tmp = double.Parse(row["MaxSizePerSignal"].ToString());
                    algoReport.ListMaxSizePerSignal.Add(tmp);
                    tmp = double.Parse(row["AvgSizePerSignal"].ToString());
                    algoReport.ListAvgSizePerSignal.Add(tmp);
                    tmp = double.Parse(row["NumOfOrderWithAbnormalFillPrice"].ToString());
                    algoReport.ListNumOfOrderWithAbnormalFillPrice.Add(tmp);
                    tmp = double.Parse(row["NumOfBuyOrders"].ToString());
                    algoReport.ListNumOfBuyOrders.Add(tmp);
                    tmp = double.Parse(row["NumOfSellOrders"].ToString());
                    algoReport.ListNumOfSellOrders.Add(tmp);
                    tmp = double.Parse(row["NumOfOrderDirectionReversalInOneMinute"].ToString());
                    algoReport.ListNumOfOrderDirectionReversalInOneMinute.Add(tmp);
                    tmp = double.Parse(row["NumOfRejectOrders"].ToString());
                    algoReport.ListNumOfRejectOrders.Add(tmp);
                    tmp = double.Parse(row["NumOfOrdersExceedingFatFingerLimit"].ToString());
                    algoReport.ListNumOfOrdersExceedingFatFingerLimit.Add(tmp);
                }
            }
            catch (Exception ex)
            {
                OnDBConnectionFailed(ex);
            }

            WriteSlippageToAbsPnlReport();

            Dictionary<string, double> orderCounts_NumOfOrderMatchInFiveMinute = new Dictionary<string, double>();
            Dictionary<string, double> orderCounts_NumOfReversalInOneMinute = new Dictionary<string, double>();
            Dictionary<string, double> orderCounts_NumOfOrderRejected = new Dictionary<string, double>();
            Dictionary<string, double> orderCounts_NumOfOrderRounddown = new Dictionary<string, double>();
            fileWriter = File.CreateText(outputPath + "\\PostTradeAnalysisReport_" + fileNamePostfix + ".csv");
            StreamWriter fileWriter2 = File.CreateText(outputPath + "\\PostTradeAnalysisReportCompareOneYear_" + fileNamePostfix + ".csv");

            fileWriter2.Write("Product,");
            fileWriter2.Write("MaxNumOfOrderPerSecond,Percentile,");
            fileWriter2.Write("AvgNumOfOrderPerSecond,Percentile,");
            fileWriter2.Write("MaxVolPerSecond,Percentile,");
            fileWriter2.Write("AvgVolPerSecond,Percentile,");
            fileWriter2.Write("Avg5MinParticipationRate,Percentile,");
            fileWriter2.Write("Max5MinParticipationRate,Percentile,");
            fileWriter2.Write("NumOfOrderPerDay,Percentile,");
            fileWriter2.Write("MaxNumOfOrderPerSignal,Percentile,");
            fileWriter2.Write("AvgNumOfOrderPerSignal,Percentile,");
            fileWriter2.Write("MaxOrderSizePerOrder,Percentile,");
            fileWriter2.Write("AvgOrderSizePerOrder,Percentile,");
            fileWriter2.Write("MaxSizePerSignal,Percentile,");
            fileWriter2.Write("AvgSizePerSignal,Percentile,");
            fileWriter2.Write("NumOfOrderWithAbnormalFillPrice,Percentile,");
            fileWriter2.Write("NumOfBuyOrders,Percentile,");
            fileWriter2.Write("NumOfSellOrders,Percentile,");
            fileWriter2.Write("NumOfOrderDirectionReversalInOneMinute,Percentile,");
            fileWriter2.Write("NumOfRejectOrders,Percentile,");
            fileWriter2.WriteLine("NumOfOrdersExceedingFatFingerLimit,Percentile");

            foreach (string prod in AlgoReport.Keys)
            {
                var algoReport = AlgoReport[prod];
                fileWriter.WriteLine("TestDescription," + algoReport.TestDescription);
                fileWriter.WriteLine("Product," + prod);
                fileWriter2.Write(prod + ",");
                fileWriter.Write("Strategies,");
                foreach (string s in algoReport.Strategies.Keys)
                {
                    fileWriter.Write(s + "(" + algoReport.Strategies[s] + ") ");
                }
                fileWriter.WriteLine();
                fileWriter.WriteLine("Period," + StartDate + "-" + EndDate);
                fileWriter.WriteLine("AUM," + algoReport.AUM);
                fileWriter.WriteLine("Maximum Number of orders per second when we have signals in that period," + algoReport.MaxNumOrderPerSecond);
                algoReport.ListMaxNumOfOrderPerSecond.Sort();
                double count = 0;
                if (algoReport.ListMaxNumOfOrderPerSecond.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListMaxNumOfOrderPerSecond.Count; i++)
                    {
                        if (algoReport.MaxNumOrderPerSecond < algoReport.ListMaxNumOfOrderPerSecond[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListMaxNumOfOrderPerSecond.Count;
                }
                fileWriter2.Write(algoReport.MaxNumOrderPerSecond + "," + count + ",");

                fileWriter.WriteLine("Average Number of orders per second when we have signals in that period," + algoReport.AvgNumOrderPerSecond);
                algoReport.ListAvgNumOfOrderPerSecond.Sort();
                count = 0;
                if (algoReport.ListAvgNumOfOrderPerSecond.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListAvgNumOfOrderPerSecond.Count; i++)
                    {
                        if (algoReport.AvgNumOrderPerSecond < algoReport.ListAvgNumOfOrderPerSecond[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListAvgNumOfOrderPerSecond.Count;
                }
                fileWriter2.Write(algoReport.AvgNumOrderPerSecond + "," + count + ",");

                fileWriter.WriteLine("Maximum Number of volume sent per second when we have signals in that period," + algoReport.MaxVolumeSentPerSecond);
                algoReport.ListMaxVolPerSecond.Sort();
                count = 0;
                if (algoReport.ListMaxVolPerSecond.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListMaxVolPerSecond.Count; i++)
                    {
                        if (algoReport.MaxVolumeSentPerSecond < algoReport.ListMaxVolPerSecond[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListMaxVolPerSecond.Count;
                }
                fileWriter2.Write(algoReport.MaxVolumeSentPerSecond + "," + count + ",");

                fileWriter.WriteLine("Average Number of volume sent per second when we have signals in that period," + algoReport.AvgVolumeSentPerSecond);
                algoReport.ListAvgVolPerSecond.Sort();
                count = 0;
                if (algoReport.ListAvgVolPerSecond.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListAvgVolPerSecond.Count; i++)
                    {
                        if (algoReport.AvgVolumeSentPerSecond < algoReport.ListAvgVolPerSecond[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListAvgVolPerSecond.Count;
                }
                fileWriter2.Write(algoReport.AvgVolumeSentPerSecond + "," + count + ",");

                fileWriter.WriteLine("5min Average Market participation ratio after we executed the signal," + algoReport.AvgMarketParticipationRate);
                algoReport.ListAvg5MinParticipationRate.Sort();
                count = 0;
                if (algoReport.ListAvg5MinParticipationRate.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListAvg5MinParticipationRate.Count; i++)
                    {
                        if (algoReport.AvgMarketParticipationRate < algoReport.ListAvg5MinParticipationRate[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListAvg5MinParticipationRate.Count;
                }
                fileWriter2.Write(algoReport.AvgMarketParticipationRate + "," + count + ",");

                fileWriter.WriteLine("5min Maximum Market participation ratio after we executed the signal," + algoReport.MaxMarketParticipationRate + "," + algoReport.MaxParticipationTime);
                algoReport.ListMax5MinParticipationRate.Sort();
                count = 0;
                if (algoReport.ListMax5MinParticipationRate.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListMax5MinParticipationRate.Count; i++)
                    {
                        if (algoReport.MaxMarketParticipationRate < algoReport.ListMax5MinParticipationRate[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListMax5MinParticipationRate.Count;
                }
                fileWriter2.Write(algoReport.MaxMarketParticipationRate + "," + count + ",");

                fileWriter.WriteLine("Maximum Number of orders per day (calculated only when we have signals in that day)," + algoReport.MaxNumOrderPerDay);
                algoReport.ListNumOfOrderPerDay.Sort();
                count = 0;
                if (algoReport.ListNumOfOrderPerDay.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListNumOfOrderPerDay.Count; i++)
                    {
                        if (algoReport.MaxNumOrderPerDay < algoReport.ListNumOfOrderPerDay[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListNumOfOrderPerDay.Count;
                }
                fileWriter2.Write(algoReport.MaxNumOrderPerDay + "," + count + ",");

                fileWriter.WriteLine("Average Number of orders per day (calculated only when we have signals in that day)," + algoReport.AvgNumOrderPerDay);
                fileWriter.WriteLine("Maximum Number of orders per master signals," + algoReport.MaxNumOrderPerSignal);
                fileWriter.WriteLine("Maximum Number of orders cancelled per master signal," + algoReport.MaxNumOrderCancelledPerSignal);

                algoReport.ListMaxNumOfOrderPerSignal.Sort();
                count = 0;
                if (algoReport.ListMaxNumOfOrderPerSignal.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListMaxNumOfOrderPerSignal.Count; i++)
                    {
                        if (algoReport.MaxNumOrderPerSignal < algoReport.ListMaxNumOfOrderPerSignal[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListMaxNumOfOrderPerSignal.Count;
                }
                fileWriter2.Write(algoReport.MaxNumOrderPerSignal + "," + count + ",");

                fileWriter.WriteLine("Average Number of orders per master signals," + algoReport.AvgNumOrderPerSignal);
                algoReport.ListAvgNumOfOrderPerSignal.Sort();
                count = 0;
                if (algoReport.ListAvgNumOfOrderPerSignal.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListAvgNumOfOrderPerSignal.Count; i++)
                    {
                        if (algoReport.AvgNumOrderPerSignal < algoReport.ListAvgNumOfOrderPerSignal[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListAvgNumOfOrderPerSignal.Count;
                }
                fileWriter2.Write(algoReport.AvgNumOrderPerSignal + "," + count + ",");

                fileWriter.WriteLine("Maximum order size per order," + algoReport.MaxOrderSizePerOrder);
                algoReport.ListMaxOrderSizePerOrder.Sort();
                count = 0;
                if (algoReport.ListMaxOrderSizePerOrder.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListMaxOrderSizePerOrder.Count; i++)
                    {
                        if (algoReport.MaxOrderSizePerOrder < algoReport.ListMaxOrderSizePerOrder[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListMaxOrderSizePerOrder.Count;
                }
                fileWriter2.Write(algoReport.MaxOrderSizePerOrder + "," + count + ",");

                fileWriter.WriteLine("Average order size per order," + algoReport.AvgOrderSizePerOrder);
                algoReport.ListAvgOrderSizePerOrder.Sort();
                count = 0;
                if (algoReport.ListAvgOrderSizePerOrder.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListAvgOrderSizePerOrder.Count; i++)
                    {
                        if (algoReport.AvgOrderSizePerOrder < algoReport.ListAvgOrderSizePerOrder[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListAvgOrderSizePerOrder.Count;
                }
                fileWriter2.Write(algoReport.AvgOrderSizePerOrder + "," + count + ",");

                fileWriter.WriteLine("Maximum slippage In Ticks and Spread between arrival price and execution price," + algoReport.MaxSlippageInTicks + "," + algoReport.MaxSlippageInSpread + "," + algoReport.MaxSlippageDay);
                fileWriter.WriteLine("Minimum slippage In Ticks and Spread between arrival price and execution price," + algoReport.MinSlippageInTicks + "," + algoReport.MinSlippageInSpread + "," + algoReport.MinSlippageDay);
                fileWriter.WriteLine("Average slippage between arrival price and execution price," + algoReport.AvgSlippage);
                fileWriter.WriteLine("Maximum size of the master signal," + algoReport.MaxSizePerSignal);
                algoReport.ListMaxSizePerSignal.Sort();
                count = 0;
                if (algoReport.ListMaxSizePerSignal.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListMaxSizePerSignal.Count; i++)
                    {
                        if (algoReport.MaxSizePerSignal < algoReport.ListMaxSizePerSignal[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListMaxSizePerSignal.Count;
                }
                fileWriter2.Write(algoReport.MaxSizePerSignal + "," + count + ",");


                fileWriter.WriteLine("Average size of the master signal," + algoReport.AvgSizePerSignal);
                algoReport.ListAvgSizePerSignal.Sort();
                count = 0;
                if (algoReport.ListAvgSizePerSignal.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListAvgSizePerSignal.Count; i++)
                    {
                        if (algoReport.AvgSizePerSignal < algoReport.ListAvgSizePerSignal[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListAvgSizePerSignal.Count;
                }
                fileWriter2.Write(algoReport.AvgSizePerSignal + "," + count + ",");

                fileWriter.WriteLine("Number of orders with abnormal fill price," + algoReport.NumOrderWithAbnormalFillPrice);
                algoReport.ListNumOfOrderWithAbnormalFillPrice.Sort();
                count = 0;
                if (algoReport.ListNumOfOrderWithAbnormalFillPrice.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListNumOfOrderWithAbnormalFillPrice.Count; i++)
                    {
                        if (algoReport.NumOrderWithAbnormalFillPrice < algoReport.ListNumOfOrderWithAbnormalFillPrice[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListNumOfOrderWithAbnormalFillPrice.Count;
                }
                fileWriter2.Write(algoReport.NumOrderWithAbnormalFillPrice + "," + count + ",");

                fileWriter.WriteLine("Maximum Total number of buy orders per day," + algoReport.MaxNumBuyOrderPerDay);
                algoReport.ListNumOfBuyOrders.Sort();
                count = 0;
                if (algoReport.ListNumOfBuyOrders.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListNumOfBuyOrders.Count; i++)
                    {
                        if (algoReport.MaxNumBuyOrderPerDay < algoReport.ListNumOfBuyOrders[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListNumOfBuyOrders.Count;
                }
                fileWriter2.Write(algoReport.MaxNumBuyOrderPerDay + "," + count + ",");

                fileWriter.WriteLine("Maximum Total number of sell orders per day," + algoReport.MaxNumSellOrderPerDay);
                algoReport.ListNumOfSellOrders.Sort();
                count = 0;
                if (algoReport.ListNumOfSellOrders.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListNumOfSellOrders.Count; i++)
                    {
                        if (algoReport.MaxNumSellOrderPerDay < algoReport.ListNumOfSellOrders[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListNumOfSellOrders.Count;
                }
                fileWriter2.Write(algoReport.MaxNumSellOrderPerDay + "," + count + ",");

                fileWriter.WriteLine("Maximum Number of order direction reversal in one minute," + algoReport.MaxNumberOfReversalInOneMinute + "," + algoReport.MaxNumberOfReversalTime);
                fileWriter.WriteLine("Maximum Number of orders matched in five minutes," + algoReport.MaxNumberOfOrdersMatchedInFiveMinutes + "," + algoReport.MaxNumberOfOrdersMatchedTime);
                fileWriter.WriteLine("PnL of the orders matched in five minutes," + algoReport.PnLOrdersMatchedInFiveMinutes);
                if (!orderCounts_NumOfOrderMatchInFiveMinute.ContainsKey(prod))
                {
                    orderCounts_NumOfOrderMatchInFiveMinute.Add(prod, -algoReport.MaxNumberOfOrdersMatchedInFiveMinutes);
                }
                else
                {
                    orderCounts_NumOfOrderMatchInFiveMinute[prod] = -algoReport.MaxNumberOfOrdersMatchedInFiveMinutes;
                }
                if (!orderCounts_NumOfReversalInOneMinute.ContainsKey(prod))
                {
                    orderCounts_NumOfReversalInOneMinute.Add(prod, -algoReport.MaxNumberOfReversalInOneMinute);
                }
                else
                {
                    orderCounts_NumOfReversalInOneMinute[prod] = -algoReport.MaxNumberOfReversalInOneMinute;
                }

                algoReport.ListNumOfOrderDirectionReversalInOneMinute.Sort();
                count = 0;
                if (algoReport.ListNumOfOrderDirectionReversalInOneMinute.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListNumOfOrderDirectionReversalInOneMinute.Count; i++)
                    {
                        if (algoReport.MaxNumberOfReversalInOneMinute < algoReport.ListNumOfOrderDirectionReversalInOneMinute[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListNumOfOrderDirectionReversalInOneMinute.Count;
                }
                fileWriter2.Write(algoReport.MaxNumberOfReversalInOneMinute + "," + count + ",");

                fileWriter.WriteLine("Maximum Execution Duration per Master Signal," + algoReport.MaxExecutionDurationPerSignal + "," + algoReport.MaxExecutionDurationPerSignalTime);

                fileWriter.WriteLine("Longest Calc Price Interval(ms)," + algoReport.LongestCalcPriceInterval + "," + algoReport.LongestCalcPriceIntervalTime);

                fileWriter.WriteLine("Longest Use Sim BBO Interval(ms)," + algoReport.LongestUseSimBBOInterval + "," + algoReport.LongestUseSimBBOIntervalTime);

                fileWriter.WriteLine("Number of orders rejected," + algoReport.NumOrderRejected);
                algoReport.ListNumOfRejectOrders.Sort();
                count = 0;
                if (algoReport.ListNumOfRejectOrders.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListNumOfRejectOrders.Count; i++)
                    {
                        if (algoReport.NumOrderRejected < algoReport.ListNumOfRejectOrders[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListNumOfRejectOrders.Count;
                }
                fileWriter2.Write(algoReport.NumOrderRejected + "," + count + ",");

                if (!orderCounts_NumOfOrderRejected.ContainsKey(prod))
                {
                    orderCounts_NumOfOrderRejected.Add(prod, -algoReport.NumOrderRejected);
                }
                else
                {
                    orderCounts_NumOfOrderRejected[prod] = -algoReport.NumOrderRejected;
                }

                fileWriter.WriteLine("Number of orders whose size are rounded down due to exceeding fat finger limit," + algoReport.NumOrderExceedingFatFingerLimit);
                algoReport.ListNumOfOrdersExceedingFatFingerLimit.Sort();
                count = 0;
                if (algoReport.ListNumOfOrdersExceedingFatFingerLimit.Count > 0)
                {
                    for (int i = 0; i < algoReport.ListNumOfOrdersExceedingFatFingerLimit.Count; i++)
                    {
                        if (algoReport.NumOrderExceedingFatFingerLimit < algoReport.ListNumOfOrdersExceedingFatFingerLimit[i])
                        {
                            count = i;
                            break;
                        }
                    }
                    count = count / algoReport.ListNumOfOrdersExceedingFatFingerLimit.Count;
                }
                fileWriter2.WriteLine(algoReport.NumOrderExceedingFatFingerLimit + "," + count);

                if (!orderCounts_NumOfOrderRounddown.ContainsKey(prod))
                {
                    orderCounts_NumOfOrderRounddown.Add(prod, -algoReport.NumOrderExceedingFatFingerLimit);
                }
                else
                {
                    orderCounts_NumOfOrderRounddown[prod] = -algoReport.NumOrderExceedingFatFingerLimit;
                }

                fileWriter.WriteLine("Was the production running without crashing," + algoReport.IsNotCrashing);
                fileWriter.WriteLine();
            }

            fileWriter.Close();
            fileWriter2.Close();

            if (StartDate.Equals(EndDate))
            {
                StreamWriter fileWriter3 = File.CreateText(outputPath + "\\ExceptionalOrderStatistics_" + StartDate + ".csv");
                fileWriter3.WriteLine("Product,NumOfOrdersMatchedInFiveMinutes,AvgPnLInTicks");
                int count = 0;
                foreach (KeyValuePair<string, double> item in orderCounts_NumOfOrderMatchInFiveMinute.OrderBy(key => key.Value))
                {
                    if (Math.Abs(item.Value) > 0.1)
                    {
                        if (count < 5)
                        {
                            fileWriter3.WriteLine(item.Key + "," + Math.Abs(item.Value) + "," + AlgoReport[item.Key].PnLOrdersMatchedInFiveMinutes);
                            count = count + 1;
                        }
                    }
                }

                fileWriter3.WriteLine();
                count = 0;
                fileWriter3.WriteLine("Product,NumOfOrderDirectionalReversalInOneMinute");
                foreach (KeyValuePair<string, double> item in orderCounts_NumOfReversalInOneMinute.OrderBy(key => key.Value))
                {
                    if (Math.Abs(item.Value) > 0.1)
                    {
                        if (count < 5)
                        {
                            fileWriter3.WriteLine(item.Key + "," + Math.Abs(item.Value));
                            count = count + 1;
                        }
                    }
                }

                fileWriter3.WriteLine();
                count = 0;
                fileWriter3.WriteLine("Product,NumOfOrderRejected");
                foreach (KeyValuePair<string, double> item in orderCounts_NumOfOrderRejected.OrderBy(key => key.Value))
                {
                    if (Math.Abs(item.Value) > 0.1)
                    {
                        if (count < 5)
                        {
                            fileWriter3.WriteLine(item.Key + "," + Math.Abs(item.Value));
                            count = count + 1;
                        }
                    }
                }

                fileWriter3.WriteLine();
                count = 0;
                fileWriter3.WriteLine("Product,NumOfOrderRounddownByFatFingerLimit");
                foreach (KeyValuePair<string, double> item in orderCounts_NumOfOrderRounddown.OrderBy(key => key.Value))
                {
                    if (Math.Abs(item.Value) > 0.1)
                    {
                        if (count < 5)
                        {
                            fileWriter3.WriteLine(item.Key + "," + Math.Abs(item.Value));
                            count = count + 1;
                        }
                    }
                }

                fileWriter3.Close();
            }

            // data integrity checking 
            foreach (KeyValuePair<string, int> pair in machineProdDateList)
            {
                if (pair.Value == 0 && !pair.Key.Contains("/"))
                {
                    //logWriter.WriteLine("Signal summary files not found for " + pair.Key);
                }

            }

            foreach (var signalLine in signalFileLines)
            {
                if (signalLine == null) continue;
                if (signalLine.SignalSummarySize != signalLine.Size)
                {
                    sbFillRateSpread.AppendLine("Signal size discrepancy between signal summary and vfix trades for " + signalLine.Product + "-"
                        + signalLine.SignalKey + ", SignalSummarySize = " + signalLine.SignalSummarySize + ", vfix size = " + signalLine.Size);
                }
            }

            var path = outputPath + "\\Log_FillRateSpread_" + fileNamePostfix + ".txt";
            HelperFunction.EnsureFileExists(path);
            //File.WriteAllText(path, sbFillRateSpread.ToString());

            Console.WriteLine("Num of All Orders = " + numAllOrders + ", Num of Skipped Orders = " + numSkippedOrders + ", Num of BBO Cross Orders = " + numBBOEqualOrders);
            sbSkippedOrders.AppendLine("Num of All Orders = " + numAllOrders + ", Num of Skipped Orders = " + numSkippedOrders + ", Num of BBO Cross Orders = " + numBBOEqualOrders);

            var skippedOrdersPath = outputPath + "\\skippedOrders_" + fileNamePostfix + ".txt";
            HelperFunction.EnsureFileExists(skippedOrdersPath);
            //File.WriteAllText(skippedOrdersPath, sbSkippedOrders.ToString());
        }

        private void WriteSlippageToAbsPnlReport()
        {
            var prodPnLDict = new Dictionary<string, double>();
            // Read PnL from DB
            GetProductPnl(prodPnLDict);
            var fileWriter = File.CreateText(outputPath + "\\SlippageToAbsPnLReport_" + fileNamePostfix + ".csv");
            fileWriter.WriteLine("Product,AbsPnL,Slippage,Ratio");
            foreach (string s in prodPnLDict.Keys)
            {
                double _slippage = 0;
                double ratio = 0;
                if ((prodSlippageDict.ContainsKey(s)) && (Math.Abs(prodPnLDict[s]) > 1e-6))
                {
                    _slippage = prodSlippageDict[s];
                    ratio = _slippage / prodPnLDict[s];
                    fileWriter.WriteLine(s + "," + prodPnLDict[s] + "," + _slippage + "," + ratio);
                }
            }
            fileWriter.Close();
        }

        private void GetProductPnl(Dictionary<string, double> prodPnLDict)
        {
            try
            {
                string sql = "select Prod,Pnl from TradeLog where TradeDate >= '" + StartDate + "' and TradeDate <='" + EndDate + "' and Pnl != 'N.A.'";
                var tb = HelperFunction.ExecuteQuery(sql);

                foreach (DataRow row in tb.Rows)
                {
                    string prodName = row["Prod"].ToString().ToUpper();
                    if (prodName == "TX")
                    {
                        prodName = "TXF";
                    }
                    if (prodName == "TXFI")
                    {
                        prodName = "FXF";
                    }
                    if (prodName == "TE")
                    {
                        prodName = "EXF";
                    }
                    if (prodName == "JNM")
                    {
                        prodName = "NM";
                    }
                    double pnl = 0;
                    try
                    {
                        pnl = double.Parse(row["Pnl"].ToString());
                    }
                    catch
                    {
                        pnl = 0;
                    }
                    //Console.WriteLine(prodName + "," + pnl);
                    if (!prodPnLDict.ContainsKey(prodName))
                    {
                        prodPnLDict.Add(prodName, 0);
                    }
                    prodPnLDict[prodName] += Math.Abs(pnl);
                }
            }
            catch (Exception ex)
            {
                OnDBConnectionFailed(ex);
            }
        }

        private void OnDBConnectionFailed(Exception ex)
        {
            Log("Error", "fail to connect DB server. Exception: " + ex.ToString());
        }

        public void Log(string type, string logmessage)
        {
            Logger.Log(type, logmessage);
        }
    }
}
