﻿using System;
using System.Linq;
using System.Configuration;

namespace Utility.Util
{
    public class ConfigUtil
    {
        #region Properties
        
        public static readonly string ConfigFileName = ConfigurationManager.AppSettings["PASSWORD_CONFIG"];
        #endregion

        #region Private Methods
        private static Configuration GetMappedExeConfiguration()
        {
            if (!string.IsNullOrEmpty(ConfigFileName))
            {
                ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
                configFileMap.ExeConfigFilename = ConfigFileName;
                Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configFileMap, ConfigurationUserLevel.None);
                if (config == null || !config.HasFile)
                    throw new ConfigurationErrorsException(String.Format("Configuration file '{0}' could not be loaded.", ConfigFileName));

                return config;
            }
            return null;
        }
        #endregion

        #region Public Methods
        public static string GetMappedExeAppSettings(string key)
        {
            Configuration mappedConfig = GetMappedExeConfiguration();
            KeyValueConfigurationCollection settings = mappedConfig.AppSettings.Settings;

            if (settings[key] == null) return string.Empty;

            string mappedValue = settings[key].Value;
            if (string.IsNullOrEmpty(mappedValue))
                throw new Exception(string.Format("No such key '{0}' was found in the configuration file. Please report to IT Department.", key));

            return mappedValue;
        }

        public static string GetAppSettings(string key)
        {
            return GetAppSettings(key, string.Empty);
        }

        public static string GetAppSettings(string key, string defaultValue)
        {
            var value = ConfigurationManager.AppSettings[key];
            return (value == null) ? defaultValue : value;
        }
        #endregion
    }
}
