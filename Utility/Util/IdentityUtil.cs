﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;
using System.Security;
using System.Runtime.ConstrainedExecution;
using System.IO;


namespace Utility.Util
{
    public class IdentityUtil
    {
        #region Properties
        public static readonly bool SERVICEACCT_ENABLE = Convert.ToBoolean(ConfigurationManager.AppSettings["SERVICEACCT_ENABLE"]);
        public static readonly string SERVICEACCT_DOMAIN = ConfigurationManager.AppSettings["SERVICEACCT_DOMAIN"];
        public static readonly string SERVICEACCT_USERNAME = ConfigurationManager.AppSettings["SERVICEACCT_USERNAME"];
        public static readonly string SERVICEACCT_PASSWORD = Util.ConfigUtil.GetMappedExeAppSettings("SERVICEACCT_PASSWORD");
        public static readonly string SERVICEACCT_PASSWORD_isEncrypted = ConfigurationManager.AppSettings["SERVICEACCT_PASSWORD_ISENCRYPT"];
        public static readonly bool DB_SERVICEACCT_ENABLE = Convert.ToBoolean(ConfigurationManager.AppSettings["DB_SERVICEACCT_ENABLE"]);
        public static readonly string DB_SERVICEACCT_DOMAIN = ConfigurationManager.AppSettings["DB_SERVICEACCT_DOMAIN"];
        public static readonly string DB_SERVICEACCT_USERNAME = ConfigurationManager.AppSettings["DB_SERVICEACCT_USERNAME"];
        //public static readonly string DB_SERVICEACCT_PASSWORD =  ConfigurationManager.AppSettings["DB_SERVICEACCT_PASSWORD"];
        public static readonly string DB_SERVICEACCT_PASSWORD = Util.ConfigUtil.GetMappedExeAppSettings("DB_SERVICEACCT_PASSWORD");
        public static readonly string DB_SERVICEACCT_PASSWORD_isEncrypted = ConfigurationManager.AppSettings["DB_SERVICEACCT_PASSWORD_ISENCRYPT"];

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword, LogonSessionType dwLogonType, LogonProvider dwLogonProvider, out SafeTokenHandle phToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int DuplicateToken(IntPtr hToken, int impersonationLevel, ref IntPtr hNewToken);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool RevertToSelf();

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public extern static bool CloseHandle(IntPtr handle);

        public enum LogonSessionType : int
        {
            Interactive = 2,
            Network,
            Batch,
            Service,
            NetworkCleartext = 8,
            NewCredentials
        }
        public enum LogonProvider : int
        {
            Default = 0, // default for platform (use this!)
            WinNT35,     // sends smoke signals to authority
            WinNT40,     // uses NTLM
            WinNT50      // negotiates Kerb or NTLM
        }

        public enum ImpersonateType : int
        {
            Default = 0, // default for platform (use this!)
            MSSQL     // Impersonate for MSSQL
            
        }
        #endregion

        #region Methods

        // If you incorporate this code into a DLL, be sure to demand FullTrust.
        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public static WindowsImpersonationContext Impersonate()
        {
            return Impersonate(SERVICEACCT_DOMAIN, SERVICEACCT_USERNAME, SERVICEACCT_PASSWORD, ImpersonateType.Default);
        }

        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public static WindowsImpersonationContext DBImpersonate()
        {
            return Impersonate(DB_SERVICEACCT_DOMAIN, DB_SERVICEACCT_USERNAME, DB_SERVICEACCT_PASSWORD, ImpersonateType.MSSQL);
        }

        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public static WindowsImpersonationContext Impersonate(string domainName, string userName, string password)
        {
            return Impersonate(domainName, userName, password, ImpersonateType.Default);
        }

        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        public static WindowsImpersonationContext DBImpersonate(string domainName, string userName, string password)
        {
            return Impersonate(domainName, userName, password, ImpersonateType.MSSQL);
        }

        // If you incorporate this code into a DLL, be sure to demand FullTrust.
        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        private static WindowsImpersonationContext Impersonate(string domainName, string userName, string password, ImpersonateType impType)
        {
            // do nothing if it's not enabled
            if (((ImpersonateType.MSSQL == impType) && !DB_SERVICEACCT_ENABLE) || ((ImpersonateType.MSSQL != impType) && !SERVICEACCT_ENABLE))
                return null;

            //const int LOGON32_PROVIDER_DEFAULT = 0;
            ////This parameter causes LogonUser to create a primary token. 
            //const int LOGON32_LOGON_INTERACTIVE = 2;
            //const int LOGON32_LOGON_BATCH = 1;
            string acctPassword = password;
            string isEncrypted = "";
            
            isEncrypted = SERVICEACCT_PASSWORD_isEncrypted;
            if (impType == ImpersonateType.MSSQL)
            {
                isEncrypted = DB_SERVICEACCT_PASSWORD_isEncrypted;
            }

            SafeTokenHandle safeTokenHandle;
            WindowsImpersonationContext impersonatedUser;
            try
            {
                if ((!(isEncrypted == null) && isEncrypted.ToLower().Equals("true")) && !string.IsNullOrEmpty(acctPassword))
                {
                    string decryptPass = EncryptUtil.DecryptString(acctPassword);
                    acctPassword = decryptPass;
                }

                // Call LogonUser to obtain a handle to an access token. 
                bool bImpersonated = LogonUser(userName, domainName, acctPassword, LogonSessionType.Interactive, LogonProvider.Default, out safeTokenHandle);

                if (false == bImpersonated)
                {
                    int ret = Marshal.GetLastWin32Error();
                    throw new System.ComponentModel.Win32Exception(ret);
                }
                using (safeTokenHandle)
                {
                    //bool bRetVal = DuplicateToken(pExistingTokenHandle, (int)SECURITY_IMPERSONATION_LEVEL.SecurityImpersonation, ref pDuplicateTokenHandle);

                    // Use the token handle returned by LogonUser. 
                    impersonatedUser = WindowsIdentity.Impersonate(safeTokenHandle.DangerousGetHandle());
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Exception occurred. " + ex.Message);
            }

            return impersonatedUser;
        }

        public static void Undo(WindowsImpersonationContext impersonateUser)
        {
            impersonateUser.Undo();
        }

        public static void DoImpersonate(bool enable, Action action)
        {
            if (enable && SERVICEACCT_ENABLE)
            {
                using (Impersonate())
                {
                    action();
                }
            }
            else action();
        }

        public static void DoImpersonate(Action action)
        {
            DoImpersonate(true, action);
        }

        public static void DoDBImpersonate(Action action)
        {
            if (DB_SERVICEACCT_ENABLE)
            {
                using (DBImpersonate())
                {
                    action();
                }
            }
            else action();
        }

        public static void DoDBImpersonate(Action action, bool impersonateEnable)
        {
            if (impersonateEnable)
            {
                using (DBImpersonate())
                {
                    action();
                }
            }
            else action();
        }
        #endregion
    }

    public sealed class SafeTokenHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        private SafeTokenHandle()
            : base(true)
        {
        }

        [DllImport("kernel32.dll")]
        [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
        [SuppressUnmanagedCodeSecurity]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CloseHandle(IntPtr handle);

        protected override bool ReleaseHandle()
        {
            return CloseHandle(handle);
        }
    }

}
