﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Utility.Util
{
    public class ConvertUtil
    {
        #region Enum
        /// <summary>
        /// Parse the enum value.
        /// </summary>
        public static T EnumParse<T>(object value)
        {
            return (T)Enum.Parse(typeof(T), value.ToString());
        }

        /// <summary>
        /// Parse the enum value with default value on conversion failure.
        /// </summary>
        public static T EnumTryParse<T>(object value, T defaultValue) where T : struct
        {
            T result;
            var succeeded = Enum.TryParse<T>(value.ToString(), out result);
            if (!succeeded) result = defaultValue;
            return result;
        }
        #endregion

        #region String
        public static string StringParse(object value)
        {
            return StringParse(value, null);
        }

        public static string StringParse(object value, string defaultValue)
        {
            if (value == null) return defaultValue;
            return value.ToString();
        }
        #endregion

        #region Bool
        public static bool BoolParse(object value)
        {
            return BoolParse(value, false);
        }

        public static bool BoolParse(object value, bool defaultValue)
        {
            var result = false;
            if (Boolean.TryParse(value.ToString(), out result))
            {
                return result;
            }
            else return defaultValue;
        }
        #endregion

        #region Integer
        public static int IntParse(object value)
        {
            return IntParse(value, 0);
        }

        public static int IntParse(object value, int defaultValue)
        {
            var result = 0;
            if (int.TryParse(value.ToString(), out result))
            {
                return result;
            }
            else return defaultValue;
        }
        #endregion

        #region Double
        public static double DoubleParse(object value)
        {
            return DoubleParse(value, 0);
        }

        public static double DoubleParse(object value, double defaultValue)
        {
            double result = 0;
            if (double.TryParse(value.ToString(), out result))
            {
                return result;
            }
            else return defaultValue;
        }
        #endregion
    }
}
