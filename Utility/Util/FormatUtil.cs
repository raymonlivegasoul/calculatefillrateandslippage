﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utility.Util
{
    public class FormatUtil
    {
        public static DateTime GetWeekDay(DateTime source)
        {
            return GetWeekDay(source, true);
        }

        public static DateTime GetWeekDay(DateTime source, bool forward)
        {
            var direction = forward ? 1 : -1;
            while (source.DayOfWeek == DayOfWeek.Saturday
                || source.DayOfWeek == DayOfWeek.Sunday)
            {
                source = source.AddDays(-1);
            }

            return source;
        }

    }
}
