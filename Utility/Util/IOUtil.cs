﻿using System;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Configuration;

namespace Utility.Util
{
    public class IOUtil
    {
        public static string GetFileServerPath(string extendPath)
        {
            return Path.Combine(ConfigurationManager.AppSettings["FILE_SERVER_PATH"], extendPath);
        }

        public static void ConfirmDirectoryExist(string path, bool impersonate = false)
        {
            if (impersonate)
            {
                using (WindowsImpersonationContext impersonatedUser = IdentityUtil.Impersonate())
                {
                    ConfirmDirectoryExist(path);
                }
            }
            else
            {
                ConfirmDirectoryExist(path);
            }
        }

        private static void ConfirmDirectoryExist(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static void ConfirmFileExist(string path, bool impersonate = false)
        {
            if (impersonate)
            {
                using (WindowsImpersonationContext impersonatedUser = IdentityUtil.Impersonate())
                {
                    ConfirmFileExist(path);
                }
            }
            else
            {
                ConfirmFileExist(path);
            }
        }

        private static void ConfirmFileExist(string path)
        {
            if (!File.Exists(path))
            {
                var stream = File.Create(path);
                stream.Close();
            }
        }

        public static int CopyFolder(string org_folder, string arc_folder, bool overwrite, bool impersonate = false)
        {
            if (impersonate)
            {
                using (WindowsImpersonationContext impersonatedUser = IdentityUtil.Impersonate())
                {
                    return CopyFolder(org_folder, arc_folder, overwrite);
                }
            }
            else
            {
                return CopyFolder(org_folder, arc_folder, overwrite);
            }
        }

        private static int CopyFolder(string org_folder, string dest_folder, bool overwrite)
        {
            try
            {
                var count = 0;
                ConfirmDirectoryExist(dest_folder);

                if (Directory.Exists(org_folder))
                {
                    DirectoryInfo di_folderPath = new DirectoryInfo(org_folder);
                    foreach (FileInfo f in di_folderPath.GetFiles())
                    {
                        if (new FileInfo(di_folderPath + "\\" + f.Name).Exists)
                        {
                            f.CopyTo(dest_folder + "\\" + f.Name, overwrite);
                            count++;
                        }
                    }
                }
                return count;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static void MoveFolder(string org_folder, string arc_folder, bool impersonate = false)
        {
            if (impersonate)
            {
                using (WindowsImpersonationContext impersonatedUser = IdentityUtil.Impersonate())
                {
                    MoveFolder(org_folder, arc_folder);
                }
            }
            else
            {
                MoveFolder(org_folder, arc_folder);
            }
        }

        private static void MoveFolder(string org_folder, string dest_folder)
        {
            try
            {
                ConfirmDirectoryExist(dest_folder);

                if (Directory.Exists(org_folder))
                {
                    DirectoryInfo di_folderPath = new DirectoryInfo(org_folder);
                    foreach (FileInfo f in di_folderPath.GetFiles())
                    {
                        if (new FileInfo(di_folderPath + "\\" + f.Name).Exists)
                        {
                            f.MoveTo(dest_folder + "\\" + f.Name);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static void WriteToFile(string path, string fileName, string fileExtension, string content, bool impersonate = false)
        {
            WriteToFile(path, fileName + "." + fileExtension, content, impersonate);
        }

        public static void WriteToFile(string path, string fileName, string content, bool impersonate = false)
        {
            if (impersonate)
            {
                using (WindowsImpersonationContext impersonatedUser = IdentityUtil.Impersonate())
                {
                    WriteToFile(path, fileName, content);
                }
            }
            else
            {
                WriteToFile(path, fileName, content);
            }
        }

        private static void WriteToFile(string path, string fileName, string content)
        {
            var fullPath = Path.Combine(path, fileName);

            ConfirmDirectoryExist(path);
            ConfirmFileExist(fullPath);
            
            File.WriteAllText(fullPath, content);
        }

        public static string[] FileReadAllLines(string path, string fileName, bool impersonate = false)
        {
            var fullPath = Path.Combine(path, fileName);

            return FileReadAllLines(fullPath, impersonate);
        }

        public static string[] FileReadAllLines(string fullPath, bool impersonate = false)
        {
            if (impersonate)
            {
                using (WindowsImpersonationContext impersonatedUser = IdentityUtil.Impersonate())
                {
                    return FileReadAllLines(fullPath);
                }
            }
            else
            {
                return FileReadAllLines(fullPath);
            }
        }

        private static string[] FileReadAllLines(string filePath)
        {
            return File.ReadAllLines(filePath);
        }

        public static string FileReadAllText(string path, string fileName, bool impersonate = false)
        {
            var fullPath = Path.Combine(path, fileName);

            return FileReadAllText(fullPath, impersonate);
        }

        public static string FileReadAllText(string fullPath, bool impersonate = false)
        {
            if (impersonate)
            {
                using (WindowsImpersonationContext impersonatedUser = IdentityUtil.Impersonate())
                {
                    return FileReadAllText(fullPath);
                }
            }
            else
            {
                return FileReadAllText(fullPath);
            }
        }

        private static string FileReadAllText(string filePath)
        {
            return File.ReadAllText(filePath);
        }

        public static FileInfo[] GetFiles(string filePath, bool impersonate = false)
        {
            if (impersonate)
            {
                using (WindowsImpersonationContext impersonatedUser = IdentityUtil.Impersonate())
                {
                    return GetFiles(filePath);
                }
            }
            else
            {
                return GetFiles(filePath);
            }
        }

        private static FileInfo[] GetFiles(string filePath)
        {
            var dir = new DirectoryInfo(filePath);
            return dir.GetFiles();
        }
    }
}
