﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using Utility.Util;

namespace Utility.Database
{
    public class DBManager
    {
        #region Properties
        private readonly Dictionary<string, string> _connectionStrings;
        private static DBManager _instance;
        public static string appConnSSPIKey = "integrated security=sspi;";

        public static IDatabase Default
        {
            get { return GetInstance().GetDatabase("Default"); }
        }

        public static IDatabase Local
        {
            get { return GetInstance().GetDatabase("LOCAL"); }
        }
        #endregion

        #region Constructors
        private DBManager()
        {
            _connectionStrings = new Dictionary<string, string>();
        }

        public static DBManager GetInstance()
        {
            if (_instance == null)
            {
                _instance = new DBManager();
            }
            return _instance;
        }
        #endregion

        #region Methods
        public IDatabase GetDatabase(string key)
        {
            if (!_connectionStrings.Keys.Contains(key))
            {
                string newCs = ConfigurationManager.ConnectionStrings[key].ToString();

                var isEncrypted = ConfigurationManager.AppSettings["DB_Password_Encryption"];
                int pos = newCs.ToLower().IndexOf(EncryptUtil.appConnPasswordKey);
                if ((!(isEncrypted == null) && isEncrypted.ToLower().Equals("true")) && pos != -1)
                {
                    string s = newCs.Substring(pos + EncryptUtil.appConnPasswordKey.Length);
                    string decryptPass = EncryptUtil.DecryptString(s);

                    string decryptedCs = newCs.Replace(s, decryptPass);
                    _connectionStrings.Add(key, decryptedCs);
                }
                else
                {
                    _connectionStrings.Add(key, newCs);
                }
            }
            var cs = _connectionStrings[key];
            var db = new SQLServer(cs);
            return db;
        }


        public IDatabase this[string key]
        {
            get
            {
                return GetDatabase(key);
            }
        }

        #endregion

    }
}

