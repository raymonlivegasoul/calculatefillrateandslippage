﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Utility.Database
{
    public interface IDatabase
    {
        string ConnectionString { get; }
        void RunQuery(string queryString);
        void RunQuery(string queryString, params object[] args);
        void RunStoredProc(string commandName);
        void AddParameter(string parameterName, object parameter);
        void AddOutParameter(string parameterName, SqlDbType type, int size);
        void AddOutParameter(string parameterName, SqlDbType type);
        object GetOutParameter(string name);

        DataSet ExecuteDataSet();
        DataTable ExecuteDataTable();
        int ExecuteNonQuery();
        object ExecuteScalar();
        string ExecuteString();
        void ExecuteUpdateDataSet(string query, DataTable dataTable);
        bool TryConnect();

    }
}