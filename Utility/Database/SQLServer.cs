﻿using System;
using System.Data;
using System.Data.SqlClient;
using Utility.Util;

namespace Utility.Database
{
    public class SQLServer : IDatabase, IDisposable
    {
        private SqlCommand command;
        private string _connectionString;
        public string ConnectionString { get { return _connectionString; } }
        public bool IsSSPI { get; set; }
        private static string appConnSSPIKey = "integrated security=sspi;";

        public SQLServer(string connectionString)
        {
            _connectionString = connectionString;
            int posSSPI = _connectionString.ToLower().IndexOf(appConnSSPIKey);
            if (posSSPI != -1)
            {
                //IsSSPI = true;
            }
        }
        public bool TryConnect()
        {
            bool isConnected = false;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                try
                {                    
                    if (IsSSPI)
                    {
                        IdentityUtil.DoDBImpersonate(() =>
                        {
                            connection.Open();
                            connection.Close();
                            isConnected = true;
                        });
                    }
                    else
                    {
                        connection.Open();
                        connection.Close();
                        isConnected = true;
                    }
                }
                catch (SqlException sqlex)
                {
                    throw sqlex;
                }
            }
            return isConnected;
        }
        public void RunQuery(string queryString)
        {
            command = new SqlCommand();
            command.CommandText = queryString;
            command.CommandType = CommandType.Text;
            command.CommandTimeout = 3000;
        }

        public void RunQuery(string queryString, params object[] args)
        {
            RunQuery(string.Format(queryString, args));
        }

        public void RunStoredProc(string spName)
        {
            command = new SqlCommand();
            command.CommandText = spName;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandTimeout = 3000;
        }


        public void AddParameter(string parameterName, object parameter)
        {
            if (parameter is Enum)
            {
                parameter = Convert.ToInt32(parameter);
            }

            if (parameter == null) parameter = DBNull.Value;

            command.Parameters.AddWithValue(parameterName, parameter);
        }


        public void AddOutParameter(string parameterName, SqlDbType type, int size)
        {
            var outputIdParam = new SqlParameter("@" + parameterName, type, size)
            {
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(outputIdParam);
        }

        public void AddOutParameter(string parameterName, SqlDbType type)
        {
            var outputIdParam = new SqlParameter("@" + parameterName, type)
            {
                Direction = ParameterDirection.Output
            };
            command.Parameters.Add(outputIdParam);
        }

        public object GetOutParameter(string name)
        {
            return command.Parameters["@" + name].Value;
        }

        private static SqlDbType GetSqlDBType(object parameter)
        {
            var result = SqlDbType.NVarChar;
            if (parameter is String)
            {
                result = SqlDbType.NVarChar;
            }
            else if (parameter is Guid)
            {
                result = SqlDbType.UniqueIdentifier;
            }
            else if (parameter is int)
            {
                result = SqlDbType.Int;
            }
            else if (parameter is double)
            {
                result = SqlDbType.Float;
            }
            else if (parameter is bool)
            {
                result = SqlDbType.Bit;
            }
            else if (parameter is DateTime)
            {
                result = SqlDbType.DateTime;
            }
            else if (parameter is TimeSpan)
            {
                result = SqlDbType.Time;
            }
            else if (parameter is Enum)
            {
                result = SqlDbType.Int;
            }
            else if (parameter is Byte[])
            {
                result = SqlDbType.Image;
            }
            return result;
        }

        #region Execute command

        public DataSet ExecuteDataSet()
        {
            var ds = new DataSet();
            Execute(() =>
            {
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(ds);
            });
            
            return ds;
        }

        public DataTable ExecuteDataTable()
        {
            var dt = new DataTable();
            Execute(() =>
            {
                var reader = command.ExecuteReader();
                dt.Load(reader);
            });
            
            return dt;
        }

        public int ExecuteNonQuery()
        {
            var count = 0;
            Execute(() => count = command.ExecuteNonQuery());
            return count;
        }


        public object ExecuteScalar()
        {
            object result = null;
            Execute(() => result = command.ExecuteScalar());

            return result;
        }

        public string ExecuteString()
        {
            var result = ExecuteScalar();
            if (result == DBNull.Value) return null;
            else return result as string;
        }

        private void Execute(Action action)
        {
            if (IsSSPI)
            {
                IdentityUtil.DoDBImpersonate(() =>
                {
                    ExecuteWithTransaction(action);
                });
            }
            else
            {
                ExecuteWithTransaction(action);
            }
            
        }

        private void ExecuteWithTransaction(Action action)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
               
                command.Connection = connection;
                connection.Open();

                var trx = connection.BeginTransaction();
                command.Transaction = trx;

                try
                {
                    action();
                    trx.Commit();
                    //connection.Close();
                }
                catch (Exception ex)
                {
                    trx.Rollback();
                    //connection.Close();
                    throw;
                }
            }
        }

        public void Dispose()
        {
        }
        
        public void ExecuteUpdateDataSet(string query, DataTable dataTable)
        {
            if (IsSSPI)
            {
                IdentityUtil.DoDBImpersonate(() =>
                {
                    ExecuteUpdateDataSetWithTransaction(query, dataTable);
                });
            }
            else
            {
                ExecuteUpdateDataSetWithTransaction(query, dataTable);
            }
        }

        private void ExecuteUpdateDataSetWithTransaction(string query, DataTable dataTable)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;
                    var trx = connection.BeginTransaction();
                    command.Transaction = trx;
                    try
                    {
                        var cmdBuilder = new SqlCommandBuilder(dataAdapter);
                        dataAdapter.Update(dataTable);
                        trx.Commit();
                        //connection.Close();
                    }
                    catch (Exception ex)
                    {
                        trx.Rollback();
                        //connection.Close();
                        throw;
                    }
                }
            }
        }
         
        #endregion
    }
}
